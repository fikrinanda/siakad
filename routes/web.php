<?php

use App\Http\Controllers\Controller;
use App\Http\Livewire\Admin\Index as AdminIndex;
use App\Http\Livewire\Guru\Index as GuruIndex;
use App\Http\Livewire\Siswa\Index as SiswaIndex;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Guru;
use App\Http\Livewire\Auth\Wali;
use App\Http\Livewire\Guru\Data as GuruData;
use App\Http\Livewire\Guru\Tambah as GuruTambah;
use App\Http\Livewire\Guru\Lihat as GuruLihat;
use App\Http\Livewire\Guru\Ubah as GuruUbah;
use App\Http\Livewire\Siswa\Data as SiswaData;
use App\Http\Livewire\Siswa\Tambah as SiswaTambah;
use App\Http\Livewire\Siswa\Lihat as SiswaLihat;
use App\Http\Livewire\Siswa\Ubah as SiswaUbah;
use App\Http\Livewire\Mapel\Data as MapelData;
use App\Http\Livewire\Mapel\Tambah as MapelTambah;
use App\Http\Livewire\Mapel\Lihat as MapelLihat;
use App\Http\Livewire\Mapel\Ubah as MapelUbah;
use App\Http\Livewire\Mapel\Detail as MapelDetail;
use App\Http\Livewire\Kelas\Data as KelasData;
use App\Http\Livewire\Kelas\Tambah as KelasTambah;
use App\Http\Livewire\Kelas\Lihat as KelasLihat;
use App\Http\Livewire\Kelas\Ubah as KelasUbah;
use App\Http\Livewire\Pengajar\Data as PengajarData;
use App\Http\Livewire\Pengajar\Tambah as PengajarTambah;
use App\Http\Livewire\Pengajar\Ubah as PengajarUbah;
use App\Http\Livewire\Nilai\Data as NilaiData;
use App\Http\Livewire\Nilai\Lihat as NilaiLihat;
use App\Http\Livewire\Nilai\Tambah as NilaiTambah;
use App\Http\Livewire\Nilai\Detail as NilaiDetail;
use App\Http\Livewire\Nilai\Import as NilaiImport;
use App\Http\Livewire\Nilai\Perbaikan as NilaiPerbaikan;
use App\Http\Livewire\Soal\Data as SoalData;
use App\Http\Livewire\Soal\Lihat as SoalLihat;
use App\Http\Livewire\Soal\Tambah as SoalTambah;
use App\Http\Livewire\Soal\Detail as SoalDetail;
use App\Http\Livewire\Soal\Ubah as SoalUbah;
use App\Http\Livewire\Tenggat\Data as TenggatData;
use App\Http\Livewire\Tenggat\Tambah as TenggatTambah;
use App\Http\Livewire\Tenggat\Ubah as TenggatUbah;
use App\Http\Livewire\Tugas\Data as TugasData;
use App\Http\Livewire\Tugas\Mulai as TugasMulai;
use App\Http\Livewire\Tugas\Lihat as TugasLihat;
use App\Http\Livewire\Tugas\Koreksi as TugasKoreksi;
use App\Http\Livewire\Tugas\Hasil as TugasHasil;
use App\Http\Livewire\Raport\Data as RaportData;
use App\Http\Livewire\Raport\Lihat as RaportLihat;
use App\Http\Livewire\Raport\Detail as RaportDetail;
use App\Http\Livewire\Raport\Index as RaportIndex;
use App\Http\Livewire\Raport\Show;
use App\Http\Livewire\Raport\Mapel as RaportMapel;
use App\Http\Livewire\Raport\Nilai as RaportNilai;
use App\Http\Livewire\Materi\Data as MateriData;
use App\Http\Livewire\Materi\Lihat as MateriLihat;
use App\Http\Livewire\Materi\Tambah as MateriTambah;
use App\Http\Livewire\Materi\Ubah as MateriUbah;
use App\Http\Livewire\Absensi\Absen;
use App\Http\Livewire\Absensi\Data as AbsensiData;
use App\Http\Livewire\Absensi\Detail as AbsensiDetail;
use App\Http\Livewire\Absensi\Show as AbsensiShow;
use App\Http\Livewire\Absensi\Ubah as AbsensiUbah;
use App\Http\Livewire\Absensi\Ubah2 as AbsensiUbah2;
use App\Http\Livewire\Absensi\History as AbsensiHistory;
use App\Http\Livewire\Jadwal\Data as JadwalData;
use App\Http\Livewire\Jadwal\Tambah as JadwalTambah;
use App\Http\Livewire\Jadwal\Ubah as JadwalUbah;
use App\Http\Livewire\Jadwal\Lihat as JadwalLihat;
use App\Http\Livewire\Profil;
use App\Http\Livewire\Password;
use App\Http\Livewire\Logout;
use App\Models\Absensi;
use App\Models\Detail;
use App\Models\DetailKelas;
use App\Models\Jawaban;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Soal;
use App\Models\Tenggat;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/coba', function () {
    $kelas_id = '7A21';
    $siswa = DetailKelas::where('kelas_id', $kelas_id)->get();
    $kl = Kelas::find($kelas_id);
    $mapel = Mapel::get();
    foreach ($siswa as $key => $value) {

        if ($kl->nama[0] == '7') {
            $kls = 'VII - ' . $kl->nama[1];
        } else   if ($kl->nama[0] == '8') {
            $kls = 'VIII - ' . $kl->nama[1];
        } else   if ($kl->nama[0] == '9') {
            $kls = 'IX - ' . $kl->nama[1];
        }

        foreach ($mapel as $key2 => $value2) {
            $whatIWant = substr($value2->nama, strpos($value2->nama, "-") + 1);
            $jisun = explode("-", $value2->nama);
            if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                $map[$key][$key2]['siswa'] = $value->siswa_id;
                $map[$key][$key2]['id'] = $value2->id;
                $map[$key][$key2]['nama'] = $jisun[0];
                $map[$key][$key2]['tugas'] = 0;
                $map[$key][$key2]['uts'] = 0;
                $map[$key][$key2]['uas'] = 0;
                $map[$key][$key2]['total'] = 0;
            } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                $map[$key][$key2]['siswa'] = $value->siswa_id;
                $map[$key][$key2]['id'] = $value2->id;
                $map[$key][$key2]['nama'] = $jisun[0];
                $map[$key][$key2]['tugas'] = 0;
                $map[$key][$key2]['uts'] = 0;
                $map[$key][$key2]['uas'] = 0;
                $map[$key][$key2]['total'] = 0;
            } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                $map[$key][$key2]['siswa'] = $value->siswa_id;
                $map[$key][$key2]['id'] = $value2->id;
                $map[$key][$key2]['nama'] = $jisun[0];
                $map[$key][$key2]['tugas'] = 0;
                $map[$key][$key2]['uts'] = 0;
                $map[$key][$key2]['uas'] = 0;
                $map[$key][$key2]['total'] = 0;
            }
        }

        $tugas = 0;
        $uts = 0;
        $uas = 0;
        $n = 0;
        foreach ($map as $key2 => $value2) {
            $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $map[$key][$key2]['id'])->where('siswa_id', $value->siswa_id)->where('semester', '1')->get();
            foreach ($nilai as $key3 => $value3) {
                if ($value3->jenis == 'Tugas Rumah' || $value3->jenis == 'Kuis' || $value3->jenis == 'Tugas') {
                    $n++;
                    $tugas += $value3->nilai;
                }
                if ($value3->jenis == 'UTS') {
                    $uts += $value3->nilai;
                }
                if ($value3->jenis == 'UAS') {
                    $uas += $value3->nilai;
                }
            }
            if ($tugas == 0 || $n == 0) {
                $map[$key][$key2]['tugas'] = 0;
                $map[$key][$key2]['total'] = ((2 * 0) + $uts + $uas) / 4;
            } else {
                $tugas = $tugas / $n;
                $map[$key][$key2]['tugas'] = $tugas;
                $map[$key][$key2]['uts'] = $uts;
                $map[$key][$key2]['uas'] = $uas;
                $map[$key][$key2]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
            }

            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
        }
    }

    foreach ($map as $key => $value) {
        foreach ($value as $key2 => $value2) {
            if ($value2['total'] >= 70) {
                $fix[$key][$value2['id']]['Lulus'] = $value2['siswa'];
                $fix[$key][$value2['id']]['Nilai'] = $value2['total'];
            } else {
                $fix[$key][$value2['id']]['Nilai'] = $value2['total'];
                $fix[$key][$value2['id']]['Perbaikan'] = $value2['siswa'];
            }
        }
    }

    dd($fix);
});

Route::get('/import', [Controller::class, 'show']);

Route::middleware('guest')->group(function () {
    Route::get('/', Login::class)->name('login');
    Route::get('/login/guru', Guru::class);
    Route::get('/login/wali', Wali::class);
});
Route::post('/logout', Controller::class)->middleware('auth');

Route::middleware('auth', 'cekAdmin')->group(function () {
    Route::get('/admin', AdminIndex::class);
    Route::get('/guru/data', GuruData::class);
    Route::get('/guru/tambah', GuruTambah::class);
    Route::get('/guru/lihat/{username}', GuruLihat::class);
    Route::get('/guru/ubah/{username}', GuruUbah::class);
    Route::get('/siswa/tambah', SiswaTambah::class);
    Route::get('/siswa/ubah/{username}', SiswaUbah::class);
    Route::get('/mapel/data', MapelData::class);
    Route::get('/mapel/tambah', MapelTambah::class);
    Route::get('/mapel/lihat/{mapel_id}', MapelLihat::class);
    Route::get('/mapel/ubah/{mapel_id}', MapelUbah::class);
    Route::get('/kelas/data', KelasData::class);
    Route::get('/kelas/tambah', KelasTambah::class);
    Route::get('/kelas/lihat/{kelas_id}', KelasLihat::class);
    Route::get('/kelas/ubah/{kelas_id}', KelasUbah::class);
    Route::get('/pengajar/tambah', PengajarTambah::class);
    Route::get('/pengajar/ubah/{kelas_id}/{mapel_id}/{guru_id}', PengajarUbah::class);
    Route::get('/jadwal/data', JadwalData::class);
    Route::get('/jadwal/tambah/{kelas_id}', JadwalTambah::class);
    Route::get('/jadwal/ubah/{id}', JadwalUbah::class);
    Route::get('/jadwal/lihat/{kelas_id}', JadwalLihat::class);
    Route::get('/raport/data', RaportData::class);
    Route::get('/raport/{kelas_id}/{iu}', RaportLihat::class);
    Route::get('/raport/lihat/{kelas_id}/{iu}/{siswa_id}', RaportDetail::class);
    Route::get('/raport/cetak/{kelas_id}/{iu}/{siswa_id}', [Controller::class, 'cetak']);
});

Route::middleware('auth', 'cekGuru')->group(function () {
    Route::get('/guru', GuruIndex::class);
    Route::get('/nilai/data', NilaiData::class);
    Route::get('/nilai/lihat/{kelas_id}/{mapel_id}', NilaiLihat::class);
    Route::get('/nilai/tambah/{siswa}/{kelas_id}/{mapel_id}/{semester}', NilaiTambah::class);
    Route::get('/nilai/perbaikan/{siswa}/{kelas_id}/{mapel_id}/{semester}', NilaiPerbaikan::class);
    Route::get('/nilai/detail/{kelas_id}/{mapel_id}/{siswa_id}/{semester}', NilaiDetail::class);
    Route::get('/nilai/import', NilaiImport::class);
    Route::get('/nilai/export/{kelas_id}/{mapel_id}/{iu}', [Controller::class, 'export']);
    Route::get('/soal/data', SoalData::class);
    Route::get('/soal/lihat/{mapel_id}', SoalLihat::class);
    Route::get('/soal/tambah/{mapel_id}', SoalTambah::class);
    Route::get('/soal/detail/{soal_id}', SoalDetail::class);
    Route::get('/soal/ubah/{soal_id}', SoalUbah::class);
    Route::get('/tugas/data', TenggatData::class);
    Route::get('/tugas/tambah', TenggatTambah::class);
    Route::get('/tugas/ubah/{soal_id}/{kelas_id}', TenggatUbah::class);
    Route::get('/tugas/lihat/{soal_id}/{kelas_id}', TugasLihat::class);
    Route::get('/tugas/koreksi/{soal_id}/{kelas_id}/{siswa_id}', TugasKoreksi::class);
    Route::get('/materi/data', MateriData::class);
    Route::get('/materi/lihat/{mapel_id}/{kelas_id}', MateriLihat::class);
    Route::get('/materi/tambah/{mapel_id}/{kelas_id}', MateriTambah::class);
    Route::get('/materi/ubah/{materi_id}', MateriUbah::class);
    Route::get('/lihat/{id}', function ($id) {
        $materi = Materi::find($id);
        $ex = File::extension($materi->file);
        return response()->download(storage_path("app/public/$materi->file"), $materi->mapel->nama . ' - ' . $materi->nama . '.' . $ex);
    });
    Route::get('/absensi/data', AbsensiData::class);
    Route::get('/absensi/detail/{kelas_id}', AbsensiDetail::class);
    Route::get('/absensi/lihat/{kelas_id}/{tanggal}', AbsensiShow::class);
    Route::get('/absensi/ubah/{kelas_id}/{tanggal}/{siswa_id}', AbsensiUbah::class);
});

Route::middleware('auth', 'cekSiswa')->group(function () {
    Route::get('/siswa', SiswaIndex::class);
    Route::get('/tugas', TugasData::class);
    Route::get('/tugas/{soal_id}/{kelas_id}', TugasMulai::class);
    Route::get('/hasil/{soal_id}/{kelas_id}', TugasHasil::class);
    Route::get('/mapel/{kelas_id}/{mapel_id}', MapelDetail::class);
    Route::get('/unduh/{id}', function ($id) {
        $materi = Materi::find($id);
        $ex = File::extension($materi->file);
        return response()->download(storage_path("app/public/$materi->file"), $materi->mapel->nama . ' - ' . $materi->nama . '.' . $ex);
    });
    Route::get('/absensi', Absen::class);
    Route::get('/raport', RaportIndex::class);
    Route::get('/raport/detail/{kelas_id}/{iu}', Show::class);
    Route::get('/raport/mapel/{kelas_id}/{iu}', RaportMapel::class);
    Route::get('/raport/nilai/{kelas_id}/{mapel_id}/{iu}', RaportNilai::class);
    Route::get('/absensi/riwayat', AbsensiHistory::class);
    Route::get('/riwayat/ubah/{kelas_id}/{tanggal}', AbsensiUbah2::class);
});

Route::middleware('auth', 'cek')->group(function () {
    Route::get('/siswa/data', SiswaData::class);
    Route::get('/siswa/lihat/{username}', SiswaLihat::class);
    Route::get('/pengajar/data', PengajarData::class);
});

Route::get('/profil', Profil::class)->middleware('auth');
Route::get('/password/ubah', Password::class)->middleware('auth');
Route::get('/log', Logout::class)->middleware('auth');
Route::view('/anu', 'anu');
