<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Soal Mata Pelajaran {{$mpl}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">
                            <form wire:submit.prevent="ubah" autocomplete="off">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Semester</label>
                                    <div class="col-sm-9">
                                        <div wire:ignore>
                                            <select wire:model="semester" class="form-control" style="color: black;">
                                                <option hidden>Pilih Semester</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('semester')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" style="color: black;" wire:model="nama">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jenis</label>
                                    <div class="col-sm-9">
                                        <div wire:ignore>
                                            <select wire:model="jenis" class="form-control" style="color: black;">
                                                <option hidden>Pilih Jenis</option>
                                                <option value="Tugas Rumah">Tugas Rumah</option>
                                                <option value="Kuis">Kuis</option>
                                                <option value="UTS">UTS</option>
                                                <option value="UAS">UAS</option>
                                                <option value="Perbaikan">Perbaikan</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('jenis')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Keterangan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="keterangan" style="color: black; resize: none;" placeholder="Masukkan keterangan"></textarea>
                                        <div>
                                            @error('keterangan')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <br>
                                <hr style="border: solid 1px;"><br>

                                @foreach($detail as $index => $data)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertanyaan {{$index+1}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" style="color: black; resize: none;" wire:model="z.{{$index}}.pertanyaan"></textarea>
                                    </div>
                                    <div>
                                        @error('z.' . $index . '.pertanyaan')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan A</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="z.{{$index}}.a" style="color: black; resize: none;" placeholder="Masukkan Pilihan A"></textarea>
                                        <div>
                                            @error('z.' . $index . '.a')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan B</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="z.{{$index}}.b" style="color: black; resize: none;" placeholder="Masukkan Pilihan B"></textarea>
                                        <div>
                                            @error('z.' . $index . '.b')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan C</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="z.{{$index}}.c" style="color: black; resize: none;" placeholder="Masukkan Pilihan C"></textarea>
                                        <div>
                                            @error('z.' . $index . '.c')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan D</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="z.{{$index}}.d" style="color: black; resize: none;" placeholder="Masukkan Pilihan D"></textarea>
                                        <div>
                                            @error('z.' . $index . '.d')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan E</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="z.{{$index}}.e" style="color: black; resize: none;" placeholder="Masukkan Pilihan E"></textarea>
                                        <div>
                                            @error('z.' . $index . '.e')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jawaban</label>
                                    <div class="col-sm-9">
                                        <div>
                                            <select wire:model="z.{{$index}}.jawaban" class="form-control" style="color: black;">
                                                <option hidden>Pilih Jawaban</option>
                                                <option value="a">A</option>
                                                <option value="b">B</option>
                                                <option value="c">C</option>
                                                <option value="d">D</option>
                                                <option value="e">E</option>
                                                <option value="Uraian">Uraian</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('z.' . $index . '.jawaban')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertanyaan Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pertanyaan_gambar">
                                        <div>
                                            @error('s.' . $index . '.pertanyaan_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pertanyaan_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pertanyaan_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pertanyaan_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pertanyaan_gambar))
                                        <div class="mt-3 {{$ds[$index]['pertanyaan_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pertanyaan_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pertanyaan_gambar)) && ($ds[$index]['pertanyaan_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pertanyaan_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan A Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pilihan_a_gambar">
                                        <div>
                                            @error('s.' . $index . '.pilihan_a_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pilihan_a_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pilihan_a_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pilihan_a_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pilihan_a_gambar))
                                        <div class="mt-3 {{$ds[$index]['pilihan_a_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pilihan_a_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pilihan_a_gambar)) && ($ds[$index]['pilihan_a_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del_a({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pilihan_a_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat_a({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan B Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pilihan_b_gambar">
                                        <div>
                                            @error('s.' . $index . '.pilihan_b_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pilihan_b_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pilihan_b_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pilihan_b_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pilihan_b_gambar))
                                        <div class="mt-3 {{$ds[$index]['pilihan_b_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pilihan_b_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pilihan_b_gambar)) && ($ds[$index]['pilihan_b_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del_b({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pilihan_b_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat_b({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan C Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pilihan_c_gambar">
                                        <div>
                                            @error('s.' . $index . '.pilihan_c_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pilihan_c_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pilihan_c_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pilihan_c_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pilihan_c_gambar))
                                        <div class="mt-3 {{$ds[$index]['pilihan_c_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pilihan_c_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pilihan_c_gambar)) && ($ds[$index]['pilihan_c_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del_c({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pilihan_c_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat_c({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan D Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pilihan_d_gambar">
                                        <div>
                                            @error('s.' . $index . '.pilihan_d_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pilihan_d_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pilihan_d_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pilihan_d_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pilihan_d_gambar))
                                        <div class="mt-3 {{$ds[$index]['pilihan_d_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pilihan_d_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pilihan_d_gambar)) && ($ds[$index]['pilihan_d_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del_d({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pilihan_d_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat_d({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan E Gambar</label>
                                    <div class="col-sm-5">
                                        <input type="file" style="color: black;" wire:model="s.{{$index}}.pilihan_e_gambar">
                                        <div>
                                            @error('s.' . $index . '.pilihan_e_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="s.{{$index}}.pilihan_e_gambar">Uploading...</div>
                                        @if (isset($s[$index]['pilihan_e_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $s[$index]['pilihan_e_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @elseif(!is_null($data->pilihan_e_gambar))
                                        <div class="mt-3 {{$ds[$index]['pilihan_e_gambar']}}" style="object-fit: cover;">
                                            <img src="/storage/{{$z[$index]['pilihan_e_gambar']}}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if((!is_null($data->pilihan_e_gambar)) && ($ds[$index]['pilihan_e_gambar'] != 'd-none'))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="del_e({{$index}})">Hapus Gambar</button>
                                    </div>
                                    @elseif(!is_null($data->pilihan_e_gambar))
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-info btn-xs mt-3 float-right" wire:click.prevent="bat_e({{$index}})">Batal Hapus</button>
                                    </div>
                                    @endif
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="hapus('{{$data->hash}}')">-</button>
                                    </div>
                                </div>

                                <br>
                                @endforeach

                                @foreach($soal as $index => $midzy)
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertanyaan {{$index+$shin+1}}</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.pertanyaan" style="color: black; resize: none;" placeholder="Masukkan pertanyaan"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.pertanyaan')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan A</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.a" style="color: black; resize: none;" placeholder="Masukkan Pilihan A"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.a')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan B</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.b" style="color: black; resize: none;" placeholder="Masukkan Pilihan B"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.b')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan C</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.c" style="color: black; resize: none;" placeholder="Masukkan Pilihan C"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.c')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan D</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.d" style="color: black; resize: none;" placeholder="Masukkan Pilihan D"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.d')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan E</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" wire:model="soal.{{$index}}.e" style="color: black; resize: none;" placeholder="Masukkan Pilihan E"></textarea>
                                        <div>
                                            @error('soal.' . $index . '.e')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Jawaban</label>
                                    <div class="col-sm-9">
                                        <div>
                                            <select wire:model="soal.{{$index}}.jawaban" class="form-control" style="color: black;">
                                                <option hidden>Pilih Jawaban</option>
                                                <option value="a">A</option>
                                                <option value="b">B</option>
                                                <option value="c">C</option>
                                                <option value="d">D</option>
                                                <option value="e">E</option>
                                                <option value="Uraian">Uraian</option>
                                            </select>
                                        </div>
                                        <div>
                                            @error('soal.' . $index . '.jawaban')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertanyaan Gambar</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pertanyaan_gambar">
                                        <div>
                                            @error('soal.' . $index . '.pertanyaan_gambar')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pertanyaan_gambar">Uploading...</div>
                                        @if (isset($soal[$index]['pertanyaan_gambar']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pertanyaan_gambar']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan A Gambar</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pilihan_a">
                                        <div>
                                            @error('soal.' . $index . '.pilihan_a')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pilihan_a">Uploading...</div>
                                        @if (isset($soal[$index]['pilihan_a']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pilihan_a']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan B Gambar</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pilihan_b">
                                        <div>
                                            @error('soal.' . $index . '.pilihan_b')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pilihan_b">Uploading...</div>
                                        @if (isset($soal[$index]['pilihan_b']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pilihan_b']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan C Gambar</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pilihan_c">
                                        <div>
                                            @error('soal.' . $index . '.pilihan_c')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pilihan_c">Uploading...</div>
                                        @if (isset($soal[$index]['pilihan_c']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pilihan_c']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan D Gambar</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pilihan_d">
                                        <div>
                                            @error('soal.' . $index . '.pilihan_d')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pilihan_d">Uploading...</div>
                                        @if (isset($soal[$index]['pilihan_d']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pilihan_d']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pilihan E Gambar</label>
                                    <div class="col-sm-7">
                                        <input type="file" style="color: black;" wire:model="soal.{{$index}}.pilihan_e">
                                        <div>
                                            @error('soal.' . $index . '.pilihan_e')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="soal.{{$index}}.pilihan_e">Uploading...</div>
                                        @if (isset($soal[$index]['pilihan_e']))
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $soal[$index]['pilihan_e']->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif

                                    </div>
                                    @if(count($this->soal) > 1)
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-danger btn-xs mt-3 float-right" wire:click.prevent="remove({{$index}})">-</button>
                                    </div>
                                    @endif
                                </div>
                                @endforeach


                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-secondary btn-sm mt-3" wire:click.prevent="add">+</button>
                                </div>

                                <br>
                                <hr style="border: solid 1px;">
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Jumlah Soal Pilihan Ganda {{$pilgan}}</label>
                                    <label class="col-sm-3 col-form-label">Bobot</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" style="color: black;" wire:model="bobot_pilgan" {{$dp}}>
                                        <div>
                                            @error('bobot_pilgan')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <label class="col-sm-3 col-form-label">%</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-12 col-form-label">Jumlah Soal Uraian {{$uraian}}</label>
                                    <label class="col-sm-3 col-form-label">Bobot</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" style="color: black;" wire:model="bobot_uraian" {{$du}}>
                                        <div>
                                            @error('bobot_uraian')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <label class="col-sm-3 col-form-label">%</label>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary ml-3 mt-5">Ubah</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>