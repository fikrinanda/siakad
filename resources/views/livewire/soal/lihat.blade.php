<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Soal Mata Pelajaran {{$mpl}}</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/soal/tambah/{{$i}}" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length">
                                <label>Show <select wire:model="perPage" class="mr-3">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>

                                <label>Semester <select wire:model="semester">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select></label>

                            </div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($soal) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Semester</th>
                                        <th>Jenis</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($soal as $key => $s)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$soal->firstItem() + $key}}</strong></td>
                                        <td style="width: 18%;">{{$s->id}}</td>
                                        <td style="width: 18%;">{{$s->nama}}</td>
                                        <td style="width: 18%;">{{$s->semester}}</td>
                                        <td style="width: 18%;">{{$s->jenis}}</td>
                                        <td style="width: 18%;">
                                            <div class="d-flex">
                                                <a href="/soal/detail/{{$s->id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/soal/ubah/{{$s->id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$s->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$soal->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>