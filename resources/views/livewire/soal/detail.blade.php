<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Soal Mata Pelajaran {{$mpl}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-form">

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Semester</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" style="color: black;" wire:model="semester" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" style="color: black;" wire:model="nama" disabled>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Jenis</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" style="color: black;" wire:model="jenis" disabled>
                                </div>
                            </div>

                            <br>
                            <hr style="border: solid 1px;"><br>
                            @foreach($detail as $index => $data)
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Pertanyaan {{$index+1}}</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="4" style="color: black; resize: none;" disabled>{{$data->pertanyaan}}</textarea>
                                    @if(!is_null($data->pertanyaan_gambar))
                                    <img src="/storage/{{$data->pertanyaan_gambar}}" width="300" height="200" class="my-3">
                                    @endif
                                </div>

                            </div>

                            @if($data->jawaban != 'Uraian')
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Pilihan</label>
                                <div class="col-sm-9 mt-2">
                                    @if(!is_null($data->pilihan_a) || !is_null($data->pilihan_a_gambar))
                                    <div class="radio mb-2">
                                        <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$data->jawaban=='a' ? 'checked' : ''}}> {{$data->pilihan_a}}</label>
                                        <div>
                                            @if(!is_null($data->pilihan_a_gambar))
                                            <img src="/storage/{{$data->pilihan_a_gambar}}" width="300" height="200" class="my-3">
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($data->pilihan_b) || !is_null($data->pilihan_b_gambar))
                                    <div class="radio mb-2">
                                        <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$data->jawaban=='b' ? 'checked' : ''}}> {{$data->pilihan_b}}</label>
                                        <div>
                                            @if(!is_null($data->pilihan_b_gambar))
                                            <img src="/storage/{{$data->pilihan_b_gambar}}" width="300" height="200" class="my-3">
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($data->pilihan_c) || !is_null($data->pilihan_c_gambar))
                                    <div class="radio mb-2">
                                        <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$data->jawaban=='c' ? 'checked' : ''}}> {{$data->pilihan_c}}</label>
                                        <div>
                                            @if(!is_null($data->pilihan_c_gambar))
                                            <img src="/storage/{{$data->pilihan_c_gambar}}" width="300" height="200" class="my-3">
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($data->pilihan_d) || !is_null($data->pilihan_d_gambar))
                                    <div class="radio mb-2">
                                        <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$data->jawaban=='d' ? 'checked' : ''}}> {{$data->pilihan_d}}</label>
                                        <div>
                                            @if(!is_null($data->pilihan_d_gambar))
                                            <img src="/storage/{{$data->pilihan_d_gambar}}" width="300" height="200" class="my-3">
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($data->pilihan_e) || !is_null($data->pilihan_e_gambar))
                                    <div class="radio mb-2">
                                        <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$data->jawaban=='e' ? 'checked' : ''}}> {{$data->pilihan_e}}</label>
                                        <div>
                                            @if(!is_null($data->pilihan_e_gambar))
                                            <img src="/storage/{{$data->pilihan_e_gambar}}" width="300" height="200" class="my-3">
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <br>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>