<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lihat Raport {{$ket}}</h4>
                    </div>
                    <div class="card-body">
                        <!-- <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div> -->
                        @if(count($gee) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Nama</th>
                                        <th>Rata-rata Nilai</th>
                                        <th>Peringkat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($gee as $key)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$loop->iteration}}</strong></td>
                                        <td style="width: 10%;">{{$key['nama']}}</td>
                                        <td style="width: 10%;">{{$key['rata']}}</td>
                                        <td style="width: 10%;">Peringkat {{$loop->iteration}}</td>
                                        <td style="width: 10%;">
                                            <div class="d-flex">
                                                <a href="/raport/lihat/{{$kelas_id}}/{{$iu}}/{{$key['id']}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>