<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Detail Raport</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Nama Sekolah</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>SMPN 1 Ngantru</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class=" mb-3">
                                            <h5>Alamat</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>Jl. RAYA NGANTRU NO.142</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Nama Peserta Didik</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{auth()->user()->siswa->nama}}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Kelas</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class=" mb-3">
                                            <h5>{{$kls}}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Semester ke</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{$iu}}</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Tahun Pelajaran</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{$tahun}}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        @if(count($map) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Mata Pelajaran</th>
                                        <th>KKM</th>
                                        <th>Nilai</th>
                                        <th>Deskripsi Kemajuan Belajar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($map as $key => $s)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$loop->iteration}}</strong></td>
                                        <td style="width: 20%;">{{$s['nama']}}</td>
                                        <td style="width: 20%;">70</td>
                                        <td style="width: 20%;">
                                            <?php
                                            if (round($s['total']) < 70) {
                                                $test =  App\Models\Perbaikan::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->where('mapel_id', $s['id'])->where('semester', $iu);
                                                if ($test->exists()) {
                                                    if ($test->first()->nilai >= 70) {
                                                        echo 70;
                                                    } else {
                                                        if ($test->first()->nilai >= round($s['total'])) {
                                                            echo $test->first()->nilai;
                                                        } else {
                                                            echo round($s['total']);
                                                        }
                                                    }
                                                } else {
                                                    echo round($s['total']);
                                                }
                                            } else {
                                                echo round($s['total']);
                                            }
                                            ?>
                                        </td>
                                        <td style="width: 30%;">
                                            @if(round($s['total']) > 70)
                                            Terlampaui
                                            @elseif(round($s['total']) == 70)
                                            Tercapai
                                            @elseif(round($s['total']) < 70) <?php
                                                                                $test =  App\Models\Perbaikan::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->where('mapel_id', $s['id'])->where('semester', $iu);
                                                                                if ($test->exists()) {
                                                                                    if ($test->first()->nilai >= 70) {
                                                                                        echo 'Tercapai';
                                                                                    } else {
                                                                                        if ($test->first()->nilai >= round($s['total'])) {
                                                                                            echo 'Belum Terlampaui';
                                                                                        } else {
                                                                                            echo 'Belum Terlampaui';
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    echo 'Belum Terlampaui';
                                                                                }
                                                                                ?> @endif </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Ketidakhadiran</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Sakit</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{$sakit}} hari</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class=" mb-3">
                                            <h5>Izin</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class=" mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{$izin}} hari</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="mb-3">
                                            <h5>Tanpa Keterangan</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="mb-3">
                                            <h5>:</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <h5>{{$tanpa_keterangan}} hari</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @if($iu == '2')
            @if($x)
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Keputusan</h4>
                    </div>
                    <div class="card-body">
                        <p>Berdasarkan hasil yang telah dicapai pada semester 1 dan 2, peserta didik ditetapkan</p>
                        <h5>{{$z}}</h5>
                    </div>
                </div>
            </div>
            @elseif($itzeh && $kelas_id[0] == '9')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Keputusan</h4>
                    </div>
                    <div class="card-body">
                        <p>Berdasarkan hasil yang telah dicapai pada semester 1 dan 2, peserta didik ditetapkan</p>
                        <h5>Lulus</h5>
                    </div>
                </div>
            </div>
            @else
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5>Keputusan :</h5>
                        <p>Berdasarkan hasil yang telah dicapai pada semester 1 dan 2, peserta didik ditetapkan</p>
                        <div class="basic-form">
                            <form wire:submit.prevent="keputusan" autocomplete="off">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Naik ke kelas</label>
                                    <div class="col-sm-9">
                                        <div>
                                            <select wire:model="naik" class="form-control" style="color: black;" {{$nd}}>
                                                <option hidden>Pilih kelas</option>
                                                @if(count($kelas) > 0)
                                                @foreach($kelas as $g)
                                                <option value="{{$g->id}}">{{$g->nama}} - {{$g->tahun}}</option>
                                                @endforeach
                                                @else
                                                <option value="Lulus">Lulus</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div>
                                            @error('naik')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tinggal di kelas</label>
                                    <div class="col-sm-9">
                                        <div>
                                            <select wire:model="tinggal" class="form-control" style="color: black;" {{$td}}>
                                                <option hidden>Pilih kelas</option>
                                                @foreach($kelas2 as $g)
                                                <option value="{{$g->id}}">{{$g->nama}} - {{$g->tahun}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div>
                                            @error('tinggal')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @if(!$x)
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary ml-3 {{$d}}">Ok</button>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endif
        </div>
    </div>
</div>