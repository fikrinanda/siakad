<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nilai Kelas {{$kelas_id}}</h4>
                    </div>
                    <div class="card-body">
                        @if(count($mapel) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mapel as $key => $g)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$key+1}}</strong></td>
                                        <td style="width: 60%;">{{$g->nama}}</td>
                                        <td style="width: 30%;">
                                            <div class="d-flex">
                                                <a href="/raport/nilai/{{$kelas_id}}/{{$g->id}}/{{$iu}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>