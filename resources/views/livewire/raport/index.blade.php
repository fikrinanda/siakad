<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Raport</h4>
                    </div>
                    <div class="card-body">
                        @if(count($raport) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th>Nama Kelas</th>
                                        <th>Semester</th>
                                        <th>Tahun</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($raport as $key => $k)
                                    <tr>
                                        <td style="width: 25%;">{{$k->kelas->nama}}</td>
                                        <td style="width: 25%;">1</td>
                                        <td style="width: 25%;">{{$k->kelas->tahun}}</td>
                                        <td style="width: 25%;">
                                            <div class="d-flex">
                                                <a href="/raport/detail/{{$k->kelas_id}}/1" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/raport/mapel/{{$k->kelas_id}}/1" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat">Data Nilai</a>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 25%;">{{$k->kelas->nama}}</td>
                                        <td style="width: 25%;">2</td>
                                        <td style="width: 25%;">{{$k->kelas->tahun}}</td>
                                        <td style="width: 25%;">
                                            <div class="d-flex">
                                                <a href="/raport/detail/{{$k->kelas_id}}/2" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/raport/mapel/{{$k->kelas_id}}/2" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat">Data Nilai</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>