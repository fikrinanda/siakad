<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Materi</h4>
                        <!-- <div class="d-sm-flex align-items-center">
                            <a href="/soal/tambah/" class="btn btn-outline-primary rounded">Tambah</a>
                        </div> -->
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($kelas) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Kelas</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($kelas as $key => $k)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$kelas->firstItem() + $key}}</strong></td>
                                        <td style="width: 30%;">{{$k->mapel->nama}}</td>
                                        <td style="width: 30%;">{{$k->kelas->nama}} - {{$k->kelas->tahun}}</td>
                                        <td style="width: 30%;">
                                            <div class="d-flex">
                                                <a href="/materi/lihat/{{$k->mapel_id}}/{{$k->kelas_id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$kelas->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>