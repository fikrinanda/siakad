<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Materi {{$mpl}}</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Judul Materi</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="nama" placeholder="Masukkan judul materi">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Soal</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="soal_id" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Soal</option>
                                                        <option value="">Tanpa Soal</option>
                                                        @foreach($soal as $s)
                                                        <option value="{{$s->id}}">{{$s->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('soal_id')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Link</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="link" placeholder="Masukkan link">
                                                <div>
                                                    @error('link')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">File</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="file">
                                                <div>
                                                    @error('file')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3 mt-5">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>