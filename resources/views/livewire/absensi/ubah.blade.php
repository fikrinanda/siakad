<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Absensi</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Siswa</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="sis" disabled>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="kel" disabled>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tanggal</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="tang" disabled>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Keterangn</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="ket" class="form-control" style="color: black;">
                                                        <option value="Hadir">Hadir</option>
                                                        <option value="Sakit">Sakit</option>
                                                        <option value="Izin">Izin</option>
                                                        <option value="Bolos">Tanpa Keterangan</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('ket')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        @if($ket != 'Hadir')
                                        <div class="form-group row d-flex justify-content-center align-items-center">
                                            <label class="col-sm-3 col-form-label">Surat</label>
                                            @if($surat != null)
                                            <div class="col-sm-9">
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <img src="/storage/{{$surat}}" height="400" width="300">
                                                </div>
                                            </div>
                                            @else
                                            <div class="col-sm-9">
                                                Belum melampirkan surat
                                            </div>
                                            @endif
                                        </div>
                                        @endif

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3 mt-5">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>