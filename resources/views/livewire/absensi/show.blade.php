<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Absensi {{$tgl}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-end" wire:ignore>
                            <div><label>Keterangan <select wire:model="ket">
                                        <option hidden>Pilih Keterangan</option>
                                        <option value="Hadir">Hadir</option>
                                        <option value="Sakit">Sakit</option>
                                        <option value="Izin">Izin</option>
                                    </select></label></div>
                        </div>
                        @if(count($absen) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Siswa</th>
                                        <th>Keterangan</th>
                                        <th>Surat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($absen as $key => $g)
                                    <tr>
                                        <td style="width: 8%;"><strong>{{$key +1}}</strong></td>
                                        <td style="width: 23%;">{{$g->siswa->nama}}</td>
                                        <td style="width: 23%;">{{$g->keterangan}}</td>
                                        <td style="width: 23%;">
                                            @if($g->surat != null)
                                            <div class="d-flex">
                                                <a href="/storage/{{$g->surat}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat Surat" data-lightbox="mygallery"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                            @elseif($g->keterangan != 'Hadir' && $g->surat == null)
                                            Belum melampirkan surat
                                            @endif
                                        </td>
                                        <td style="width: 23%;">
                                            <div class="d-flex">
                                                <a href="/absensi/ubah/{{$kelas_id}}/{{$tanggal}}/{{$g->siswa_id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Siswa yang tidak hadir</h4>
                    </div>
                    <div class="card-body">
                        @if(count($bolos) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Siswa</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bolos as $key => $g)
                                    <tr>
                                        <td style="width: 30%;"><strong>{{$key +1}}</strong></td>
                                        <td style="width: 30%;">{{$g->siswa->nama}}</td>
                                        <td style="width: 30%;">
                                            <div class="d-flex">
                                                <a href="/absensi/ubah/{{$kelas_id}}/{{$tanggal}}/{{$g->siswa_id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>