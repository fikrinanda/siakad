<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Ubah Absensi {{$tgl}}</h4>
                    </div>
                    <div class="card-body">
                        <form wire:submit.prevent="ubah" autocomplete="off">
                            <div class="form-group row mt-3">
                                <label class="col-sm-3 col-form-label">Keterangan</label>
                                <div class="col-sm-9">
                                    <div wire:ignore>
                                        <select wire:model="ket" class="form-control" style="color: black;">
                                            <option value="Hadir">Hadir</option>
                                            <option value="Sakit">Sakit</option>
                                            <option value="Izin">Izin</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            @if($ket != 'Hadir')
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Surat</label>
                                <div class="col-sm-9">
                                    <input type="file" style="color: black;" wire:model="surat2">
                                    <div>
                                        @error('surat2')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div wire:loading wire:target="surat2">Uploading...</div>
                                    @if ($surat2)
                                    <div class="mt-3" style="object-fit: cover;">
                                        <div>Photo Preview:</div>
                                        <img src="{{ $surat2->temporaryUrl() }}" height="400" width="300">
                                    </div>
                                    @elseif($surat)
                                    <div class="mt-3" style="object-fit: cover;">
                                        <img src="/storage/{{$surat}}" height="400" width="300">
                                    </div>
                                    @endif
                                </div>
                            </div>
                            @endif

                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-outline-primary rounded">Ok</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>