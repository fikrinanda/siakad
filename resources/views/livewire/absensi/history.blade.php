<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Riwayat Absensi</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <!-- <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div> -->
                        </div>
                        @if(count($absensi) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Kelas</th>
                                        <th>Tanggal</th>
                                        <th>Keterangan</th>
                                        <th>Surat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($absensi as $key => $g)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$absensi->firstItem() + $key}}</strong></td>
                                        <td style="width: 18%;">{{$g->kelas->nama}} - {{$g->kelas->tahun}}</td>
                                        <td style="width: 18%;">{{$g->tanggal->format('d F Y')}}</td>
                                        <td style="width: 18%;">{{$g->keterangan}}</td>
                                        <td style="width: 18%;">
                                            @if($g->surat != null)
                                            <div class="d-flex">
                                                <a href="/storage/{{$g->surat}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat Surat" data-lightbox="mygallery"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                            @elseif($g->keterangan != 'Hadir' && $g->surat == null)
                                            Belum melampirkan surat
                                            @endif
                                        </td>
                                        <td style="width: 18%;">
                                            <?php
                                            $absensi2 = App\Models\Absensi::where('kelas_id', $g->kelas_id)->where('tanggal', Carbon\Carbon::parse($g->tanggal->format('d-m-Y'))->format('Y-m-d'));
                                            $t = Carbon\Carbon::parse($g->tanggal->format('d-m-Y'))->format('d') + 3;
                                            $t2 = Carbon\Carbon::now()->format('d');
                                            ?>
                                            @if($absensi2->exists() && ($t2 <= $t)) <div class="d-flex">
                                                <a href="/riwayat/ubah/{{$g->kelas_id}}/{{$g->tanggal->format('d-m-Y')}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                        </div>
                        @endif
                        </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                    <div class="mt-3">
                        {{$absensi->links()}}
                    </div>
                    @else
                    <h1>Tidak ada data</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>