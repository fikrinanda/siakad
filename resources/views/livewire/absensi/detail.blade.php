<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Absensi</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div><label>Tanggal <select wire:model="tanggal" data-live-search="true">
                                        <option hidden>Pilih Tanggal</option>
                                        @foreach($absensi as $k)
                                        <option value="{{$k->tanggal->format('Y-m-d')}}">{{$k->tanggal->format('d, M Y')}}</option>
                                        @endforeach
                                    </select></label></div>
                        </div>
                        @if(count($absen) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Tanggal</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($absen as $key => $g)
                                    <tr>
                                        <td style="width: 30%;"><strong>{{$key +1}}</strong></td>
                                        <td style="width: 30%;">{{$g->tanggal->format('d, M Y')}}</td>
                                        <td style="width: 30%;">
                                            <div class="d-flex">
                                                <a href="/absensi/lihat/{{$kelas_id}}/{{$g->tanggal->format('d-m-Y')}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$absen->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>