<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Absensi</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/absensi/riwayat" class="btn btn-sm btn-outline-primary rounded">Riwayat</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php

                        use Carbon\Carbon;
                        use App\Models\Absensi;

                        $a = Absensi::where('siswa_id', auth()->user()->siswa->id)->latest('tanggal')->exists();
                        if ($a) {
                            $z = Absensi::where('siswa_id', auth()->user()->siswa->id)->latest('tanggal')->first()->tanggal;
                        }
                        $b = Carbon::now()->format('d M Y');
                        ?>
                        @if((Carbon::now()->format('D') != 'Sat') || (Carbon::now()->format('D') != 'Sun'))
                        @if(Absensi::where('siswa_id', auth()->user()->siswa->id)->where('keterangan', '!=', 'Bolos')->latest('tanggal')->exists() && Absensi::where('siswa_id', auth()->user()->siswa->id)->where('keterangan', '!=', 'Bolos')->latest('tanggal')->first()->tanggal->format('d M Y') == $b)
                        <h4 style="color: black;">Anda sudah melakukan absensi hari ini</h4>
                        @elseif((Carbon::now()->format('H:i:s') > "07:00:00" ) && (Carbon::now()->format('H:i:s') < "12:00:00" )) <h4 style="color: black;">Tanggal {{Carbon::now()->format('d M Y')}}</h4>
                            <form wire:submit.prevent="absen" autocomplete="off">
                                <div class="form-group row mt-3">
                                    <label class="col-sm-3 col-form-label">Keterangan</label>
                                    <div class="col-sm-9">
                                        <div wire:ignore>
                                            <select wire:model="ket" class="form-control" style="color: black;">
                                                <option value="Hadir">Hadir</option>
                                                <option value="Sakit">Sakit</option>
                                                <option value="Izin">Izin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                @if($ket != 'Hadir')
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Foto</label>
                                    <div class="col-sm-9">
                                        <input type="file" style="color: black;" wire:model="surat">
                                        <div>
                                            @error('surat')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div wire:loading wire:target="surat">Uploading...</div>
                                        @if ($surat)
                                        <div class="mt-3" style="object-fit: cover;">
                                            <div>Photo Preview:</div>
                                            <img src="{{ $surat->temporaryUrl() }}" height="400" width="300">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @endif

                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-outline-primary rounded">Ok</button>
                                </div>
                            </form>
                            @endif
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>