<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white">
                            <div>
                                {{$nama}}
                            </div>
                            <div class="mt-2">
                                Kelas {{$n}}
                            </div>
                        </h5>
                    </div>
                    <div class="card-body mb-0" style="color:black">
                        <h4>Materi</h4>
                        <div class="list-icons">
                            <ul class="list-icons">
                                @foreach($materi as $m)
                                @if($m->file != null)
                                <li><a href="/unduh/{{$m->id}}" style="font-weight: bold;"><span class="align-middle mr-2"><i class="fa fa-chevron-right"></i></span>{{$m->nama}}</a></li>
                                @endif
                                @if($m->link != null)
                                <li><a href="//{{$m->link}}" style="font-weight: bold;" target="_blank"><span class="align-middle mr-2"><i class="fa fa-chevron-right"></i></span>{{$m->link}}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>

                        <h4 class="mt-5">Tugas</h4>
                        <style>
                            #h:hover {
                                text-decoration: underline;
                            }
                        </style>
                        @foreach($data as $key2 => $d)
                        @foreach($tenggat as $key => $te)
                        @if(!App\Models\Jawaban::where('siswa_id', auth()->user()->siswa->id)->where('soal_id', $te->soal_id)->exists())
                        @if($te->tenggat > $tgl)
                        @if(($d['id'] == $te->soal->mapel_id) && ($te->soal->jenis != 'Perbaikan'))
                        <div>
                            Tenggat : {{$te->tenggat->format('d, M Y')}} Jam {{$te->tenggat->format('H:i')}}
                        </div>
                        @endif
                        @if (($d['id'] == $te->soal->mapel_id) && ($te->soal->jenis != 'Perbaikan'))
                        <div class="mb-3">
                            <a href="/tugas/{{$te->soal_id}}/{{$te->kelas_id}}" style="color:black" id="h">
                                {{$te->soal->jenis}} - {{$te->soal->nama}}
                            </a>
                        </div>
                        @endif

                        @if(($d['id'] == $te->soal->mapel_id) && (70 > $map[$key2]['total']) && ($te->soal->jenis == 'Perbaikan'))
                        <div>
                            Tenggat : {{$te->tenggat->format('d, M Y')}} Jam {{$te->tenggat->format('H:i')}}
                        </div>
                        @endif
                        @if (($d['id'] == $te->soal->mapel_id) && (70 > $map[$key2]['total']) && ($te->soal->jenis == 'Perbaikan'))
                        <div class="mb-3">
                            <a href="/tugas/{{$te->soal_id}}/{{$te->kelas_id}}" style="color:black" id="h">
                                {{$te->soal->jenis}} - {{$te->soal->nama}}
                            </a>
                        </div>
                        @endif
                        @endif
                        @endif
                        @endforeach
                        @endforeach

                        <h4 class="mt-5">Hasil</h4>
                        @foreach($tenggat as $t)
                        @if($t->bagikan == 'Ya' && $t->soal->jenis != 'Perbaikan')
                        <div class="mb-3">
                            <a href="/hasil/{{$t->soal_id}}/{{$t->kelas_id}}" style="color:black" id="h">
                                {{$t->soal->jenis}} - {{$t->soal->nama}}
                            </a>
                        </div>
                        @endif
                        @if($t->bagikan == 'Ya' && $test && $t->soal->jenis == 'Perbaikan')
                        <div class="mb-3">
                            <a href="/hasil/{{$t->soal_id}}/{{$t->kelas_id}}" style="color:black" id="h">
                                {{$t->soal->jenis}} - {{$t->soal->nama}}
                            </a>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>