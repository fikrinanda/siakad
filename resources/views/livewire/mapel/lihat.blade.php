<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Mata Pelajaran {{$ket}}</h4>

                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-start" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                        </div>
                        @if(count($guru) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Kelas</th>
                                        <th>Tahun</th>
                                        <th>Guru</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($guru as $key => $g)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$guru->firstItem() + $key}}</strong></td>
                                        <td style="width: 30%;">{{$g->kelas->nama}}</td>
                                        <td style="width: 30%;">{{$g->kelas->tahun}}</td>
                                        <td style="width: 30%;">{{$g->guru->nama}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$guru->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>