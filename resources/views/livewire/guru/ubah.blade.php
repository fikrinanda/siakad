<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Guru</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nama" wire:model="nama">
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                            <div class="col-sm-9 mt-2">
                                                <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Laki-laki"> Laki-laki</label>
                                                <label class="radio-inline mr-3"><input type="radio" wire:model="jenis_kelamin" value="Perempuan"> Perempuan</label>
                                                <div>
                                                    @error('jenis_kelamin')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                            <div class="col-sm-9">
                                                <input type="date" class="form-control" style="color: black;" wire:model="tanggal_lahir" max="{{$tgl}}">
                                                <div>
                                                    @error('tanggal_lahir')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" rows="4" wire:model="alamat" style="color: black; resize: none;" placeholder="Masukkan alamat"></textarea>
                                                <div>
                                                    @error('alamat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Foto</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="foto2">
                                                <div>
                                                    @error('foto2')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="foto2">Uploading...</div>
                                                @if ($foto2)
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <div>Photo Preview:</div>
                                                    <img src="{{ $foto2->temporaryUrl() }}" height="400" width="300">
                                                </div>
                                                @else
                                                <div class="mt-3" style="object-fit: cover;">
                                                    <img src="/storage/{{$foto}}" height="400" width="300">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>