<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lihat Jadwal</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/jadwal/tambah/{{$kelas_id}}" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($jadwal) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Hari</th>
                                        <th>Mapel</th>
                                        <th>Guru</th>
                                        <th>Jam</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($jadwal as $key => $g)
                                    <?php
                                    $nona = explode("-", $g->jam);
                                    ?>
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$key+1}}</strong></td>
                                        <td style="width: 18%;">{{$g->hari}}</td>
                                        <td style="width: 18%;">{{$g->mapel->nama}}</td>
                                        <td style="width: 18%;">{{$g->guru->nama}}</td>
                                        <td style="width: 18%;">{{$awal[$nona[0]]}} - {{$akhir[$nona[1]]}}</td>
                                        <td style="width: 18%;">
                                            <div class="d-flex">
                                                <a href="/jadwal/ubah/{{$g->id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$g->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>