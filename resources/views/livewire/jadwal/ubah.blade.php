<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Jadwal</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Hari</label>
                                            <div class="col-sm-9">
                                                <div>
                                                    <select wire:model="hari" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Hari Kelas</option>
                                                        <option value="Senin">Senin</option>
                                                        <option value="Selasa">Selasa</option>
                                                        <option value="Rabu">Rabu</option>
                                                        <option value="Kamis">Kamis</option>
                                                        <option value="Jumat">Jumat</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('hari')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Mapel</label>
                                            <div class="col-sm-9">
                                                <div>
                                                    <select wire:model="mapel" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Mapel</option>
                                                        @foreach($mpl as $g)
                                                        <option value="{{$g->id}}">{{$g->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('mapel')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Guru</label>
                                            <div class="col-sm-9 d-flex justify-content-start align-items-center">
                                                <div>
                                                    <div>
                                                        {{$guru}}
                                                    </div>
                                                    <div>
                                                        @error('guru')
                                                        <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jam</label>
                                            <div class="col-sm-4">
                                                <div>
                                                    <select wire:model="jam1" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Jam Mulai</option>
                                                        @for ($i = $a; $i < $shin; $i++) <option value="{{$i}}">Jam ke {{$i+1}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('jam1')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-1 d-flex justify-content-center align-items-center">-</div>
                                            <div class="col-sm-4">
                                                <div>
                                                    <select wire:model="jam2" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Jam Selesai</option>
                                                        @for ($i = $b; $i < $ryujin; $i++) <option value="{{$i}}">Jam ke {{$i+1}}</option>
                                                            @endfor
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('jam2')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>