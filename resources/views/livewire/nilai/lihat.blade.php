<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Nilai Kelas {{$kls}} Mata Pelajaran {{$mpl}}</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/nilai/import" class="btn btn-outline-primary rounded btn-sm">Import</a>
                            <a href="/nilai/export/{{$i}}/{{$i2}}/{{$semester}}" class="btn btn-outline-primary rounded btn-sm ml-3">Export</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length">
                                <label>Show <select wire:model="perPage" class="mr-3">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>

                                <label>Semester <select wire:model="semester">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select></label>

                            </div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($nilai) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Nama</th>
                                        <th>Tugas</th>
                                        <th>UTS</th>
                                        <th>UAS</th>
                                        <th>Total</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($nilai as $key => $n)
                                    <tr>
                                        <td style="width: 9%;"><strong>{{$nilai->firstItem() + $key}}</strong></td>
                                        <td style="width: 13%;">{{$n->nama}}</td>
                                        <td style="width: 13%;">
                                            <?php
                                            $j = 0;
                                            $t = 0;
                                            $tu = App\Models\Nilai::where('siswa_id', $n->id)->where('mapel_id', $i2)->where(function ($q) {
                                                $q->where('jenis', 'Tugas Rumah');
                                                $q->orWhere('jenis', 'Kuis');
                                                $q->orWhere('jenis', 'Tugas');
                                            })->where('kelas_id', $i)->where('semester', $semester)->get();

                                            foreach ($tu as $key) {
                                                $j += $key->nilai;
                                                $t++;
                                            }
                                            if ($j > 0) {
                                                $tugas = $j / $t;
                                                echo round($j / $t);
                                            } else {
                                                $tugas = 0;
                                                echo 0;
                                            }
                                            ?>
                                        </td>
                                        <td style="width: 13%;">
                                            <?php
                                            $j = 0;
                                            $uts = App\Models\Nilai::where('siswa_id', $n->id)->where('mapel_id', $i2)->where('jenis', 'UTS')->where('kelas_id', $i)->where('semester', $semester)->first();
                                            if (isset($uts->nilai)) {
                                                $salon = $uts->nilai;
                                                echo $uts->nilai;
                                            } else {
                                                $salon = 0;
                                                echo 0;
                                            }
                                            ?>
                                        </td>
                                        <td style="width: 13%;">
                                            <?php
                                            $j = 0;
                                            $uas = App\Models\Nilai::where('siswa_id', $n->id)->where('mapel_id', $i2)->where('jenis', 'UAS')->where('kelas_id', $i)->where('semester', $semester)->first();
                                            if (isset($uas->nilai)) {
                                                $bobrok = $uas->nilai;
                                                echo $uas->nilai;
                                            } else {
                                                $bobrok = 0;
                                                echo 0;
                                            }
                                            ?>
                                        </td>
                                        <td style="width: 13%;">
                                            <?php
                                            $pekenyes = round(((2 * $tugas) + $salon + $bobrok) / 4);
                                            echo $pekenyes;
                                            ?>
                                        </td>
                                        <td style="width: 13%;">
                                            @if($pekenyes > 70)
                                            Terlampaui
                                            @elseif($pekenyes == 70)
                                            Tercapai
                                            @elseif($pekenyes < 70) @if(App\Models\Perbaikan::where('siswa_id', $n->id)->where('mapel_id', $i2)->where('kelas_id', $i)->where('semester', $semester)->exists())
                                                @php
                                                $z = App\Models\Perbaikan::where('siswa_id', $n->id)->where('mapel_id', $i2)->where('kelas_id', $i)->where('semester', $semester)->first();
                                                @endphp

                                                @if($z->nilai >= 70)
                                                Tercapai (melakukan perbaikan, mendapat nilai {{$z->nilai}})
                                                @elseif($z->nilai < $pekenyes) Belum Tercapai (melakukan perbaikan, mendapat nilai {{$z->nilai}}) @endif @else <a href="/nilai/perbaikan/{{$n->id}}/{{$i}}/{{$i2}}/{{$semester}}" class="btn btn-danger shadow sharp mr-1">Perbaikan</a>
                                                    @endif
                                                    @endif
                                        </td>

                                        <td style="width: 13%;">
                                            <div class="d-flex">
                                                <a href="/nilai/tambah/{{$n->id}}/{{$i}}/{{$i2}}/{{$semester}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Tambah"><i class="fa fa-plus"></i></a>
                                                <a href="/nilai/detail/{{$i}}/{{$i2}}/{{$n->id}}/{{$semester}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$nilai->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>