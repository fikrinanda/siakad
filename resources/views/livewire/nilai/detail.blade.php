<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Detail Nilai</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                        </div>
                        @if(count($nilai) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Jenis</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($nilai as $key => $n)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$nilai->firstItem() + $key}}</strong></td>
                                        <td style="width: 45%;">{{$n->jenis}}</td>
                                        <td style="width: 45%;">{{$n->nilai}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$nilai->links()}}
                        </div>
                        @else
                        <table class="table table-responsive-md">
                            <thead>
                                <tr>
                                    <th class="width80">No</th>
                                    <th>Jenis</th>
                                    <th>Nilai</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td style="width: 10%;"><strong>1</strong></td>
                                    <td style="width: 45%;">Tugas</td>
                                    <td style="width: 45%;">0</td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;"><strong>1</strong></td>
                                    <td style="width: 45%;">UTS</td>
                                    <td style="width: 45%;">0</td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;"><strong>1</strong></td>
                                    <td style="width: 45%;">UAS</td>
                                    <td style="width: 45%;">0</td>
                                </tr>

                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>