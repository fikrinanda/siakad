<div class="content-body">
    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Import Nilai</h4>
                            </div>
                            <div class="card-body">
                                @if(session('status'))
                                <div class="alert alert-success solid alert-square">
                                    <strong>Berhasil!</strong>
                                    {{session('status')}}
                                </div>
                                @endif

                                @if (isset($errors) && $errors->any())
                                <div>
                                    @foreach ($errors->all() as $error)
                                    <div class="alert alert-warning solid alert-square">
                                        <strong>Gagal!</strong>
                                        {{ $error }}
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                                <div class="basic-form">
                                    <form wire:submit.prevent="import" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">File</label>
                                            <div class="col-sm-9">
                                                <input type="file" style="color: black;" wire:model="file">
                                                <div>
                                                    @error('file')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div wire:loading wire:target="file">Uploading...</div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3 mt-5">Import</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>