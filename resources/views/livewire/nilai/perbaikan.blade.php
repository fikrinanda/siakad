<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Nilai Perbaikan</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$kls}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Mata Pelajaran</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$mpl}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Siswa</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$siswa2}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Semester</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$semester}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nilai</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nilai" wire:model="nilai">
                                                <div>
                                                    @error('nilai')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3 mt-5">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>