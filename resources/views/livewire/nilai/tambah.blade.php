<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Nilai</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$kls}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Mata Pelajaran</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$mpl}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Siswa</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$siswa2}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Semester</label>
                                            <div class="col-sm-9 d-flex align-items-center">
                                                {{$semester}}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Jenis</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="jenis" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Jenis</option>
                                                        <option value="Tugas Rumah">Tugas Rumah</option>
                                                        <option value="Kuis">Kuis</option>
                                                        <option value="UTS">UTS</option>
                                                        <option value="UAS">UAS</option>
                                                        <option value="Perbaikan">Perbaikan</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('jenis')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($nilai as $index => $orderTgs)
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nilai</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" style="color: black;" placeholder="Masukkan nilai" wire:model="nilai.{{$index}}">
                                                <div>
                                                    @error('nilai.' . $index)
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if($jenis == 'Tugas Rumah' || $jenis == 'Kuis')
                                            @if(count($this->nilai) > 1)
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-danger btn-xs mt-3" wire:click.prevent="remove({{$index}})">-</button>
                                            </div>
                                            @endif
                                            @endif
                                        </div>
                                        @endforeach

                                        @if($jenis == 'Tugas Rumah' || $jenis == 'Kuis')
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-secondary btn-sm mt-3" wire:click.prevent="add">+</button>
                                        </div>
                                        @endif

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3 mt-5">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>