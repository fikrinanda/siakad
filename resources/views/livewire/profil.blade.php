<div class="content-body">
    <div class="container-fluid">
        <div class="row gutters-sm">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="/storage/{{$foto}}" alt="Admin" width="300" height="350">
                            <div class="mt-3">
                                <h4>{{$nama}}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3">
                                @if(auth()->user()->level == 'Siswa')
                                <h6 class="mb-0">NISN</h6>
                                @elseif(auth()->user()->level == 'Guru')
                                <h6 class="mb-0">NIP</h6>
                                @endif
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$uname}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Nama</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$nama}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Jenis Kelamin</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$jenis_kelamin}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Tanggal Lahir</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$tanggal_lahir->format('d, F Y')}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Alamat</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                {{$alamat}}
                            </div>
                        </div>
                        <hr>
                        @if(auth()->user()->level == 'Siswa')
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Kelas</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                @foreach($kelas as $k)
                                {{$k->kelas->nama}} - {{$k->kelas->tahun}}
                                <br>
                                @endforeach
                            </div>
                        </div>
                        @elseif(auth()->user()->level == 'Guru')
                        <div class="row">
                            <div class="col-sm-3">
                                <h6 class="mb-0">Mengajar</h6>
                            </div>
                            <div class="col-sm-9 text-secondary">
                                @foreach($mengajar as $m)
                                Kelas {{$m->kelas->nama}} Mata Pelajaran {{$m->mapel->nama}}
                                <br>
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>