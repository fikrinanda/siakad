<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Siswa {{$ket}}</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/siswa/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($siswa) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($siswa as $key => $s)
                                    <tr>
                                        <td style="width: 12%;"><strong>{{$siswa->firstItem() + $key}}</strong></td>
                                        <td style="width: 22%;">{{$s->siswa_id}}</td>
                                        <td style="width: 22%;">{{$s->siswa->user->username}}</td>
                                        <td style="width: 22%;">{{$s->siswa->nama}}</td>
                                        <td style="width: 22%;">
                                            <div class="d-flex">
                                                <a href="/siswa/lihat/{{$s->siswa->user->username}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="#" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-edit"></i></a>
                                                <a href="#" class="btn btn-danger shadow sharp"><i class="fa fa-trash"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$siswa->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>