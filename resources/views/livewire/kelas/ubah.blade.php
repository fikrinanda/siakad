<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Kelas</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-4">
                                                <div wire:ignore>
                                                    <select wire:model="tingkat" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Tingkat</option>
                                                        <option value="7">VII</option>
                                                        <option value="8">VIII</option>
                                                        <option value="9">IX</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('tingkat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div wire:ignore>
                                                    <select wire:model="nama" class="form-control" style="color: black;">
                                                        <option hidden>Pilih Nama</option>
                                                        <option value="A">A</option>
                                                        <option value="B">B</option>
                                                        <option value="C">C</option>
                                                        <option value="D">D</option>
                                                        <option value="E">E</option>
                                                        <option value="F">F</option>
                                                        <option value="G">G</option>
                                                        <option value="H">H</option>
                                                        <option value="I">I</option>
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('nama')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tahun Ajaran</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" style="color: black;" wire:model="tahun" disabled>
                                                <div>
                                                    @error('tahun')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Wali Kelas</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="guru" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Wali Kelas</option>
                                                        @foreach($guru2 as $g)
                                                        <option value="{{$g->id}}">{{$g->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('guru')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>