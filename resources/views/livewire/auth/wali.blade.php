<div>
    <div class="page-links">
        <a href="/">Login</a>
        <a href="/login/guru">Login guru</a>
        <a href="/login/wali" class="active">Login wali kelas</a>
    </div>
    @if(session()->get('error'))
    <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">
        {{ session('error') }}
    </div>
    @endif
    <form wire:submit.prevent="login" autocomplete="off">
        <input class="form-control" type="text" wire:model="username" placeholder="Username" required>
        <input class="form-control" type="password" wire:model="password" placeholder="Password" required>
        <div class="form-button">
            <button id="submit" type="submit" class="ibtn">Login</button>
        </div>
    </form>
</div>