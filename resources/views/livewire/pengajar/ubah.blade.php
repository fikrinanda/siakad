<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Ubah Pengajar</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="ubah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="kls" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Kelas</option>
                                                        @foreach($kelas as $k)
                                                        <option value="{{$k->id}}">{{$k->nama}} - {{$k->tahun}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('kls')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Mata Pelajaran</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="mp" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Mata Pelajaran</option>
                                                        @foreach($mapel as $m)
                                                        <option value="{{$m->id}}">{{$m->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('mp')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Guru</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="gr" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Guru</option>
                                                        @foreach($guru as $g)
                                                        <option value="{{$g->id}}">{{$g->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('gr')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Ubah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>