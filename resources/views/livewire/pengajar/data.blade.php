<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        @if(auth()->user()->level == 'Admin')
                        <h4 class="card-title">Data Pengajar</h4>
                        @elseif(auth()->user()->level == 'Guru')
                        <h4 class="card-title">Data Mengajar</h4>
                        @endif
                        @if(auth()->user()->level == 'Admin')
                        <div class="d-sm-flex align-items-center">
                            <a href="/pengajar/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between" wire:ignore>
                            <div class="dataTables_length"><label>Show <select wire:model="perPage">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label></div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($pengajar) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Kelas</th>
                                        <th>Mata Pelajaran</th>
                                        <th>Guru</th>
                                        <th>Tahun</th>
                                        @if(auth()->user()->level == 'Admin')
                                        <th>Aksi</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pengajar as $key => $p)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$pengajar->firstItem() + $key}}</strong></td>
                                        <td style="width: 18%;">{{$p->kelas->nama}}</td>
                                        <td style="width: 18%;">{{$p->mapel->nama}}</td>
                                        <td style="width: 18%;">{{$p->guru->nama}}</td>
                                        <td style="width: 18%;">{{$p->kelas->tahun}}</td>
                                        @if(auth()->user()->level == 'Admin')
                                        <td style="width: 18%;">
                                            <div class="d-flex">
                                                <a href="/pengajar/ubah/{{$p->kelas_id}}/{{$p->mapel_id}}/{{$p->guru_id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$p->kelas_id}}','{{$p->mapel_id}}','{{$p->guru_id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$pengajar->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>