<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Tenggat</h4>
                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form wire:submit.prevent="tambah" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Soal</label>
                                            <div class="col-sm-9">
                                                <div wire:ignore>
                                                    <select wire:model="s" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Soal</option>
                                                        @foreach($soal as $s)
                                                        <option value="{{$s->id}}">{{$s->jenis}} - {{$s->mapel->nama}} - {{$s->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('s')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9">
                                                <div>
                                                    <select wire:model="k" class="form-control" style="color: black;" data-live-search="true">
                                                        <option hidden>Pilih Kelas</option>
                                                        @foreach($kelas as $k)
                                                        <option value="{{$k->kelas_id}}">{{$k->kelas->nama}} - {{$k->kelas->tahun}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    @error('k')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tenggat</label>
                                            <div class="col-sm-9">
                                                <input type="datetime-local" class="form-control" style="color: black;" wire:model="tenggat" min="{{$tgl}}">
                                                <div>
                                                    @error('tenggat')
                                                    <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-end">
                                            <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>