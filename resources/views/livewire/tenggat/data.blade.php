<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Tenggat</h4>
                        <div class="d-sm-flex align-items-center">
                            <a href="/tugas/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between mb-3" wire:ignore>
                            <div>
                                <label>Show <select wire:model="perPage" class="mr-3">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>
                                <label>Kelas <select wire:model="kls">
                                        @foreach($kelas as $k)
                                        <option value="{{$k->kelas->id}}">{{$k->kelas->nama}} - {{$k->kelas->tahun}}</option>
                                        @endforeach
                                    </select></label>
                            </div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($tenggat) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Soal</th>
                                        <th>Jenis</th>
                                        <th>Kelas</th>
                                        <th>Tahun</th>
                                        <th>Tenggat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tenggat as $key => $t)
                                    <tr>
                                        <td style="width: 10%;"><strong>{{$tenggat->firstItem() + $key}}</strong></td>
                                        <td style="width: 16%;">{{$t->soal->nama}}</td>
                                        <td style="width: 10%;">{{$t->soal->jenis}}</td>
                                        <td style="width: 16%;">{{$t->kelas->nama}}</td>
                                        <td style="width: 16%;">{{$t->kelas->tahun}}</td>
                                        <td style="width: 16%;">{{$t->tenggat->format('d, F Y H:i:s')}}</td>
                                        <td style="width: 16%;">
                                            <div class="d-flex">
                                                <a href="/tugas/lihat/{{$t->soal_id}}/{{$t->kelas_id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/tugas/ubah/{{$t->soal_id}}/{{$t->kelas_id}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$t->soal_id}}','{{$t->kelas_id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$tenggat->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>