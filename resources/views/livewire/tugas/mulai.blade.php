<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <form wire:submit.prevent="jawab" autocomplete="off">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>{{$sl->jenis}} - {{$sl->nama}}</h4>
                            <h4>Tenggat : {{$teng->tenggat->format('d, M Y')}} Jam {{$teng->tenggat->format('H:i')}}</h4>
                            <br>
                            <h4>{{$sl->keterangan}}</h4>
                            <br>
                            @if(count($materi) > 0)
                            <h4>Materi</h4>
                            <div class="list-icons">
                                <ul class="list-icons">
                                    @foreach($materi as $m)
                                    @if($m->file != null)
                                    <li><a href="/unduh/{{$m->id}}" style="font-weight: bold;"><span class="align-middle mr-2"><i class="fa fa-chevron-right"></i></span>{{$m->nama}}</a></li>
                                    @endif
                                    @if($m->link != null)
                                    <h5>{{$m->link}}</h5>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @foreach($soal as $index => $s)
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title" style="color: black;">
                                {{$loop->iteration}}. {{$s->pertanyaan}}
                            </h5>
                        </div>
                        <div class="ml-4">
                            @if(!is_null($s->pertanyaan_gambar))
                            <img src="/storage/{{$s->pertanyaan_gambar}}" width="300" height="200">
                            @endif
                        </div>
                        <div class="card-body mb-0" style="color:black">
                            @if($s->jawaban == 'Uraian')
                            <div>
                                <textarea class="form-control" rows="4" wire:model="jawaban.{{$index}}" style="color: black; resize: none;" placeholder="Masukkan jawaban"></textarea>
                                <div>
                                    @error('jawaban.' . $index)
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                            </div>
                            @else

                            @foreach($pil[$index] as $itzy => $midzy)
                            @if(!is_null($midzy[0]) || !is_null($midzy[1]))
                            <div class="mt-3">
                                <div class="radio">
                                    <label>
                                        <input type="radio" wire:model="jawaban.{{$index}}" value="{{$midzy[2]}}" style="height:25px; width:25px; vertical-align: middle;">
                                        &nbsp; &nbsp;{{$midzy[0]}}
                                    </label>
                                </div>
                            </div>
                            @endif
                            @if(!is_null($midzy[1]))
                            <div>
                                <img src="/storage/{{$midzy[1]}}" width="300" height="200">
                            </div>
                            @endif

                            @endforeach

                            <div>
                                @error('jawaban.' . $index)
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            @endif

                        </div>
                        <!-- <div class="card-footer bg-transparent border-3" style="color: black;">Last updateed 3 min ago
                        </div> -->
                    </div>
                </div>
                @endforeach

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="d-flex justify-content-between">
                                <div>
                                    @if($sl->bobot_pilgan > 0)
                                    <h5>Bobot Pilihan Ganda : {{$sl->bobot_pilgan}} %</h5>
                                    @endif
                                    @if($sl->bobot_uraian > 0)
                                    <h5>Bobot Uraian : {{$sl->bobot_uraian}} %</h5>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary ml-3">Serahkan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>