<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <style>
                #h:hover {
                    text-decoration: underline;
                }
            </style>
            @foreach($data as $key2 => $d)
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header bg-info">
                        <h5 class="card-title text-white">
                            <a href="/mapel/{{$d['kelas_id']}}/{{$d['id']}}" style="color:white" id="h">
                                <div>
                                    {{$d['nama']}}
                                </div>
                                <div class="mt-2">
                                    Kelas {{$d['kelas']}}
                                </div>
                            </a>
                            <div class="mt-2">
                                <?php
                                $awal = ["07.00", "07.40", "08.20", "09.00", "10.00", "10.40", "11.20", "13.00", "13.40", "14.20"];
                                $akhir = ["07.40", "08.20", "09.00", "09.40", "10.40", "11.20", "12.00", "13.40", "14.20", "15.00"];
                                $nona = App\Models\Jadwal::where('kelas_id', $d['kelas_id'])->where('mapel_id', $d['id']);
                                if ($nona->exists()) {
                                    $salon = $nona->first();
                                    $bobrok = explode("-", $salon->jam);
                                    echo $salon->hari . " " . $awal[$bobrok[0]] . " - " . $akhir[$bobrok[1]];
                                }
                                ?>
                            </div>
                            <div class="mt-2">
                                <?php
                                $markonah = App\Models\Pengajar::where('kelas_id', $d['kelas_id'])->where('mapel_id', $d['id']);
                                if ($markonah->exists()) {
                                    echo "Guru " . $markonah->first()->guru->nama;
                                }
                                ?>
                            </div>
                        </h5>

                    </div>
                    <div class="card-body mb-0" style="color:black">
                        @foreach($tenggat as $key => $te)
                        @if(!App\Models\Jawaban::where('siswa_id', auth()->user()->siswa->id)->where('soal_id', $te->soal_id)->exists())
                        @if($te->tenggat > $tgl)
                        @if(($d['id'] == $te->soal->mapel_id) && ($te->soal->jenis != 'Perbaikan'))
                        <div>
                            Tenggat : {{$te->tenggat->format('d, M Y')}} Jam {{$te->tenggat->format('H:i')}}
                        </div>
                        @endif
                        @if (($d['id'] == $te->soal->mapel_id) && ($te->soal->jenis != 'Perbaikan'))
                        <div class="mb-3">
                            <a href="/tugas/{{$te->soal_id}}/{{$te->kelas_id}}" style="color:black" id="h">
                                {{$te->soal->jenis}} - {{$te->soal->nama}}
                            </a>
                        </div>
                        @endif

                        @if(($d['id'] == $te->soal->mapel_id) && (70 > $map[$key2]['total']) && ($te->soal->jenis == 'Perbaikan'))
                        <div>
                            Tenggat : {{$te->tenggat->format('d, M Y')}} Jam {{$te->tenggat->format('H:i')}}
                        </div>
                        @endif
                        @if (($d['id'] == $te->soal->mapel_id) && (70 > $map[$key2]['total']) && ($te->soal->jenis == 'Perbaikan'))
                        <div class="mb-3">
                            <a href="/tugas/{{$te->soal_id}}/{{$te->kelas_id}}" style="color:black" id="h">
                                {{$te->soal->jenis}} - {{$te->soal->nama}}
                            </a>
                        </div>
                        @endif
                        @endif
                        @endif
                        @endforeach
                    </div>
                    <!-- <div class="card-footer bg-transparent border-3" style="color: black;">Last updateed 3 min ago
                    </div> -->
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>