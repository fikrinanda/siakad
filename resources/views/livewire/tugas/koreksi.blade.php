<div class="content-body">
    <div class="container-fluid">
        <form wire:submit.prevent="koreksi" autocomplete="off">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h3>{{$nama}}</h3>
                        </div>
                    </div>
                </div>
                @foreach($soal as $index => $data)
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title" style="color: black;">
                                {{$loop->iteration}}. {{$data->pertanyaan}}
                            </h5>
                        </div>
                        <div class="ml-4">
                            @if(!is_null($data->pertanyaan_gambar))
                            <img src="/storage/{{$data->pertanyaan_gambar}}" width="300" height="200">
                            @endif
                        </div>
                        <div class="card-body">


                            @if($data->jawaban == 'Uraian')
                            @php
                            $j = App\Models\Jawaban::where('hash', $data->hash)->where('siswa_id', $siswa_id)->first()->jawaban;
                            @endphp
                            <div>
                                <textarea class="form-control" rows="4" style="color: black; resize: none;" disabled>{{$j}}</textarea>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label class="col-sm-1 col-form-label">Nilai</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" style="color: black;" wire:model="mak.{{$h}}.0" placeholder="Masukkan nilai">
                                </div>
                                <div class="col-sm-3">
                                    @error('mak.' . $h . '.0')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <?php
                            $h++;
                            ?>
                            @else
                            @php
                            $j = App\Models\Jawaban::where('hash', $data->hash)->where('siswa_id', $siswa_id)->first()->jawaban;
                            @endphp
                            @if(!is_null($data->pilihan_a) || !is_null($data->pilihan_a_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'a' ? 'checked' : ''}}> {{$data->pilihan_a}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'a'))
                                    <i class="fas fa-check"></i>
                                    @elseif($j == 'a')
                                    <i class="fas fa-times"></i>
                                    @elseif($data->jawaban == 'a')
                                    <i class="fas fa-check"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_a_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_a_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_b) || !is_null($data->pilihan_b_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'b' ? 'checked' : ''}}> {{$data->pilihan_b}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'b'))
                                    <i class="fas fa-check"></i>
                                    @elseif($j == 'b')
                                    <i class="fas fa-times"></i>
                                    @elseif($data->jawaban == 'b')
                                    <i class="fas fa-check"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_b_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_b_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_c) || !is_null($data->pilihan_c_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'c' ? 'checked' : ''}}> {{$data->pilihan_c}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'c'))
                                    <i class="fas fa-check"></i>
                                    @elseif($j == 'c')
                                    <i class="fas fa-times"></i>
                                    @elseif($data->jawaban == 'c')
                                    <i class="fas fa-check"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_c_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_c_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_d) || !is_null($data->pilihan_d_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'd' ? 'checked' : ''}}> {{$data->pilihan_d}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'd'))
                                    <i class="fas fa-check"></i>
                                    @elseif($j == 'd')
                                    <i class="fas fa-times"></i>
                                    @elseif($data->jawaban == 'd')
                                    <i class="fas fa-check"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_d_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_d_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_e) || !is_null($data->pilihan_e_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'e' ? 'checked' : ''}}> {{$data->pilihan_e}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'e'))
                                    <i class="fas fa-check"></i>
                                    @elseif($j == 'e')
                                    <i class="fas fa-times"></i>
                                    @elseif($data->jawaban == 'e')
                                    <i class="fas fa-check"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_e_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_e_gambar}}" width="300" height="200">
                            </div>
                            @endif

                            <br>
                            @endif


                        </div>
                    </div>
                </div>
                @endforeach

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Nilai Pilihan Ganda</h5>
                                <div class="col-sm-2">
                                    <h5 style="color: black;">{{$pilgan}} / {{$jumlah_pilgan}} X {{$bobot_pilgan}} = {{$total_pilgan}}</h5>
                                </div>
                            </div>
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Nilai Uraian</h5>
                                <div class="col-sm-2">
                                    <h5 style="color: black;">{{$uraian}} / 100 X {{$bobot_uraian}} = {{$total_uraian}}</h5>
                                </div>
                            </div>
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Total</h5>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" style="color: black;" wire:model="total" disabled>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary ml-3">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>