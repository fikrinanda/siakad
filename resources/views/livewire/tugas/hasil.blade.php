<div class="content-body">
    <div class="container-fluid">
        <form wire:submit.prevent="koreksi" autocomplete="off">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h3>{{$nama}}</h3>
                        </div>
                    </div>
                </div>
                @if(App\Models\Jawaban::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->exists())
                @foreach($soal as $index => $data)
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title" style="color: black;">
                                {{$loop->iteration}}. {{$data->pertanyaan}}
                            </h5>
                        </div>
                        <div class="ml-4">
                            @if(!is_null($data->pertanyaan_gambar))
                            <img src="/storage/{{$data->pertanyaan_gambar}}" width="300" height="200">
                            @endif
                        </div>
                        <div class="card-body">


                            @if($data->jawaban == 'Uraian')
                            @php
                            $j = App\Models\Jawaban::where('hash', $data->hash)->where('siswa_id', auth()->user()->siswa->id)->first()->jawaban;
                            @endphp
                            <div>
                                <textarea class="form-control" rows="4" style="color: black; resize: none;" disabled>{{$j}}</textarea>
                            </div>
                            <br>
                            <?php
                            $h++;
                            ?>
                            @else
                            @php
                            $j = App\Models\Jawaban::where('hash', $data->hash)->where('siswa_id', auth()->user()->siswa->id)->first()->jawaban;
                            @endphp
                            @if(!is_null($data->pilihan_a) || !is_null($data->pilihan_a_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'a' ? 'checked' : ''}}> {{$data->pilihan_a}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'a'))
                                    <i class="fa fa-check"></i>
                                    @elseif($j == 'a')
                                    <i class="fa fa-times"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_a_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_a_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_b) || !is_null($data->pilihan_b_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'b' ? 'checked' : ''}}> {{$data->pilihan_b}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'b'))
                                    <i class="fa fa-check"></i>
                                    @elseif($j == 'b')
                                    <i class="fa fa-times"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_b_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_b_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_c) || !is_null($data->pilihan_c_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'c' ? 'checked' : ''}}> {{$data->pilihan_c}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'c'))
                                    <i class="fa fa-check"></i>
                                    @elseif($j == 'c')
                                    <i class="fa fa-times"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_c_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_c_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_d) || !is_null($data->pilihan_d_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'd' ? 'checked' : ''}}> {{$data->pilihan_d}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'd'))
                                    <i class="fa fa-check"></i>
                                    @elseif($j == 'd')
                                    <i class="fa fa-times"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_d_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_d_gambar}}" width="300" height="200">
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_e) || !is_null($data->pilihan_e_gambar))
                            <div class="my-3">
                                <div class="radio">
                                    <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;" {{$j == 'e' ? 'checked' : ''}}> {{$data->pilihan_e}}</label>
                                    @if(($j == $data->jawaban) && ($j == 'e'))
                                    <i class="fa fa-check"></i>
                                    @elseif($j == 'e')
                                    <i class="fa fa-times"></i>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(!is_null($data->pilihan_e_gambar))
                            <div>
                                <img src="/storage/{{$data->pilihan_e_gambar}}" width="300" height="200">
                            </div>
                            @endif

                            <br>
                            @endif


                        </div>
                    </div>
                </div>
                @endforeach

                @else
                @foreach($soal as $index => $data)
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title" style="color: black;">
                                {{$loop->iteration}}. {{$data->pertanyaan}}
                            </h5>
                        </div>
                        <div class="card-body">
                            @if($data->jawaban == 'Uraian')
                            <div>
                                <textarea class="form-control" rows="4" style="color: black; resize: none;" disabled></textarea>
                            </div>
                            <br>
                            <?php
                            $h++;
                            ?>
                            @else
                            <div class="radio">
                                <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;"> {{$data->pilihan_a}}</label>
                                @if($data->jawaban == 'a')
                                <i class="fa fa-check"></i>
                                @endif
                            </div>
                            <div class="radio mb-2">
                                <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;"> {{$data->pilihan_b}}</label>
                                @if($data->jawaban == 'b')
                                <i class="fa fa-check"></i>
                                @endif
                            </div>
                            <div class="radio mb-2">
                                <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;"> {{$data->pilihan_c}}</label>
                                @if($data->jawaban == 'c')
                                <i class="fa fa-check"></i>
                                @endif
                            </div>
                            <div class="radio mb-2">
                                <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;"> {{$data->pilihan_d}}</label>
                                @if($data->jawaban == 'd')
                                <i class="fa fa-check"></i>
                                @endif
                            </div>
                            <div class="radio mb-2">
                                <label><input type="radio" name="pilihan{{$index}}" disabled style="height:25px; width:25px; vertical-align: middle;"> {{$data->pilihan_e}}</label>
                                @if($data->jawaban == 'e')
                                <i class="fa fa-check"></i>
                                @endif
                            </div>

                            <br>
                            @endif


                        </div>
                    </div>
                </div>
                @endforeach
                @endif

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Nilai Pilihan Ganda</h5>
                                <div class="col-sm-2">
                                    <h5 style="color: black;">{{$p}}</h5>
                                </div>
                            </div>
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Nilai Uraian</h5>
                                <div class="col-sm-2">
                                    <h5 style="color: black;">{{$g}}</h5>
                                </div>
                            </div>
                            <div class="form-group row">
                                <h5 class="col-sm-2" style="color: black;">Total</h5>
                                <div class="col-sm-2">
                                    <h5 style="color: black;">{{$p + $g}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>