<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lihat Tugas {{$oncom}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between mb-3" wire:ignore>
                            <label>Show <select wire:model="perPage" class="mr-3">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                </select></label>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($sis) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Nama</th>
                                        <th>Tanggal Menyerahkan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sis as $key => $s)
                                    <tr>
                                        <td style="width: 30%;"><strong>{{$sis->firstItem() + $key}}</strong></td>
                                        <td style="width: 30%;">{{$s->siswa->nama}}</td>
                                        <td style="width: 30%;">{{$s->created_at->format('d, F Y')}} Jam {{$s->created_at->format('H:i:s')}}</td>
                                        <td style="width: 10%;">
                                            <div class="d-flex">
                                                <a href="/tugas/koreksi/{{$s->soal_id}}/{{$s->kelas_id}}/{{$s->siswa_id}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$sis->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Hasil</h4>
                        <div class="d-sm-flex align-items-center">
                            @if($once == 'Tidak')
                            <button wire:click="bagikan" class="btn btn-outline-primary rounded mr-3">Bagikan Hasil</button>
                            @elseif($once == 'Ya')
                            <button wire:click="batalkan" class="btn btn-outline-danger rounded mr-3">Batalkan Bagikan Hasil</button>
                            @endif
                            <button wire:click="nilai" class="btn btn-outline-primary rounded">Nilai</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between mb-3" wire:ignore>
                            <label>Show <select wire:model="perPage2" class="mr-3">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="20">20</option>
                                </select></label>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search2"></label></div>
                        </div>
                        @if(count($hasil) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        <th>Nama</th>
                                        <th>Pilihan Ganda</th>
                                        <th>Uraian</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($hasil as $key => $h)
                                    <tr>
                                        <td style="width: 20%;"><strong>{{$hasil->firstItem() + $key}}</strong></td>
                                        <td style="width: 20%;">{{$h->siswa->nama}}</td>
                                        <td style="width: 20%;">{{$h->pilihan_ganda}}</td>
                                        <td style="width: 20%;">{{$h->uraian}}</td>
                                        <td style="width: 20%;">{{$h->total}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$hasil->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>