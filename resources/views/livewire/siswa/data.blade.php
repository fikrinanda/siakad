<div class="content-body">
    <div class="container-fluid">
        <!-- Vectormap -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Siswa</h4>
                        @if(auth()->user()->level == 'Admin')
                        <div class="d-sm-flex align-items-center">
                            <a href="/siswa/tambah" class="btn btn-outline-primary rounded">Tambah</a>
                        </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dataTables_wrapper d-flex justify-content-between mb-3" wire:ignore>
                            <div>
                                <label>Show <select wire:model="perPage" class="mr-3">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                    </select></label>
                                @if(auth()->user()->level == 'Admin')
                                <label>Kelas <select wire:model="kls">
                                        @foreach($kelas as $k)
                                        <option value="{{$k->id}}">{{$k->nama}} - {{$k->tahun}}</option>
                                        @endforeach
                                    </select></label>
                                @elseif(auth()->user()->level == 'Guru')
                                <label>Kelas <select wire:model="kls">
                                        @foreach($kelas as $k)
                                        <option value="{{$k->kelas->id}}">{{$k->kelas->nama}} - {{$k->kelas->tahun}}</option>
                                        @endforeach
                                    </select></label>
                                @endif

                            </div>
                            <div class="dataTables_filter"><label>Search&nbsp;:&nbsp;<input type="search" wire:model="search"></label></div>
                        </div>
                        @if(count($siswa) > 0)
                        <div class="table-responsive">
                            <table class="table table-responsive-md">
                                <thead>
                                    <tr>
                                        <th class="width80">No</th>
                                        @if(auth()->user()->level == 'Admin')
                                        <th>ID</th>
                                        <th>NISN</th>
                                        <th>Nama</th>
                                        @elseif(auth()->user()->level == 'Guru')
                                        <th>NISN</th>
                                        <th>Nama</th>

                                        @endif
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($siswa as $key => $s)
                                    <tr>
                                        <td style="width: 12%;"><strong>{{$siswa->firstItem() + $key}}</strong></td>
                                        @if(auth()->user()->level == 'Admin')
                                        <td style="width: 22%;">{{$s->id}}</td>
                                        <td style="width: 22%;">{{$s->user->username}}</td>
                                        <td style="width: 22%;">{{$s->nama}}</td>
                                        @elseif(auth()->user()->level == 'Guru')
                                        <td style="width: 22%;">{{$s->user->username}}</td>
                                        <td style="width: 22%;">{{$s->nama}}</td>

                                        @endif
                                        <td style="width: 22%;">
                                            <div class="d-flex">
                                                @if(auth()->user()->level == 'Guru')
                                                <a href="/siswa/lihat/{{$s->user->username}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                @elseif(auth()->user()->level == 'Admin')
                                                <a href="/siswa/lihat/{{$s->user->username}}" class="btn btn-success shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-info-circle"></i></a>
                                                <a href="/siswa/ubah/{{$s->user->username}}" class="btn btn-warning shadow sharp mr-1" data-toggle="tooltip" data-placement="top" title="Ubah"><i class="fa fa-edit"></i></a>
                                                <button wire:click="hapus('{{$s->user->id}}')" class="btn btn-danger shadow sharp" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">
                            {{$siswa->links()}}
                        </div>
                        @else
                        <h1>Tidak ada data</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>