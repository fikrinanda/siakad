<div class="content-body">
    <div class="container-fluid">
        <div class="form-head d-flex mb-3 align-items-start justify-content-between">
            <div class="mr-auto">
                <h2 class="text-black font-w600 mb-0">Sistem Informasi SMPN 1 Ngantru</h2>
                <p class="mb-0">Selamat Datang Siswa {{auth()->user()->siswa->nama}}</p>
            </div>
            <div wire:ignore>
                <div><label>Filter tahun <select wire:model="thn" class="default-select">
                            @foreach($tahun as $t)
                            <option value="{{$t->kelas->tahun}}">{{$t->kelas->tahun}}</option>
                            @endforeach
                        </select></label></div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">

                <h3>Tahun Ajaran {{$thn}}</h3>

            </div>
        </div>

        <div class="row">
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <span class="mr-3 bgl-primary text-primary">
                                <svg width="36" height="28" viewBox="0 0 640 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill="#2F4CDD" d="M208 352c-2.39 0-4.78.35-7.06 1.09C187.98 357.3 174.35 360 160 360c-14.35 0-27.98-2.7-40.95-6.91-2.28-.74-4.66-1.09-7.05-1.09C49.94 352-.33 402.48 0 464.62.14 490.88 21.73 512 48 512h224c26.27 0 47.86-21.12 48-47.38.33-62.14-49.94-112.62-112-112.62zm-48-32c53.02 0 96-42.98 96-96s-42.98-96-96-96-96 42.98-96 96 42.98 96 96 96zM592 0H208c-26.47 0-48 22.25-48 49.59V96c23.42 0 45.1 6.78 64 17.8V64h352v288h-64v-64H384v64h-76.24c19.1 16.69 33.12 38.73 39.69 64H592c26.47 0 48-22.25 48-49.59V49.59C640 22.25 618.47 0 592 0z"></path>
                                </svg>

                            </span>
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$guru}}</span></h3>
                                <p class="mb-0">Guru</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <span class="mr-3 bgl-primary text-primary">
                                <svg width="36" height="28" viewBox="0 0 448 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill="#2F4CDD" d="M319.4 320.6L224 416l-95.4-95.4C57.1 323.7 0 382.2 0 454.4v9.6c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-9.6c0-72.2-57.1-130.7-128.6-133.8zM13.6 79.8l6.4 1.5v58.4c-7 4.2-12 11.5-12 20.3 0 8.4 4.6 15.4 11.1 19.7L3.5 242c-1.7 6.9 2.1 14 7.6 14h41.8c5.5 0 9.3-7.1 7.6-14l-15.6-62.3C51.4 175.4 56 168.4 56 160c0-8.8-5-16.1-12-20.3V87.1l66 15.9c-8.6 17.2-14 36.4-14 57 0 70.7 57.3 128 128 128s128-57.3 128-128c0-20.6-5.3-39.8-14-57l96.3-23.2c18.2-4.4 18.2-27.1 0-31.5l-190.4-46c-13-3.1-26.7-3.1-39.7 0L13.6 48.2c-18.1 4.4-18.1 27.2 0 31.6z"></path>
                                </svg>
                            </span>
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$siswa}}</span></h3>
                                <p class="mb-0">Siswa</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <span class="mr-3 bgl-primary text-primary">
                                <svg width="36" height="28" viewBox="0 0 448 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill="#2F4CDD" d="M448 360V24c0-13.3-10.7-24-24-24H96C43 0 0 43 0 96v320c0 53 43 96 96 96h328c13.3 0 24-10.7 24-24v-16c0-7.5-3.5-14.3-8.9-18.7-4.2-15.4-4.2-59.3 0-74.7 5.4-4.3 8.9-11.1 8.9-18.6zM128 134c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm0 64c0-3.3 2.7-6 6-6h212c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H134c-3.3 0-6-2.7-6-6v-20zm253.4 250H96c-17.7 0-32-14.3-32-32 0-17.6 14.4-32 32-32h285.4c-1.9 17.1-1.9 46.9 0 64z"></path>
                                </svg>
                            </span>
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$mapel}}</span></h3>
                                <p class="mb-0">Mata Pelajaran</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="widget-stat card">
                    <div class="card-body p-4">
                        <div class="media ai-icon">
                            <span class="mr-3 bgl-primary text-primary">
                                <svg width="36" height="28" viewBox="0 0 640 512" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill="#2F4CDD" d="M0 224v272c0 8.84 7.16 16 16 16h80V192H32c-17.67 0-32 14.33-32 32zm360-48h-24v-40c0-4.42-3.58-8-8-8h-16c-4.42 0-8 3.58-8 8v64c0 4.42 3.58 8 8 8h48c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8zm137.75-63.96l-160-106.67a32.02 32.02 0 0 0-35.5 0l-160 106.67A32.002 32.002 0 0 0 128 138.66V512h128V368c0-8.84 7.16-16 16-16h96c8.84 0 16 7.16 16 16v144h128V138.67c0-10.7-5.35-20.7-14.25-26.63zM320 256c-44.18 0-80-35.82-80-80s35.82-80 80-80 80 35.82 80 80-35.82 80-80 80zm288-64h-64v320h80c8.84 0 16-7.16 16-16V224c0-17.67-14.33-32-32-32z"></path>
                                </svg>
                            </span>
                            <div class="media-body">
                                <h3 class="mb-0 text-black"><span class="counter ml-0">{{$kelas}}</span></h3>
                                <p class="mb-0">Kelas</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>