<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Raport</title>

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            /* border: 1px solid #dddddd; */
            text-align: left;
            padding: 4px;
        }
    </style>
</head>

<body>

    <table width="100%" page-break-inside: auto;>
        <tbody>

            <tr>
                <td>Nama Sekolah</td>
                <td>:</td>
                <td>SMP Negeri 1 Ngantru</td>
                <td>Kelas</td>
                <td>:</td>
                <td>{{$kls}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>Jl. Raya Ngantru No.142</td>
                <td>Semester ke</td>
                <td>:</td>
                <td>{{$iu}}</td>
            </tr>
            <tr>
                <td>Nama Peserta Didik</td>
                <td>:</td>
                <td>{{$nama}}</td>
                <td>Tahun Pelajaran</td>
                <td>:</td>
                <td>{{$tahun}}</td>
            </tr>


        </tbody>
    </table>

    <br>

    <table width="100%" page-break-inside: auto;>
        <thead>
            <tr>
                <th style="border: 1px solid black; text-align: center; padding: 10px;">No.</th>
                <th style="border: 1px solid black; text-align: center; padding: 10px;">Mata Pelajaran</th>
                <th style="border: 1px solid black; text-align: center; padding: 10px;">KKM</th>
                <th style="border: 1px solid black; text-align: center; padding: 10px;">Nilai</th>
                <th style="border: 1px solid black; text-align: center; padding: 10px;">Deskripsi Kemajuan Belajar</th>
            </tr>
        </thead>
        <tbody>
            @foreach($map as $key => $s)
            <tr>
                <td style="border: 1px solid black; padding: 10px; width: 5%; text-align: center;">{{$loop->iteration}}</td>
                <td style="border: 1px solid black; padding: 10px; width: 39%;">{{$s['nama']}}</td>
                <td style="border: 1px solid black; padding: 10px; width: 8%; text-align: center;">70</td>
                <td style="border: 1px solid black; padding: 10px; width: 8%; text-align: center;">{{round($s['total'])}}</td>
                <td style="border: 1px solid black; padding: 10px; width: 40%;">
                    @if(round($s['total']) > 70)
                    Terlampaui
                    @elseif(round($s['total']) == 70)
                    Tercapai
                    @elseif(round($s['total']) < 70) Belum Tercapai @endif </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    <br>

    <table width="100%" page-break-inside: auto; style="width: 50%;">
        <thead>
            <tr>
                <th style="border: 1px solid black; text-align: center;">Ketidakhadiran</th>
            </tr>
        </thead>
        <tbody style="border: 1px solid black;">

            <tr style="border: 1px solid black;">
                <td style="border: 1px solid black;">
                    <table>
                        <tbody>
                            <tr>
                                <td>Sakit</td>
                                <td>:</td>
                                <td>{{$sakit}} hari</td>
                            </tr>
                            <tr>
                                <td>Izin</td>
                                <td>:</td>
                                <td>{{$izin}} hari</td>
                            </tr>
                            <tr>
                                <td>Tanpa Keterangan</td>
                                <td>:</td>
                                <td>{{$tanpa_keterangan}} hari</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

        </tbody>
    </table>

    <div style="display: flex; width: 100%; border: solid 1px white;">
        <div style="margin-top: 50px; width: 200px; height: 150px; float: left;">
            <div style="margin-bottom: 5px; font-family: arial, sans-serif;">Mengetahui</div>
            <div style="font-family: arial, sans-serif; margin-bottom: 100px;">Orang Tua / Wali</div>

            <hr>
        </div>

        <div style="margin-top: 50px; width: 200px; height: 150px; float: right;">
            <div style="margin-bottom: 5px; font-family: arial, sans-serif;">Tulungagung, {{$tanggal}}</div>
            <div style="font-family: arial, sans-serif; margin-bottom: 73px;">Wali Kelas,</div>

            @if(auth()->user()->level == 'Guru')
            <div style="font-family: arial, sans-serif;">{{auth()->user()->guru->nama}}</div>
            <hr>
            <div style="font-family: arial, sans-serif;">NIP. {{auth()->user()->username}}</div>
            @elseif(auth()->user()->level == 'Admin')
            <?php

            $wali = App\Models\Kelas::find($kelas_id)->guru->nama;
            $nip = App\Models\Kelas::find($kelas_id)->guru->user->username;
            ?>
            <div style="font-family: arial, sans-serif;">{{$wali}}</div>
            <hr>
            <div style="font-family: arial, sans-serif;">NIP. {{$nip}}</div>
            @endif

        </div>
    </div>


    <script type="text/php">
        if ( isset($pdf) ) { 
            $pdf->page_script('
        
            $text = sprintf(_("Page %d/%d"),  $PAGE_NUM, $PAGE_COUNT);
            // Uncomment the following line if you use a Laravel-based i18n
            //$text = __("Page :pageNum/:pageCount", ["pageNum" => $PAGE_NUM, "pageCount" => $PAGE_COUNT]);
            $font = null;
            $size = 9;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default

            // Compute text width to center correctly
            $textWidth = $fontMetrics->getTextWidth($text, $font, $size);

            $x = ($pdf->get_width() - $textWidth) / 2;
            $y = $pdf->get_height() - 35;

            $pdf->text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        
    ');
}
</script>
</body>

</html>