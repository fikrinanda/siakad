<!-- Header -->
<div class="header pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h2 class="h2 d-inline-block mb-0">{{$h2}}</h2>
                </div>
                @if(request()->is('guru/data'))
                <div class="col-lg-6 col-5 text-right">
                    <a href="/guru/tambah" class="btn btn-outline-primary">Tambah</a>
                </div>
                @elseif(request()->is('siswa/data'))
                <div class="col-lg-6 col-5 text-right">
                    <a href="/siswa/tambah" class="btn btn-outline-primary">Tambah</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>