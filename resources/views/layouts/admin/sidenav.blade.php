<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  d-flex  align-items-center">
            <a class="navbar-brand" href="../../pages/dashboards/dashboard.html">
                <img src="../../assets/img/brand/blue.png" class="navbar-brand-img" alt="...">
            </a>
            <div class=" ml-auto ">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link{{request()->is('admin') ? ' active' : ''}}" href=" /admin">
                            <i class="fas fa-home{{request()->is('admin') ? ' text-default' : ' text-info'}}" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Dashboards</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{request()->is('guru/data') ? ' active' : ''}}" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="{{request()->is('guru/data') ? 'true' : 'false'}}" aria-controls="navbar-examples">
                            <i class="fas fa-database{{request()->is('guru/data') ? ' text-default' : ' text-info'}}" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Kelola Data</span>
                        </a>
                        <div class="collapse{{request()->is('guru/data') ? ' show' : ''}}" id="navbar-examples">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="/guru/data" class="nav-link{{request()->is('guru/data') ? ' active' : ''}}">
                                        <span class="sidenav-mini-icon"> G </span>
                                        <span class="sidenav-normal"> Data Guru </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/siswa/data" class="nav-link">
                                        <span class="sidenav-mini-icon"> S </span>
                                        <span class="sidenav-normal"> Data Siswa </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/mapel/data" class="nav-link">
                                        <span class="sidenav-mini-icon"> M </span>
                                        <span class="sidenav-normal"> Data Mapel </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/kelas/data" class="nav-link">
                                        <span class="sidenav-mini-icon"> K </span>
                                        <span class="sidenav-normal"> Data Kelas </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">
                            <i class="ni ni-shop text-primary" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Absensi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">
                            <i class="ni ni-shop text-primary" style="font-size: 15px;"></i>
                            <span class="nav-link-text">Raport</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>