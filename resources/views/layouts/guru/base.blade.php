<!DOCTYPE html>
<html :class="{ 'theme-dark': dark }" x-data="data()" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title ?? config('app.name') }}</title>
    <link rel="stylesheet" href="{{mix('css/app.css')}}" />

    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" />
    @livewireStyles
</head>

<body>
    <div class="flex h-screen bg-gray-50 dark:bg-gray-900" :class="{ 'overflow-hidden': isSideMenuOpen }">
        @include('layouts.aside')
        <div class="flex flex-col flex-1 w-full">
            @include('layouts.header')
            @yield('content')
        </div>
    </div>
    @yield('modal')

    <script src="{{ asset(mix('js/app.js')) }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script src="{{asset('js/init-alpine.js')}}"></script>
    <script src="{{asset('js/charts-lines.js')}}"></script>
    <script src="{{asset('js/charts-pie.js')}}"></script>
    <script src="{{asset('js/charts-bars.js')}}"></script>
    <script src="{{asset('js/focus-trap.js')}}"></script>
    @livewireScripts
</body>

</html>