<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>


</head>


<body>
    <div class="row">
        <div class="col-lg-12">
            <div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Nama Sekolah</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>SMP Negeri 1 Ngantru</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Alamat</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class=" mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>Jl. RAYA NGANTRU NO.142</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Nama Peserta Didik</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$nama}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Kelas</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class=" mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$kls}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Semester ke</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$iu}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Tahun Pelajaran</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$tahun}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12">


            <div>
                @if(count($map) > 0)
                <div class="table-responsive">
                    <table class="table table-responsive-md">
                        <thead>
                            <tr>
                                <th style="text-align:center">No</th>
                                <th style="text-align:center">Mata Pelajaran</th>
                                <th style="text-align:center">KKM</th>
                                <th style="text-align:center">Nilai</th>
                                <th style="text-align:center">Deskripsi Kemajuan Belajar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($map as $key => $s)
                            <tr>
                                <td style="width: 10%; padding: 10px;"><strong>{{$loop->iteration}}</strong></td>
                                <td style="width: 20%; padding: 10px;">{{$s['nama']}}</td>
                                <td style="width: 20%; padding: 10px;">70</td>
                                <td style="width: 20%; padding: 10px;">{{round($s['total'])}}</td>
                                <td style="width: 30%; padding: 10px;">
                                    @if(round($s['total']) > 70)
                                    Terlampaui
                                    @elseif(round($s['total']) == 70)
                                    Tercapai
                                    @elseif(round($s['total']) < 70) Belum Tercapai @endif </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <h1>Tidak ada data</h1>
                @endif
            </div>

        </div>
        <div class="col-lg-6">


            <div>
                <h5>Ketidakhadiran :</h5>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Sakit</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class=" mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$sakit}} hari</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class=" mb-3">
                                    <h5>Izin</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class=" mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$izin}} hari</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="mb-3">
                                    <h5>Tanpa Keterangan</h5>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="mb-3">
                                    <h5>:</h5>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <h5>{{$tanpa_keterangan}} hari</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        @if($x)
        <div class="col-lg-6">

            <h5>Ketidakhadiran :</h5>

            <p>Berdasarkan hasil yang telah dicapai pada semester 1 dan 2, peserta didik ditetapkan</p>
            <h5>Naik kelas</h5>
        </div>
        @endif
    </div>

</body>

</html>