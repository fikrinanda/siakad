<?php

namespace App\Exports;

use App\Models\Nilai;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class NilaiExport implements FromArray, ShouldAutoSize, WithHeadings, WithColumnFormatting, WithStrictNullComparison, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */

    private $kelas_id;
    private $mapel_id;
    private $iu;
    private $sapi;
    private $kuis;
    private $pr;
    public $head;

    public function __construct($kelas_id, $mapel_id, $iu)
    {
        $this->kelas_id = $kelas_id;
        $this->mapel_id = $mapel_id;
        $this->iu = $iu;

        $mas = Nilai::select('siswa_id')->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->orderBy('siswa_id', 'asc')->groupBy('siswa_id')->get();

        $mad = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->orderBy('siswa_id', 'asc')->get();
        foreach ($mas as $key => $value) {
            $this->sapi[$value->siswa_id]['no'] = $key + 1;
            $this->sapi[$value->siswa_id]['nisn'] = $value->siswa->user->username;
            $this->sapi[$value->siswa_id]['nama'] = $value->siswa->nama;
            $mad = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->where('siswa_id', $value->siswa_id)->where('jenis', 'Kuis')->get();

            $mad1 = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->where('siswa_id', $value->siswa_id)->where('jenis', 'Tugas Rumah')->get();

            $mad2 = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->where('siswa_id', $value->siswa_id)->get();

            foreach ($mad as $key2 => $value2) {
                $this->sapi[$value->siswa_id]['kuis' . $key2] = $value2->nilai;
            }

            foreach ($mad1 as $key2 => $value2) {
                $this->sapi[$value->siswa_id]['pr' . $key2] = $value2->nilai;
            }

            $rata = 0;
            $uts = 0;
            $uas = 0;
            $no = 0;
            foreach ($mad2 as $key2 => $value2) {
                if ($value2->jenis == 'Tugas Rumah' || $value2->jenis == 'Kuis' || $value2->jenis == 'Tugas') {
                    $no++;
                    $rata += $value2->nilai;
                }
                if ($value2->jenis == 'UTS') {
                    $uts += $value2->nilai;
                }
                if ($value2->jenis == 'UAS') {
                    $uas += $value2->nilai;
                }
            }
            if ($rata == 0 || $no == 0) {
                $this->sapi[$value->siswa_id]['rata'] = 0;
                $this->sapi[$value->siswa_id]['total'] = ($uts + $uas) / 4;
            } else {
                $rata = $rata / $no;
                $this->sapi[$value->siswa_id]['rata'] = round($rata);
                $this->sapi[$value->siswa_id]['uts'] = $uts;
                $this->sapi[$value->siswa_id]['uas'] = $uas;
                $this->sapi[$value->siswa_id]['total'] = round(((2 * $rata) + $uts + $uas) / 4);
            }
            $rata = 0;
            $uts = 0;
            $uas = 0;
            $no = 0;

            $hmm = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->where('siswa_id', $value->siswa_id)->where('jenis', 'Kuis')->count();
            $hmmm = Nilai::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->where('siswa_id', $value->siswa_id)->where('jenis', 'Tugas Rumah')->count();
            $this->kuis = $hmm;
            $this->pr = $hmmm;
        }

        $this->head[0] = 'No';
        $this->head[1] = 'NISN';
        $this->head[2] = 'Nama';

        for ($i = 0; $i < $this->kuis; $i++) {
            $this->head[$i + 3] = 'Kuis ' . ($i + 1);
        }
        for ($i = 0; $i < $this->pr; $i++) {
            $this->head[$this->kuis + 3] = 'Tugas Rumah ' . ($i + 1);
        }

        $umpah = count($this->head);
        $this->head[$umpah + 1] = 'Rata-rata tugas';
        $this->head[$umpah + 2] = 'UTS';
        $this->head[$umpah + 3] = 'UAS';
        $this->head[$umpah + 4] = 'Total';
        // dd($this->sapi, $this->kuis, $this->pr, $this->head);
    }

    public function array(): array
    {
        return $this->sapi;
    }

    public function headings(): array
    {
        return $this->head;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]]
        ];
    }
}
