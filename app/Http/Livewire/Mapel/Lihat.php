<?php

namespace App\Http\Livewire\Mapel;

use App\Models\Mapel;
use App\Models\Pengajar;
use Livewire\Component;

class Lihat extends Component
{
    public $gr;
    public $ket;
    public $perPage = 5;

    public function mount($mapel_id)
    {
        $this->gr = $mapel_id;
        $k = Mapel::find($mapel_id);
        $this->ket = $k->nama;
    }

    public function render()
    {
        $guru = Pengajar::where('mapel_id', $this->gr)->paginate(5);
        return view('livewire.mapel.lihat', compact(['guru']))->extends('layouts.admin', ['title' => 'Lihat Mapel'])->section('content');
    }
}
