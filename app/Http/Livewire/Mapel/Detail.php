<?php

namespace App\Http\Livewire\Mapel;

use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Nilai;
use App\Models\Perbaikan;
use App\Models\Siswa;
use App\Models\Tenggat;
use Carbon\Carbon;
use Livewire\Component;

class Detail extends Component
{
    public $mapel_id;
    public $kelas_id;
    public $nama;
    public $tgl;
    public $n;
    public $tahun;
    public $kls;
    public $iu;
    public $map;
    public $test;

    public function mount($kelas_id, $mapel_id)
    {
        if (Mapel::where('id', $mapel_id)->exists()) {
            $this->test = Perbaikan::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->exists();
            $m = Mapel::find($mapel_id);
            $z = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->first();
            $this->n = $z->kelas->nama . " - " . $z->kelas->tahun;
            $whatIWant = substr($m->nama, strpos($m->nama, "-") + 1);
            // $k = auth()->user()->siswa->kelas_id;
            $k = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->first()->kelas_id;

            if ((($whatIWant == 'VII') && ($k[0] == '7')) || (($whatIWant == 'VIII') && ($k[0] == '8')) || (($whatIWant == 'IX') && ($k[0] == '9'))) {
                $this->nama = $m->nama;
                $this->tgl = Carbon::now()->toDateTimeString();
            } else {
                abort('404');
            }

            $kl = Kelas::find($kelas_id);
            $this->tahun = $kl->tahun;
            if ($kl->nama[0] == '7') {
                $this->kls = 'VII - ' . $kl->nama[1];
            } else if ($kl->nama[0] == '8') {
                $this->kls = 'VIII - ' . $kl->nama[1];
            } else if ($kl->nama[0] == '9') {
                $this->kls = 'IX - ' . $kl->nama[1];
            }

            $mapel = Mapel::get();
            foreach ($mapel as $key => $value) {
                $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
                if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                }
            }

            $bulan = Carbon::now()->format('m');

            if ($bulan <= 6) {
                $this->iu = '2';
            } else if ($bulan >= 7) {
                $this->iu = '1';
            }
            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
            foreach ($this->map as $key => $value) {
                $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $value['id'])->where('siswa_id', auth()->user()->siswa->id)->where('semester', $this->iu)->get();
                foreach ($nilai as $key2 => $value2) {
                    if ($value2->jenis == 'Tugas Rumah' || $value2->jenis == 'Kuis' || $value2->jenis == 'Tugas') {
                        $n++;
                        $tugas += $value2->nilai;
                    }
                    if ($value2->jenis == 'UTS') {
                        $uts += $value2->nilai;
                    }
                    if ($value2->jenis == 'UAS') {
                        $uas += $value2->nilai;
                    }
                }
                if ($tugas == 0 || $n == 0) {
                    $this->map[$key]['tugas'] = 0;
                    $this->map[$key]['total'] = ((2 * 0) + $uts + $uas) / 4;
                } else {
                    $tugas = $tugas / $n;
                    $this->map[$key]['tugas'] = $tugas;
                    $this->map[$key]['uts'] = $uts;
                    $this->map[$key]['uas'] = $uas;
                    $this->map[$key]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
                }

                $tugas = 0;
                $uts = 0;
                $uas = 0;
                $n = 0;
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        // $i = Siswa::find(auth()->user()->siswa->id)->kelas_id[0];
        $i = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $this->kelas_id)->first()->kelas_id[0];

        $mapel = Mapel::get();
        foreach ($mapel as $key => $value) {
            $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
            if (($i == '7') && ($whatIWant == 'VII')) {
                $data[] = [
                    'id' => $value->id,
                    'nama' => $value->nama,
                ];
            } else if (($i == '8') && ($whatIWant == 'VIII')) {
                $data[] = [
                    'id' => $value->id,
                    'nama' => $value->nama,
                ];
            } else if (($i == '9') && ($whatIWant == 'IX')) {
                $data[] = [
                    'id' => $value->id,
                    'nama' => $value->nama,
                ];
            }
        }
        $materi = Materi::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->get();
        $tenggat = Tenggat::where('kelas_id', $this->kelas_id)->get();
        return view('livewire.mapel.detail', compact(['materi', 'tenggat', 'data']))->extends('layouts.siswa', ['title' => 'Mata Pelajaran'])->section('content');
    }
}
