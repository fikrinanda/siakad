<?php

namespace App\Http\Livewire\Mapel;

use App\Models\Mapel;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $mapel = Mapel::find($id);
        $this->showConfirmation($mapel->id, $mapel->nama);
    }

    public function hancur($id)
    {
        $mapel = Mapel::find($id);
        $mapel->delete();
        $this->showModal($mapel->nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Mapel $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Mapel $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $mapel = Mapel::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.mapel.data', compact(['mapel']))->extends('layouts.admin', ['title' => 'Data Mata Pelajaran'])->section('content');
    }
}
