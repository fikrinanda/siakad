<?php

namespace App\Http\Livewire\Mapel;

use App\Models\Mapel;
use Livewire\Component;

class Ubah extends Component
{
    public $nama;
    public $tingkat;
    public $i;
    protected $listeners = ['berhasil'];

    public function mount($mapel_id)
    {
        $mapel = Mapel::find($mapel_id);
        $a = explode("-", $mapel->nama);

        if ($mapel) {
            $this->i = $mapel->id;
            $this->nama = $a[0];
            $this->tingkat = $a[1];
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            'tingkat' => 'required'
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required',
            'tingkat' => 'required'
        ]);

        Mapel::where('id', $this->i)->update([
            'nama' => $this->nama . '-' . $this->tingkat,
            'tingkat' => $this->tingkat,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Mata Pelajaran $this->nama-$this->tingkat berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/mapel/data');
    }

    public function render()
    {
        return view('livewire.mapel.ubah')->extends('layouts.admin', ['title' => 'Ubah Mata Pelajaran'])->section('content');
    }
}
