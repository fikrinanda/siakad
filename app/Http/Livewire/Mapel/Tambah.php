<?php

namespace App\Http\Livewire\Mapel;

use App\Models\Mapel;
use Livewire\Component;

class Tambah extends Component
{
    public $nama;
    public $tingkat;
    protected $listeners = ['berhasil'];

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            'tingkat' => 'required'
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required',
            'tingkat' => 'required'
        ]);

        $x = Mapel::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "MP" . sprintf("%04s", $y);

        Mapel::create([
            'id' => $z,
            'nama' => $this->nama . '-' . $this->tingkat,
            'tingkat' => $this->tingkat,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Mata Pelajaran $this->nama-$this->tingkat berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/mapel/data');
    }

    public function render()
    {
        return view('livewire.mapel.tambah')->extends('layouts.admin', ['title' => 'Tambah Mata Pelajaran'])->section('content');
    }
}
