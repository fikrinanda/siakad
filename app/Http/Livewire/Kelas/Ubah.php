<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Guru;
use App\Models\Kelas;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $nama;
    public $tingkat;
    public $tahun;
    public $th;
    public $guru;
    public $i;
    protected $listeners = ['berhasil', 'sip'];

    public function mount($kelas_id)
    {
        $this->th = Carbon::now()->format('Y');

        $kelas = Kelas::find($kelas_id);

        if ($kelas) {
            $this->i = $kelas->id;
            $this->tingkat = $kelas->nama[0];
            $this->nama = $kelas->nama[1];
            $this->tahun = $kelas->tahun;
            $this->guru = $kelas->guru_id;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            'tingkat' => 'required',
            'guru' => 'required'
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required',
            'tingkat' => 'required',
            'guru' => 'required'
        ]);

        $test = Kelas::where('nama', $this->tingkat . '' . $this->nama)->where('tahun', $this->tahun)->first();
        $test2 = Kelas::where('tahun', $this->tahun)->where('guru_id', $this->guru)->first();

        $kelas2 = Kelas::find($this->i);
        $test3 = Kelas::where('nama', $kelas2->nama[0] . '' . $kelas2->nama[1])->where('tahun', $kelas2->tahun)->exists();
        if ($test) {
            if ($test->id == $this->i) {
                if ($test2) {
                    if ($test2->id == $this->i) {
                        $this->showModal();
                    } else {
                        $g = Guru::find($this->guru)->nama;
                        $this->showModal2("Guru $g sudah menjadi wali kelas di kelas lain");
                    }
                }
            } else {
                $this->showModal2("Data Kelas $this->tingkat" . '' . "$this->nama sudah ada");
            }
        } else {
            if ($test2) {
                if ($test2->id == $this->i) {
                    Kelas::where('id', $this->i)->update([
                        'id' => $this->tingkat . '' . $this->nama . '' . substr($this->th, -2),
                        'guru_id' => $this->guru,
                        'nama' => $this->tingkat . '' . $this->nama,
                        'tahun' => $this->tahun,
                    ]);
                    $this->showModal();
                } else {
                    $g = Guru::find($this->guru)->nama;
                    $this->showModal2("Guru $g sudah menjadi wali kelas di kelas lain");
                }
            } else {
                Kelas::where('id', $this->i)->update([
                    'id' => $this->tingkat . '' . $this->nama . '' . substr($this->th, -2),
                    'guru_id' => $this->guru,
                    'nama' => $this->tingkat . '' . $this->nama,
                    'tahun' => $this->tahun,
                ]);
                $this->showModal();
            }
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kelas $this->nama berhasil diubah",
        ]);
    }

    public function showModal2($text)
    {
        $this->emit('swal:modal2', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kelas/data');
    }

    public function sip()
    {
    }

    public function render()
    {
        $guru2 = Guru::get();
        return view('livewire.kelas.ubah', compact(['guru2']))->extends('layouts.admin', ['title' => 'Ubah Kelas'])->section('content');
    }
}
