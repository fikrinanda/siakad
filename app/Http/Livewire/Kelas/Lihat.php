<?php

namespace App\Http\Livewire\Kelas;

use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Siswa;
use Livewire\Component;

class Lihat extends Component
{
    public $sw;
    public $ket;

    public function mount($kelas_id)
    {
        $this->sw = $kelas_id;
        $k = Kelas::find($kelas_id);
        $this->ket = 'Kelas ' . $k->nama . ' Tahun ' . $k->tahun;
    }

    public function render()
    {
        $siswa = DetailKelas::where('kelas_id', $this->sw)->paginate(5);
        return view('livewire.kelas.lihat', compact(['siswa']))->extends('layouts.admin', ['title' => 'Lihat Kelas'])->section('content');
    }
}
