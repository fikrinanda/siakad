<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Kelas;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $kelas = Kelas::find($id);
        $this->showConfirmation($kelas->id, $kelas->nama . ' - ' . $kelas->tahun);
    }

    public function hancur($id)
    {
        $kelas = Kelas::find($id);
        $kelas->delete();
        $this->showModal($kelas->nama . ' - ' . $kelas->tahun);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kelas $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Kelas $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $kelas = Kelas::where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.kelas.data', compact(['kelas']))->extends('layouts.admin', ['title' => 'Data Kelas'])->section('content');
    }
}
