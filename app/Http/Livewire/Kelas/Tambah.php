<?php

namespace App\Http\Livewire\Kelas;

use App\Models\Guru;
use App\Models\Kelas;
use Carbon\Carbon;
use Livewire\Component;

class Tambah extends Component
{
    public $nama;
    public $tingkat;
    public $tahun;
    public $th;
    public $guru;
    protected $listeners = ['berhasil', 'sip'];

    public function mount()
    {
        $this->tahun = Carbon::now()->format('Y') . '/' . (Carbon::now()->format('Y') + 1);
        $this->th = Carbon::now()->format('Y');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            'tingkat' => 'required',
            'guru' => 'required'
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required',
            'tingkat' => 'required',
            'guru' => 'required'
        ]);

        $test = Kelas::where('nama', $this->tingkat . '' . $this->nama)->where('tahun', $this->tahun)->exists();
        $test2 = Kelas::where('tahun', $this->tahun)->where('guru_id', $this->guru)->exists();
        if ($test) {
            $this->showModal2("Data Kelas $this->tingkat" . '' . "$this->nama sudah ada");
        } else {
            if ($test2) {
                $g = Guru::find($this->guru)->nama;
                $this->showModal2("Guru $g sudah menjadi wali kelas di kelas lain");
            } else {
                Kelas::create([
                    'id' => $this->tingkat . '' . $this->nama . '' . substr($this->th, -2),
                    'guru_id' => $this->guru,
                    'nama' => $this->tingkat . '' . $this->nama,
                    'tahun' => $this->tahun,
                ]);
                $this->showModal();
            }
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Kelas $this->tingkat" . '' . "$this->nama berhasil ditambahkan",
        ]);
    }

    public function showModal2($text)
    {
        $this->emit('swal:modal2', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => $text,
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/kelas/data');
    }

    public function sip()
    {
    }

    public function render()
    {
        $guru2 = Guru::get();
        return view('livewire.kelas.tambah', compact(['guru2']))->extends('layouts.admin', ['title' => 'Tambah Kelas'])->section('content');
    }
}
