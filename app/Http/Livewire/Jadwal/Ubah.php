<?php

namespace App\Http\Livewire\Jadwal;

use App\Models\Guru;
use App\Models\Jadwal;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Pengajar;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $jadwal;
    public $hari;
    public $kelas;
    public $mapel;
    public $guru;
    public $jam1;
    public $jam2;
    public $awal;
    public $akhir;
    public $shin;
    public $ryujin;
    public $a = 0;
    public $b = 0;
    public $mpl;
    public $i;
    protected $listeners = ['berhasil'];

    public function mount($id)
    {
        $test = Jadwal::where('id', $id);
        if ($test->exists()) {
            $this->i = $id;
            $this->awal = ["07.00", "07.40", "08.20", "09.00", "10.00", "10.40", "11.20", "13.00", "13.40", "14.20"];
            $this->akhir = ["07.40", "08.20", "09.00", "09.40", "10.40", "11.20", "12.00", "13.40", "14.20", "15.00"];
            $this->shin = count($this->awal);
            $this->ryujin = count($this->akhir);
            $sip = $test->first();
            $this->hari = $sip->hari;
            $this->kelas = $sip->kelas_id;
            $this->mapel = $sip->mapel_id;
            $this->guru = $sip->guru->nama;
            $oke = explode("-", $sip->jam);
            $this->jam1 = $oke[0];
            $this->jam2 = $oke[1];

            if (isset($this->kelas[0])) {
                if ($this->kelas[0] == '7') {
                    $k = "VII";
                } else if ($this->kelas[0] == '8') {
                    $k = "VIII";
                } else if ($this->kelas[0] == '9') {
                    $k = "IX";
                }
            } else {
                $k = null;
            }
            $this->mpl = Mapel::where('nama', 'like', '%' . $k  . '%')->get();
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'hari' => 'required|regex:/^([^0-9]*)$/',
            'mapel' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
        ]);
    }

    public function updatedMapel()
    {
        if ($this->kelas != null) {
            $test = Pengajar::where('kelas_id', $this->kelas)->where('mapel_id', $this->mapel);
            if ($test->exists()) {
                $this->guru = $test->first()->guru->nama;
            } else {
                $this->guru = null;
            }
        }
    }

    public function updatedJam1()
    {
        if ($this->jam1 > 9) {
            $this->jam1 = 0;
        } else {
            $this->b = $this->jam1 + 1;
        }
    }

    public function updatedJam2()
    {
        if ($this->jam2 > 9) {
            $this->jam2 = 0;
        } else {
            $this->shin = $this->jam2;
        }
    }

    public function ubah()
    {
        $test = Pengajar::where('kelas_id', $this->kelas)->where('mapel_id', $this->mapel);
        if ($test->exists()) {
            $this->guru = $test->first()->guru_id;
        } else {
            $this->guru = null;
        }

        $this->validate([
            'hari' => 'required|regex:/^([^0-9]*)$/',
            'mapel' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
        ]);

        Jadwal::where('id', $this->i)->update([
            'hari' => $this->hari,
            'mapel_id' => $this->mapel,
            'guru_id' => $this->guru,
            'jam' => $this->jam1 . '-' . $this->jam2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Jadwal berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/jadwal/lihat/$this->kelas");
    }

    public function render()
    {
        return view('livewire.jadwal.ubah')->extends('layouts.admin', ['title' => 'Ubah Jadwal'])->section('content');
    }
}
