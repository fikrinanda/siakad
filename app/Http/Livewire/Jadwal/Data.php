<?php

namespace App\Http\Livewire\Jadwal;

use App\Models\Kelas;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $thn = '';
    protected $paginationTheme = 'bootstrap';

    public function render()
    {

        $kelas = Kelas::where('nama', 'like', '%' . $this->search . '%')->where('tahun', 'like', '%' . $this->thn . '%')->paginate($this->perPage);
        $tahun = Kelas::select('tahun')->groupBy('tahun')->get();
        return view('livewire.jadwal.data', compact(['kelas', 'tahun']))->extends('layouts.admin', ['title' => 'Data Jadwal'])->section('content');
    }
}
