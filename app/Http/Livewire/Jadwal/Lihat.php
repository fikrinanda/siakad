<?php

namespace App\Http\Livewire\Jadwal;

use App\Models\Jadwal;
use App\Models\Kelas;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    public $kelas_id;
    public $search = '';
    public $perPage = 5;
    public $awal;
    public $akhir;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount($kelas_id)
    {
        $this->awal = ["07.00", "07.40", "08.20", "09.00", "10.00", "10.40", "11.20", "13.00", "13.40", "14.20"];
        $this->akhir = ["07.40", "08.20", "09.00", "09.40", "10.40", "11.20", "12.00", "13.40", "14.20", "15.00"];

        $test = Kelas::where('id', $kelas_id);
        if ($test->exists()) {
            # code...
        } else {
            abort('404');
        }
    }

    public function hapus($id)
    {
        $user = Jadwal::find($id);
        $this->showConfirmation($user->id);
    }

    public function hancur($id)
    {
        $user = Jadwal::find($id);
        $user->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Jadwal berhasil dihapus",
        ]);
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Jadwal ?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $jadwal = Jadwal::where('kelas_id', $this->kelas_id)->get();
        return view('livewire.jadwal.lihat', compact(['jadwal']))->extends('layouts.admin', ['title' => 'Lihat Jadwal'])->section('content');
    }
}
