<?php

namespace App\Http\Livewire\Jadwal;

use App\Models\Guru;
use App\Models\Jadwal;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Pengajar;
use Carbon\Carbon;
use Livewire\Component;

class Tambah extends Component
{
    public $hari;
    public $mapel;
    public $guru;
    public $jam1;
    public $jam2;
    public $awal;
    public $akhir;
    public $shin;
    public $ryujin;
    public $a = 0;
    public $b = 0;
    public $mpl;
    public $kelas_id;
    protected $listeners = ['berhasil'];

    public function mount($kelas_id)
    {
        $this->awal = ["07.00", "07.40", "08.20", "09.00", "10.00", "10.40", "11.20", "13.00", "13.40", "14.20"];
        $this->akhir = ["07.40", "08.20", "09.00", "09.40", "10.40", "11.20", "12.00", "13.40", "14.20", "15.00"];
        $this->shin = count($this->awal);
        $this->ryujin = count($this->akhir);

        $test = Kelas::where('id', $kelas_id);
        if ($test->exists()) {
            if ($kelas_id[0] == '7') {
                $k = "VII";
            } else if ($kelas_id[0] == '8') {
                $k = "VIII";
            } else if ($kelas_id[0] == '9') {
                $k = "IX";
            }
            $this->mpl = Mapel::where('nama', 'like', '%' . $k  . '%')->get();
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'hari' => 'required|regex:/^([^0-9]*)$/',
            'mapel' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
        ]);
    }

    public function updatedMapel()
    {
        $test = Pengajar::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel);
        if ($test->exists()) {
            $this->guru = $test->first()->guru->nama;
        } else {
            $this->guru = null;
        }
    }

    public function updatedJam1()
    {
        if ($this->jam1 > 9) {
            $this->jam1 = 0;
        } else {
            $this->b = $this->jam1 + 1;
        }
    }

    public function updatedJam2()
    {
        if ($this->jam2 > 9) {
            $this->jam2 = 0;
        } else {
            $this->shin = $this->jam2;
        }
    }

    public function tambah()
    {
        $test = Pengajar::where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel);
        if ($test->exists()) {
            $this->guru = $test->first()->guru_id;
        } else {
            $this->guru = null;
        }

        $this->validate([
            'hari' => 'required|regex:/^([^0-9]*)$/',
            'mapel' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
        ]);

        $x = Jadwal::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "JW" . sprintf("%04s", $y);

        Jadwal::create([
            'id' => $z,
            'hari' => $this->hari,
            'kelas_id' => $this->kelas_id,
            'mapel_id' => $this->mapel,
            'guru_id' => $this->guru,
            'jam' => $this->jam1 . '-' . $this->jam2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Jadwal berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/jadwal/lihat/$this->kelas_id");
    }

    public function render()
    {
        return view('livewire.jadwal.tambah')->extends('layouts.admin', ['title' => 'Tambah Jadwal'])->section('content');
    }
}
