<?php

namespace App\Http\Livewire\Tenggat;

use App\Models\Kelas;
use App\Models\Pengajar;
use App\Models\Tenggat;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $kls = '';
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function mount()
    {
        $this->kls = Pengajar::where('guru_id', auth()->user()->guru->id)->first()->kelas_id;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($soal_id, $kelas_id)
    {
        $this->showConfirmation($soal_id, $kelas_id);
    }

    public function hancur($soal_id, $kelas_id)
    {
        Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Tugas berhasil dihapus",
        ]);
    }

    public function showConfirmation($soal_id, $kelas_id)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Tugas?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params1'      => $soal_id, // optional, send params to success confirmation
            'params2'      => $kelas_id, // optional, send params to success confirmation
            'params3'      => null, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->get();
        $tenggat = Tenggat::selectRaw('tenggat.*')->join('kelas', 'kelas.id', '=', 'tenggat.kelas_id')->join('soal', 'soal.id', '=', 'tenggat.soal_id')->where('tenggat.kelas_id',  $this->kls)->where('soal.guru_id', auth()->user()->guru->id)->where(function ($q) {
            $q->orWhere('kelas.nama', 'like', '%' . $this->search . '%');
            $q->orWhere('soal.nama', 'like', '%' . $this->search . '%');
        })->paginate($this->perPage);
        return view('livewire.tenggat.data', compact(['tenggat', 'kelas']))->extends('layouts.guru', ['title' => 'Data Tugas'])->section('content');
    }
}
