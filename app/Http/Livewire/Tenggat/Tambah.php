<?php

namespace App\Http\Livewire\Tenggat;

use App\Models\DetailKelas;
use App\Models\Hasil;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Pengajar;
use App\Models\Siswa;
use App\Models\Soal;
use App\Models\Tenggat;
use Carbon\Carbon;
use Livewire\Component;

class Tambah extends Component
{
    public $s;
    public $k;
    public $tenggat;
    public $tgl;
    public $anu;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d') . 'T' . Carbon::yesterday()->format('H:i:s');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            's' => 'required',
            'k' => 'required',
            'tenggat' => 'required|after:yesterday',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            's' => 'required',
            'k' => 'required',
            'tenggat' => 'required|after:yesterday',
        ]);

        if (Tenggat::where('soal_id', $this->s)->where('kelas_id', $this->k)->first()) {
            $this->showAlert();
        } else {
            Tenggat::create([
                'soal_id' => $this->s,
                'kelas_id' => $this->k,
                'tenggat' => $this->tenggat,
                'bagikan' => 'Tidak',
            ]);

            $sol = Soal::find($this->s);
            if ($sol->jenis == 'Perbaikan') {
                $kelas_id = $this->k;
                $siswa = DetailKelas::where('kelas_id', $kelas_id)->get();
                $kl = Kelas::find($kelas_id);
                $mapel = Mapel::get();
                foreach ($siswa as $key => $value) {

                    if ($kl->nama[0] == '7') {
                        $kls = 'VII - ' . $kl->nama[1];
                    } else   if ($kl->nama[0] == '8') {
                        $kls = 'VIII - ' . $kl->nama[1];
                    } else   if ($kl->nama[0] == '9') {
                        $kls = 'IX - ' . $kl->nama[1];
                    }

                    foreach ($mapel as $key2 => $value2) {
                        $whatIWant = substr($value2->nama, strpos($value2->nama, "-") + 1);
                        $jisun = explode("-", $value2->nama);
                        if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                            $map[$key][$key2]['siswa'] = $value->siswa_id;
                            $map[$key][$key2]['id'] = $value2->id;
                            $map[$key][$key2]['nama'] = $jisun[0];
                            $map[$key][$key2]['tugas'] = 0;
                            $map[$key][$key2]['uts'] = 0;
                            $map[$key][$key2]['uas'] = 0;
                            $map[$key][$key2]['total'] = 0;
                        } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                            $map[$key][$key2]['siswa'] = $value->siswa_id;
                            $map[$key][$key2]['id'] = $value2->id;
                            $map[$key][$key2]['nama'] = $jisun[0];
                            $map[$key][$key2]['tugas'] = 0;
                            $map[$key][$key2]['uts'] = 0;
                            $map[$key][$key2]['uas'] = 0;
                            $map[$key][$key2]['total'] = 0;
                        } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                            $map[$key][$key2]['siswa'] = $value->siswa_id;
                            $map[$key][$key2]['id'] = $value2->id;
                            $map[$key][$key2]['nama'] = $jisun[0];
                            $map[$key][$key2]['tugas'] = 0;
                            $map[$key][$key2]['uts'] = 0;
                            $map[$key][$key2]['uas'] = 0;
                            $map[$key][$key2]['total'] = 0;
                        }
                    }

                    $tugas = 0;
                    $uts = 0;
                    $uas = 0;
                    $n = 0;
                    foreach ($map as $key2 => $value2) {
                        $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $map[$key][$key2]['id'])->where('siswa_id', $value->siswa_id)->where('semester', $sol->semester)->get();
                        foreach ($nilai as $key3 => $value3) {
                            if ($value3->jenis == 'Tugas Rumah' || $value3->jenis == 'Kuis' || $value3->jenis == 'Tugas') {
                                $n++;
                                $tugas += $value3->nilai;
                            }
                            if ($value3->jenis == 'UTS') {
                                $uts += $value3->nilai;
                            }
                            if ($value3->jenis == 'UAS') {
                                $uas += $value3->nilai;
                            }
                        }
                        if ($tugas == 0 || $n == 0) {
                            $map[$key][$key2]['tugas'] = 0;
                            $map[$key][$key2]['total'] = ((2 * 0) + $uts + $uas) / 4;
                        } else {
                            $tugas = $tugas / $n;
                            $map[$key][$key2]['tugas'] = $tugas;
                            $map[$key][$key2]['uts'] = $uts;
                            $map[$key][$key2]['uas'] = $uas;
                            $map[$key][$key2]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
                        }

                        $tugas = 0;
                        $uts = 0;
                        $uas = 0;
                        $n = 0;
                    }
                }

                foreach ($map as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                        if (($value2['total'] < 70) && (Soal::find($this->s)->mapel_id == $value2['id'])) {
                            $fix[$key] = $value2['siswa'];
                        }
                    }
                }

                foreach ($fix as $key => $value) {
                    Hasil::create([
                        'siswa_id' => $value,
                        'soal_id' => $this->s,
                        'kelas_id' => $this->k,
                        'pilihan_ganda' => 0,
                        'uraian' => 0,
                        'total' => 0
                    ]);
                }
            } else {
                $sis = DetailKelas::where('kelas_id', $this->k)->get();
                foreach ($sis as $key => $value) {
                    Hasil::create([
                        'siswa_id' => $value->siswa->id,
                        'soal_id' => $this->s,
                        'kelas_id' => $this->k,
                        'pilihan_ganda' => 0,
                        'uraian' => 0,
                        'total' => 0
                    ]);
                }
            }


            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Tugas berhasil ditambahkan",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Tugas sudah pernah diberikan",
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/tugas/data');
    }

    public function render()
    {
        $soal = Soal::where('guru_id', auth()->user()->guru->id)->get();
        if ($this->s == null) {
            $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->get();
        } else {
            $s = Soal::find($this->s);
            $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', 'like', '%' . $s->mapel_id  . '%')->get();
        }
        return view('livewire.tenggat.tambah', compact(['soal', 'kelas']))->extends('layouts.guru', ['title' => 'Tambah Tugas'])->section('content');
    }
}
