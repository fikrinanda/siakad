<?php

namespace App\Http\Livewire\Tenggat;

use App\Models\Pengajar;
use App\Models\Soal;
use App\Models\Tenggat;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $s;
    public $k;
    public $tenggat;
    public $tgl;
    public $soal_id;
    public $kelas_id;
    protected $listeners = ['berhasil'];

    public function mount($soal_id, $kelas_id)
    {
        if (Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first()->soal->guru->id == auth()->user()->guru->id) {
            $this->tgl = Carbon::yesterday()->format('Y-m-d') . 'T' . Carbon::yesterday()->format('H:i:s');
            $z = Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first();
            $this->s = $z->soal->jenis . ' - ' . $z->soal->mapel->nama . ' - ' . $z->soal->nama;
            $this->k = $z->kelas->nama . ' - ' . $z->kelas->tahun;
            $t = Carbon::parse(Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first()->tenggat);
            $this->tenggat = $t->format('Y-m-d') . 'T' . $t->format('H:i:s');
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            's' => 'required',
            'k' => 'required',
            'tenggat' => 'required|after:yesterday',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            's' => 'required',
            'k' => 'required',
            'tenggat' => 'required|after:yesterday',
        ]);

        Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->update([
            'tenggat' => $this->tenggat
        ]);
        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Tugas berhasil diubah",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Tugas sudah pernah diberikan",
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/tugas/data');
    }

    public function render()
    {
        return view('livewire.tenggat.ubah')->extends('layouts.guru', ['title' => 'Ubah Tugas'])->section('content');
    }
}
