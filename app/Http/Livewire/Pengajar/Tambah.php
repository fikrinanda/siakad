<?php

namespace App\Http\Livewire\Pengajar;

use App\Models\Guru;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Pengajar;
use Livewire\Component;

class Tambah extends Component
{
    public $kelas;
    public $guru;
    public $mapel;
    public $kelas2;
    public $guru2;
    public $mapel2;
    protected $listeners = ['berhasil', 'sip'];

    public function mount()
    {
        $this->kelas2 = Kelas::get();
        $this->mapel2 = Mapel::get();
        $this->guru2 = Guru::get();
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'kelas' => 'required',
            'mapel' => 'required',
            'guru' => 'required',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'kelas' => 'required',
            'mapel' => 'required',
            'guru' => 'required',
        ]);

        $test = Pengajar::where('kelas_id', $this->kelas)->where('mapel_id', $this->mapel)->where('guru_id', $this->guru)->exists();
        $test2 = Pengajar::where('kelas_id', $this->kelas)->where('mapel_id', $this->mapel)->exists();

        if ($test || $test2) {
            $this->showModal2("Data pengajar sudah ada");
        } else {
            Pengajar::create([
                'kelas_id' => $this->kelas,
                'mapel_id' => $this->mapel,
                'guru_id' => $this->guru,
            ]);
            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pengajar berhasil ditambahkan",
        ]);
    }

    public function showModal2($text)
    {
        $this->emit('swal:modal2', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => $text,
        ]);
    }

    public function sip()
    {
    }

    public function berhasil()
    {
        return redirect()->to('/pengajar/data');
    }

    public function render()
    {
        return view('livewire.pengajar.tambah')->extends('layouts.admin', ['title' => 'Tambah Pengajar'])->section('content');
    }
}
