<?php

namespace App\Http\Livewire\Pengajar;

use App\Models\Pengajar;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];


    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($kelas_id, $mapel_id, $guru_id)
    {
        $this->showConfirmation($kelas_id, $mapel_id, $guru_id);
    }

    public function hancur($kelas_id, $mapel_id, $guru_id)
    {
        Pengajar::where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('guru_id', $guru_id)->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pengajar berhasil dihapus",
        ]);
    }

    public function showConfirmation($kelas_id, $mapel_id, $guru_id)
    {
        $this->emit("swal:confirm2", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Pengajar?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params1'      => $kelas_id, // optional, send params to success confirmation
            'params2'      => $mapel_id, // optional, send params to success confirmation
            'params3'      => $guru_id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            $pengajar = Pengajar::selectRaw('pengajar.*')->join('kelas', 'kelas.id', '=', 'pengajar.kelas_id')->join('mapel', 'mapel.id', '=', 'pengajar.mapel_id')->join('guru', 'guru.id', '=', 'pengajar.guru_id')->where(function ($q) {
                $q->where('kelas.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('mapel.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('guru.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('kelas.tahun', 'like', '%' . $this->search . '%');
            })->paginate($this->perPage);
            return view('livewire.pengajar.data', compact(['pengajar']))->extends('layouts.admin', ['title' => 'Data Pengajar'])->section('content');
        } else if (auth()->user()->level == 'Guru') {
            $pengajar = Pengajar::selectRaw('pengajar.*')->join('kelas', 'kelas.id', '=', 'pengajar.kelas_id')->join('mapel', 'mapel.id', '=', 'pengajar.mapel_id')->join('guru', 'guru.id', '=', 'pengajar.guru_id')->where('pengajar.guru_id', auth()->user()->guru->id)->where(function ($q) {
                $q->where('kelas.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('mapel.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('guru.nama', 'like', '%' . $this->search . '%');
                $q->orWhere('kelas.tahun', 'like', '%' . $this->search . '%');
            })->paginate($this->perPage);
            return view('livewire.pengajar.data', compact(['pengajar']))->extends('layouts.guru', ['title' => 'Data Pengajar'])->section('content');
        }
    }
}
