<?php

namespace App\Http\Livewire\Pengajar;

use App\Models\Guru;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Pengajar;
use Livewire\Component;

class Ubah extends Component
{
    public $kls;
    public $mp;
    public $gr;
    public $k;
    public $m;
    public $g;
    protected $listeners = ['berhasil'];

    public function mount($kelas_id, $mapel_id, $guru_id)
    {
        $pengajar = Pengajar::where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('guru_id', $guru_id)->first();

        if ($pengajar) {
            $this->k = $pengajar->kelas_id;
            $this->m = $pengajar->mapel_id;
            $this->g = $pengajar->guru_id;
            $this->kls = $pengajar->kelas_id;
            $this->mp = $pengajar->mapel_id;
            $this->gr = $pengajar->guru_id;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'kls' => 'required',
            'mp' => 'required',
            'gr' => 'required',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'kls' => 'required',
            'mp' => 'required',
            'gr' => 'required',
        ]);

        Pengajar::where('kelas_id', $this->k)->where('mapel_id', $this->m)->where('guru_id', $this->g)->update([
            'kelas_id' => $this->kls,
            'mapel_id' => $this->mp,
            'guru_id' => $this->gr,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Pengajar berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/pengajar/data');
    }

    public function render()
    {
        $kelas = Kelas::get();
        $mapel = Mapel::get();
        $guru = Guru::get();
        return view('livewire.pengajar.ubah', compact(['kelas', 'mapel', 'guru']))->extends('layouts.admin', ['title' => 'Ubah Pengajar'])->section('content');
    }
}
