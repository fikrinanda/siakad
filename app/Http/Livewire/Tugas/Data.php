<?php

namespace App\Http\Livewire\Tugas;

use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Tenggat;
use Carbon\Carbon;
use Livewire\Component;

class Data extends Component
{
    public $tgl;
    public $n;
    public $z;
    public $tahun;
    public $kls;
    public $iu;
    public $map;

    public function mount()
    {
        $this->tgl = Carbon::now()->toDateTimeString();
        $this->z = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->get();
        // $this->n = $z->kelas->nama . " - " . $z->kelas->tahun;

        $kelas_id = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->latest('updated_at')->first()->kelas_id;
        $kl = Kelas::find($kelas_id);
        $this->tahun = $kl->tahun;
        if ($kl->nama[0] == '7') {
            $this->kls = 'VII - ' . $kl->nama[1];
        } else if ($kl->nama[0] == '8') {
            $this->kls = 'VIII - ' . $kl->nama[1];
        } else if ($kl->nama[0] == '9') {
            $this->kls = 'IX - ' . $kl->nama[1];
        }

        $mapel = Mapel::get();
        foreach ($mapel as $key => $value) {
            $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
            if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                $this->map[$key]['id'] = $value->id;
                $this->map[$key]['nama'] = $value->nama;
            } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                $this->map[$key]['id'] = $value->id;
                $this->map[$key]['nama'] = $value->nama;
            } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                $this->map[$key]['id'] = $value->id;
                $this->map[$key]['nama'] = $value->nama;
            }
        }

        $bulan = Carbon::now()->format('m');

        if ($bulan <= 6) {
            $this->iu = '2';
        } else if ($bulan >= 7) {
            $this->iu = '1';
        }
        $tugas = 0;
        $uts = 0;
        $uas = 0;
        $n = 0;
        foreach ($this->map as $key => $value) {
            $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $value['id'])->where('siswa_id', auth()->user()->siswa->id)->where('semester', $this->iu)->get();
            foreach ($nilai as $key2 => $value2) {
                if ($value2->jenis == 'Tugas Rumah' || $value2->jenis == 'Kuis' || $value2->jenis == 'Tugas') {
                    $n++;
                    $tugas += $value2->nilai;
                }
                if ($value2->jenis == 'UTS') {
                    $uts += $value2->nilai;
                }
                if ($value2->jenis == 'UAS') {
                    $uas += $value2->nilai;
                }
            }
            if ($tugas == 0 || $n == 0) {
                $this->map[$key]['tugas'] = 0;
                $this->map[$key]['total'] = ((2 * 0) + $uts + $uas) / 4;
            } else {
                $tugas = $tugas / $n;
                $this->map[$key]['tugas'] = $tugas;
                $this->map[$key]['uts'] = $uts;
                $this->map[$key]['uas'] = $uas;
                $this->map[$key]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
            }

            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
        }

        // dd($this->map);
    }

    public function render()
    {
        $i = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->get();
        // $i = Siswa::find(auth()->user()->siswa->id)->kelas_id[0];
        $mapel = Mapel::get();
        foreach ($mapel as $key => $value) {
            foreach ($i as $key2 => $value2) {
                $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
                if (($value2->kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                    $data[] = [
                        'id' => $value->id,
                        'nama' => $value->nama,
                        'kelas' => $value2->kelas->nama . " - " . $value2->kelas->tahun,
                        'kelas_id' => $value2->kelas_id,
                    ];
                } else if (($value2->kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                    $data[] = [
                        'id' => $value->id,
                        'nama' => $value->nama,
                        'kelas' => $value2->kelas->nama . " - " . $value2->kelas->tahun,
                        'kelas_id' => $value2->kelas_id,
                    ];
                } else if (($value2->kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                    $data[] = [
                        'id' => $value->id,
                        'nama' => $value->nama,
                        'kelas' => $value2->kelas->nama . " - " . $value2->kelas->tahun,
                        'kelas_id' => $value2->kelas_id,
                    ];
                }
            }
        }
        $j = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->latest('updated_at')->first()->kelas_id;

        $tenggat = Tenggat::where('kelas_id', $j)->get();
        // dd($data);
        return view('livewire.tugas.data', compact(['data', 'tenggat']))->extends('layouts.siswa', ['title' => 'Data Tugas'])->section('content');
    }
}
