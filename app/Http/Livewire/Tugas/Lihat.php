<?php

namespace App\Http\Livewire\Tugas;

use App\Models\Hasil;
use App\Models\Jawaban;
use App\Models\Nilai;
use App\Models\Perbaikan;
use App\Models\Soal;
use App\Models\Tenggat;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $soal_id;
    public $kelas_id;
    public $search = '';
    public $perPage = 5;
    public $search2 = '';
    public $perPage2 = 5;
    public $once;
    public $oncom;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal', 'yakin3' => 'hancur3', 'batal3', 'yakin4' => 'hancur4', 'batal4'];

    public function mount($soal_id, $kelas_id)
    {
        $this->once = Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first()->bagikan;
        $z = Soal::find($soal_id);
        $this->oncom = $z->jenis . ' - ' . $z->nama;
    }

    public function markonah()
    {
        $this->once = Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->first()->bagikan;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function nilai()
    {
        $this->showConfirmation(null);
    }

    public function bagikan()
    {
        $this->showConfirmation3(null);
    }

    public function batalkan()
    {
        $this->showConfirmation4(null);
    }

    public function hancur3()
    {
        Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->update([
            'bagikan' => 'Ya'
        ]);

        $this->markonah();
        $this->showModal2();
    }

    public function hancur4()
    {
        Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->update([
            'bagikan' => 'Tidak'
        ]);

        $this->markonah();
        $this->showModal3();
    }

    public function hancur()
    {
        $a = Soal::find($this->soal_id);

        $test = Nilai::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->exists();
        if ($test) {
            if ($a->jenis == 'Perbaikan') {
                $z = Hasil::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->get();
                foreach ($z as $key => $value) {
                    Perbaikan::where('siswa_id', $value->siswa_id)->where('kelas_id', $this->kelas_id)->where('soal_id', $this->soal_id)->update([
                        'siswa_id' => $value->siswa_id,
                        'kelas_id' => $this->kelas_id,
                        'mapel_id' => $a->mapel_id,
                        'semester' => $a->semester,
                        'nilai' => $value->total,
                    ]);
                }
                $this->showModal();
            } else {
                $z = Hasil::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->get();
                foreach ($z as $key => $value) {
                    Nilai::where('siswa_id', $value->siswa_id)->where('kelas_id', $this->kelas_id)->where('soal_id', $this->soal_id)->update([
                        'siswa_id' => $value->siswa_id,
                        'kelas_id' => $this->kelas_id,
                        'mapel_id' => $a->mapel_id,
                        'semester' => $a->semester,
                        'jenis' => $a->jenis,
                        'nilai' => $value->total,
                    ]);
                }
                $this->showModal();
            }
        } else {
            if ($a->jenis == 'Perbaikan') {
                $z = Hasil::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->get();
                foreach ($z as $key => $value) {
                    Perbaikan::create([
                        'siswa_id' => $value->siswa_id,
                        'kelas_id' => $this->kelas_id,
                        'mapel_id' => $a->mapel_id,
                        'soal_id' => $this->soal_id,
                        'semester' => $a->semester,
                        'nilai' => $value->total,
                    ]);
                }
                $this->showModal();
            } else {
                $z = Hasil::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->get();
                foreach ($z as $key => $value) {
                    Nilai::create([
                        'siswa_id' => $value->siswa_id,
                        'kelas_id' => $this->kelas_id,
                        'mapel_id' => $a->mapel_id,
                        'soal_id' => $this->soal_id,
                        'semester' => $a->semester,
                        'jenis' => $a->jenis,
                        'nilai' => $value->total,
                    ]);
                }
                $this->showModal();
            }
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function batal3()
    {
        // dd('batal');
    }

    public function batal4()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil memasukkan nilai",
        ]);
    }

    public function showModal2()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil membagikan hasil",
        ]);
    }

    public function showModal3()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil membatalkan bagikan hasil",
        ]);
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin memasukkan nilai?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation3($id)
    {
        $this->emit("swal:confirm3", [
            'icon'        => 'warning',
            'title'       => "Yakin bagikan hasil?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function showConfirmation4($id)
    {
        $this->emit("swal:confirm4", [
            'icon'        => 'warning',
            'title'       => "Yakin membatalkan bagikan hasil?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $sis = Jawaban::selectRaw('jawaban_soal.*')->join('siswa', 'siswa.id', '=', 'jawaban_soal.siswa_id')->where('nama', 'like', '%' . $this->search . '%')->where('soal_id', $this->soal_id)->where('jawaban_soal.kelas_id', $this->kelas_id)->groupBy('siswa_id', 'soal_id', 'kelas_id', 'created_at', 'updated_at')->paginate($this->perPage);
        $hasil = Hasil::selectRaw('hasil_soal.*')->join('siswa', 'siswa.id', '=', 'hasil_soal.siswa_id')->where('nama', 'like', '%' . $this->search2 . '%')->where('soal_id', $this->soal_id)->where('hasil_soal.kelas_id', $this->kelas_id)->paginate($this->perPage2);
        return view('livewire.tugas.lihat', compact(['sis', 'hasil']))->extends('layouts.guru', ['title' => 'Lihat Tugas'])->section('content');
    }
}
