<?php

namespace App\Http\Livewire\Tugas;

use App\Models\Detail;
use App\Models\Hasil;
use App\Models\Jawaban;
use App\Models\Siswa;
use App\Models\Soal;
use Livewire\Component;

class Koreksi extends Component
{
    public $soal;
    public $jawaban;
    public $mak = [];
    public $n;
    public $h = 0;
    public $t = 0;
    public $pilgan = 0;
    public $uraian = 0;
    public $jumlah_pilgan = 0;
    public $jumlah_uraian = 0;
    public $bobot_pilgan = 0;
    public $bobot_uraian = 0;
    public $total_pilgan = 0;
    public $total_uraian = 0;
    public $total;
    public $soal_id;
    public $kelas_id;
    public $siswa_id;
    public $nama;
    protected $listeners = ['berhasil'];

    public function mount($soal_id, $kelas_id, $siswa_id)
    {
        if (Jawaban::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', $siswa_id)->exists()) {
            $this->nama = Siswa::find($siswa_id)->nama;
            $this->soal = Detail::where('soal_id', $soal_id)->get();
            $this->jawaban = Jawaban::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', $siswa_id)->get();
            if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count() > 0) {
                $this->n =  100 /  Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count();
            }
            $z = Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count();
            for ($i = 0; $i < $z; $i++) {
                $this->mak[] = [$this->n];
                // $this->h += $this->h;
            }

            foreach ($this->soal as $key => $value) {
                $jawaban = Jawaban::where('hash', $value->hash)->where('siswa_id', $siswa_id)->first();
                if ($jawaban) {
                    if ($jawaban->jawaban == $value->jawaban) {
                        $this->t++;
                    }
                }
            }

            if (Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
                $this->pilgan = $this->t;
                $this->jumlah_pilgan = Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->count();
                $this->bobot_pilgan = Soal::find($soal_id)->bobot_pilgan;
                $this->total_pilgan = ($this->pilgan / $this->jumlah_pilgan) * $this->bobot_pilgan;
            }

            if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists()) {
                $suzy = 0;
                foreach ($this->mak as $key => $value) {
                    $suzy += $value[0];
                }
                $this->uraian = $suzy;
                $this->bobot_uraian = Soal::find($soal_id)->bobot_uraian;
                $this->total_uraian = ($this->uraian / 100) * $this->bobot_uraian;
            }

            if ((Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) && (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists())) {
                $this->total = $this->total_pilgan + $this->total_uraian;
            } else if (Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
                $this->total = $this->total_pilgan;
            } else if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists()) {
                $this->total = $this->total_uraian;
            }
        } else {
            abort('404');
        }
    }

    public function updatedMak()
    {
        if (Detail::where('soal_id', $this->soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
            $this->pilgan = $this->t;
            $this->jumlah_pilgan = Detail::where('soal_id', $this->soal_id)->where('jawaban', '!=', 'Uraian')->count();
            $this->bobot_pilgan = Soal::find($this->soal_id)->bobot_pilgan;
            $this->total_pilgan = ($this->pilgan / $this->jumlah_pilgan) * $this->bobot_pilgan;
        }

        if (Detail::where('soal_id', $this->soal_id)->where('jawaban', 'Uraian')->exists()) {
            $suzy = 0;
            foreach ($this->mak as $key => $value) {
                $suzy += $value[0];
            }
            $this->uraian = $suzy;
            $this->bobot_uraian = Soal::find($this->soal_id)->bobot_uraian;
            $this->total_uraian = ($this->uraian / 100) * $this->bobot_uraian;
        }

        if ((Detail::where('soal_id', $this->soal_id)->where('jawaban', '!=', 'Uraian')->exists()) && (Detail::where('soal_id', $this->soal_id)->where('jawaban', 'Uraian')->exists())) {
            $this->total = $this->total_pilgan + $this->total_uraian;
        } else if (Detail::where('soal_id', $this->soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
            $this->total = $this->total_pilgan;
        } else if (Detail::where('soal_id', $this->soal_id)->where('jawaban', 'Uraian')->exists()) {
            $this->total = $this->total_uraian;
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'mak.*.0' => "required|numeric|min:0|max:$this->n"
        ]);
    }

    public function koreksi()
    {
        $this->validate([
            'mak.*.0' => "required|numeric|min:0|max:$this->n"
        ]);

        Hasil::where('siswa_id', $this->siswa_id)->where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->update([
            'pilihan_ganda' => $this->total_pilgan,
            'uraian' => $this->total_uraian,
            'total' => $this->total,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Siswa $this->nama mendapat nilai $this->total",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/tugas/lihat/$this->soal_id/$this->kelas_id");
    }

    public function render()
    {
        return view('livewire.tugas.koreksi')->extends('layouts.guru', ['title' => 'Koreksi Tugas'])->section('content');
    }
}
