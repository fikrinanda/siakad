<?php

namespace App\Http\Livewire\Tugas;

use App\Models\Detail;
use App\Models\DetailKelas;
use App\Models\Hasil;
use App\Models\Jawaban;
use App\Models\Materi;
use App\Models\Soal;
use App\Models\Tenggat;
use Carbon\Carbon;
use Livewire\Component;

class Mulai extends Component
{
    public $soal_id;
    public $kelas_id;
    public $jawaban = [];
    public $soal;
    public $materi;
    public $pil = [];
    public $hash = [];
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'sip'];

    public function mount($soal_id, $kelas_id)
    {
        $i = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->first()->kelas_id;

        if ($te = Tenggat::where('soal_id', $soal_id)->where('kelas_id', $i)->first()) {
            if (($te->tenggat > Carbon::now()->toDateTimeString()) && (!Jawaban::where('siswa_id', auth()->user()->siswa->id)->where('soal_id', $soal_id)->exists())) {
                $this->materi = Materi::where('soal_id', $soal_id)->get();
                $this->soal = Detail::where('soal_id', $this->soal_id)->get()->shuffle();
                foreach ($this->soal as $key => $value) {
                    $this->jawaban[] = [];
                    $this->hash[] = $value->hash;
                    $this->pil[] = collect([
                        [
                            0 => $value->pilihan_a,
                            1 => $value->pilihan_a_gambar,
                            2 => 'a'
                        ],
                        [
                            0 => $value->pilihan_b,
                            1 => $value->pilihan_b_gambar,
                            2 => 'b'
                        ],
                        [
                            0 => $value->pilihan_c,
                            1 => $value->pilihan_c_gambar,
                            2 => 'c'
                        ],
                        [
                            0 => $value->pilihan_d,
                            1 => $value->pilihan_d_gambar,
                            2 => 'd'
                        ],
                        [
                            0 => $value->pilihan_e,
                            1 => $value->pilihan_e_gambar,
                            2 => 'e'
                        ]
                    ])->shuffle()->all();
                }
                // dd($this->pil[0][0][1]);
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function jawab()
    {
        // dd($this->jawaban);
        $this->validate([
            'jawaban.*' => 'required',
        ]);

        $this->showConfirmation(null);
    }

    public function hancur()
    {
        $i = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $this->kelas_id)->first()->kelas_id;
        if ($te = Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $i)->first()) {
            if (($te->tenggat > Carbon::now()->toDateTimeString()) && (!Jawaban::where('siswa_id', auth()->user()->siswa->id)->where('soal_id', $this->soal_id)->exists())) {
                $b = 0;
                foreach ($this->soal as $key => $value) {
                    if ($value->jawaban == $this->jawaban[$key]) {
                        $b++;
                    }
                }

                foreach ($this->soal as $key => $value) {
                    Jawaban::create([
                        'siswa_id' => auth()->user()->siswa->id,
                        'soal_id' => $this->soal_id,
                        'kelas_id' => $this->kelas_id,
                        'hash' => $this->hash[$key],
                        'jawaban' => $this->jawaban[$key],
                    ]);
                }

                $this->showModal();
            } else {
                $this->showModal2();
            }
        } else {
            $this->showModal2();
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Berhasil menyerahkan tugas",
        ]);
    }

    public function showModal2()
    {
        $this->emit('swal:modal2', [
            'icon'  => 'error',
            'title' => 'Gagal!!!',
            'text'  => "Batas waktu sudah habis",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/tugas');
    }

    public function sip()
    {
        return redirect()->to('/tugas');
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menyerahkan tugas?",
            'text'        => "",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $sl = Soal::find($this->soal_id);
        $teng = Tenggat::where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->first();
        return view('livewire.tugas.mulai', compact(['sl', 'teng']))->extends('layouts.siswa', ['title' => 'Tugas'])->section('content');
    }
}
