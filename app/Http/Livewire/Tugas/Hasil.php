<?php

namespace App\Http\Livewire\Tugas;

use App\Models\Detail;
use App\Models\Hasil as ModelsHasil;
use App\Models\Jawaban;
use App\Models\Siswa;
use App\Models\Soal;
use App\Models\Tenggat;
use Livewire\Component;

class Hasil extends Component
{
    public $soal;
    public $jawaban;
    public $mak = [];
    public $n;
    public $h = 0;
    public $t = 0;
    public $pilgan = 0;
    public $uraian = 0;
    public $jumlah_pilgan = 0;
    public $jumlah_uraian = 0;
    public $bobot_pilgan = 0;
    public $bobot_uraian = 0;
    public $total_pilgan = 0;
    public $total_uraian = 0;
    public $total = 0;
    public $soal_id;
    public $kelas_id;
    public $nama;
    public $p = 0;
    public $g = 0;

    public function mount($soal_id, $kelas_id)
    {
        if (Jawaban::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->exists() && Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first()->bagikan == 'Ya') {
            $this->p = ModelsHasil::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->first()->pilihan_ganda;
            $this->g = ModelsHasil::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->first()->uraian;

            $this->nama = Siswa::find(auth()->user()->siswa->id)->nama;
            $this->soal = Detail::where('soal_id', $soal_id)->get();
            $this->jawaban = Jawaban::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->get();
            if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count() > 0) {
                $this->n =  100 /  Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count();
            }
            $z = Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count();
            for ($i = 0; $i < $z; $i++) {
                $this->mak[] = [$this->n];
                // $this->h += $this->h;
            }

            foreach ($this->soal as $key => $value) {
                $jawaban = Jawaban::where('hash', $value->hash)->where('siswa_id', auth()->user()->siswa->id)->first();
                if ($jawaban) {
                    if ($jawaban->jawaban == $value->jawaban) {
                        $this->t++;
                    }
                }
            }

            if (Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
                $this->pilgan = $this->t;
                $this->jumlah_pilgan = Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->count();
                $this->bobot_pilgan = Soal::find($soal_id)->bobot_pilgan;
                $this->total_pilgan = ($this->pilgan / $this->jumlah_pilgan) * $this->bobot_pilgan;
            }

            if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists()) {
                $suzy = 0;
                foreach ($this->mak as $key => $value) {
                    $suzy += $value[0];
                }
                $this->uraian = $suzy;
                $this->bobot_uraian = Soal::find($soal_id)->bobot_uraian;
                $this->total_uraian = ($this->uraian / 100) * $this->bobot_uraian;
            }

            if ((Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) && (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists())) {
                $this->total = $this->total_pilgan + $this->total_uraian;
            } else if (Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->exists()) {
                $this->total = $this->total_pilgan;
            } else if (Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->exists()) {
                $this->total = $this->total_uraian;
            }
        } else if (Tenggat::where('soal_id', $soal_id)->where('kelas_id', $kelas_id)->first()->bagikan == 'Ya') {
            $this->soal = Detail::where('soal_id', $soal_id)->get();
            $this->nama = Siswa::find(auth()->user()->siswa->id)->nama;
        } else {
            abort('404');
        }
    }


    public function render()
    {
        $hasil = ModelsHasil::where('siswa_id', auth()->user()->siswa->id)->where('soal_id', $this->soal_id)->where('kelas_id', $this->kelas_id)->first();
        return view('livewire.tugas.hasil', compact(['hasil']))->extends('layouts.siswa', ['title' => 'Hasil'])->section('content');
    }
}
