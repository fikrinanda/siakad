<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use Carbon\Carbon;
use Livewire\Component;

class Show extends Component
{
    public $kelas_id;
    public $tanggal;
    public $anu;
    public $tgl;
    public $ket = '';
    public $hmm;

    public function mount($kelas_id, $tanggal)
    {
        $this->anu = Carbon::parse($tanggal)->format('Y-m-d');
        $this->tgl = Carbon::parse($tanggal)->format('d M Y');
    }

    public function render()
    {
        $absen = Absensi::where('kelas_id', $this->kelas_id)->where('tanggal', $this->anu)->where('keterangan', 'like', '%' . $this->ket . '%')->where('keterangan', '!=', 'Bolos')->get();
        $bolos = Absensi::where('kelas_id', $this->kelas_id)->where('tanggal', $this->anu)->where('keterangan', 'Bolos')->get();
        return view('livewire.absensi.show', compact(['absen', 'bolos']))->extends('layouts.guru', ['title' => 'Show Absensi'])->section('content');
    }
}
