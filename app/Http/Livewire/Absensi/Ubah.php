<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use Carbon\Carbon;
use Livewire\Component;

class Ubah extends Component
{
    public $kelas_id;
    public $tanggal;
    public $siswa_id;
    public $sis;
    public $kel;
    public $tang;
    public $ket;
    public $tgl;
    public $surat;
    protected $listeners = ['berhasil'];

    public function mount($kelas_id, $tanggal, $siswa_id)
    {
        $this->tgl = Carbon::parse($tanggal)->format('Y-m-d');
        $test = Absensi::where('kelas_id', $kelas_id)->where('tanggal', $this->tgl)->where('siswa_id', $siswa_id)->exists();
        if ($test) {
            $lia = Absensi::where('kelas_id', $kelas_id)->where('tanggal', $this->tgl)->where('siswa_id', $siswa_id)->first();
            $this->sis = $lia->siswa->nama;
            $this->kel = $lia->kelas->nama;
            $this->tang = $lia->tanggal->format('d, M Y');
            $this->ket = $lia->keterangan;
            $this->surat = $lia->surat;
        } else {
            abort('404');
        }
    }

    public function ubah()
    {
        Absensi::where('kelas_id', $this->kelas_id)->where('tanggal', $this->tgl)->where('siswa_id', $this->siswa_id)->update([
            'keterangan' => $this->ket
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Absensi berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/absensi/lihat/$this->kelas_id/$this->tanggal");
    }

    public function render()
    {
        return view('livewire.absensi.ubah')->extends('layouts.guru', ['title' => 'Ubah Absensi'])->section('content');
    }
}
