<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah2 extends Component
{
    use WithFileUploads;

    public $ket;
    public $surat;
    public $surat2;
    public $kelas_id;
    public $tanggal;
    public $tgl;
    protected $listeners = ['berhasil'];

    public function mount($kelas_id, $tanggal)
    {
        $absensi = Absensi::where('kelas_id', $kelas_id)->where('tanggal', Carbon::parse($tanggal)->format('Y-m-d'));
        $t = Carbon::parse($tanggal)->format('d') + 3;
        $t2 = Carbon::now()->format('d');
        if ($absensi->exists() && ($t2 <= $t)) {
            $this->ket = $absensi->first()->keterangan;
            $this->surat = $absensi->first()->surat;
            $this->tgl = Carbon::parse($tanggal)->format('d M Y');
        } else {
            abort('404');
        }
    }

    public function ubah()
    {
        // if ($this->ket != 'Hadir') {
        //     $this->validate([
        //         'surat2' => 'required|image|max:5000',
        //     ]);
        // }

        if ($this->surat2) {
            Storage::disk('public')->delete($this->surat);
            $surat2 = $this->surat2->store('images/guru/foto', 'public');
        } else {
            if ($this->ket == 'Hadir') {
                Storage::disk('public')->delete($this->surat);
                $surat2 = null;
            } else {
                $surat2 = $this->surat ?? null;
            }
        }

        Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $this->kelas_id)->where('tanggal', Carbon::parse($this->tanggal)->format('Y-m-d'))->update([
            'keterangan' => $this->ket,
            'surat' => $surat2 ?? null
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Absensi $this->tgl berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/absensi/riwayat');
    }

    public function render()
    {
        return view('livewire.absensi.ubah2')->extends('layouts.siswa', ['title' => 'Ubah Absensi'])->section('content');
    }
}
