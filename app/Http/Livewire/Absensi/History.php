<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use Livewire\Component;
use Livewire\WithPagination;

class History extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $absensi = Absensi::where('siswa_id', auth()->user()->siswa->id)->paginate($this->perPage);
        return view('livewire.absensi.history', compact(['absensi']))->extends('layouts.siswa', ['title' => 'Riwayat Absensi'])->section('content');
    }
}
