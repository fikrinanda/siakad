<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use App\Models\DetailKelas;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class Absen extends Component
{
    use WithFileUploads;

    public $sis;
    public $ket = 'Hadir';
    public $surat;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->sis = auth()->user()->siswa->nama;
    }

    public function absen()
    {
        // if ($this->ket != 'Hadir') {
        //     $this->validate([
        //         'surat' => 'required|image|max:5000',
        //     ]);
        // }

        if ($this->surat != null) {
            $foto = $this->surat->store('images/siswa/surat', 'public') ?? null;
        } else {
            $foto = null;
        }

        Absensi::where('siswa_id', auth()->user()->siswa->id)->where('tanggal', Carbon::now()->format('Y-m-d'))->where('kelas_id', DetailKelas::where('siswa_id', auth()->user()->siswa->id)->latest('created_at')->first()->kelas_id)->update([
            'keterangan' => $this->ket,
            'surat' => $foto
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "",
        ]);
    }

    public function berhasil()
    {
        return true;
    }

    public function render()
    {
        return view('livewire.absensi.absen')->extends('layouts.siswa', ['title' => 'Absensi'])->section('content');
    }
}
