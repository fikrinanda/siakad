<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Absensi;
use Livewire\Component;
use Livewire\WithPagination;

class Detail extends Component
{
    use WithPagination;

    public $kelas_id;
    public $perPage = 5;
    public $tanggal = '';
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function mount($kelas_id)
    {
    }

    public function render()
    {
        $absen = Absensi::select('tanggal')->groupBy('tanggal')->where('kelas_id', $this->kelas_id)->where('tanggal', 'like', '%' . $this->tanggal . '%')->paginate($this->perPage);
        $absensi = Absensi::select('tanggal')->where('kelas_id', $this->kelas_id)->groupBy('tanggal')->get();
        return view('livewire.absensi.detail', compact(['absen', 'absensi']))->extends('layouts.guru', ['title' => 'Detail Absensi'])->section('content');
    }
}
