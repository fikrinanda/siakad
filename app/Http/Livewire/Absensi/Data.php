<?php

namespace App\Http\Livewire\Absensi;

use App\Models\Kelas;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $kelas = Kelas::where('guru_id', auth()->user()->guru->id)->paginate($this->perPage);
        return view('livewire.absensi.data', compact(['kelas']))->extends('layouts.guru', ['title' => 'Data Absensi'])->section('content');
    }
}
