<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logout extends Component
{
    public function mount()
    {
        auth()->logout();
        return redirect('/');
    }

    public function render()
    {
        if (auth()->user()->level == 'Guru') {
            return view('livewire.logout')->extends('layouts.guru')->section('content');
        } else if (auth()->user()->level == 'Siswa') {
            return view('livewire.logout')->extends('layouts.siswa')->section('content');
        }
    }
}
