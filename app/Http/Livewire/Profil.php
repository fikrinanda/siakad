<?php

namespace App\Http\Livewire;

use App\Models\DetailKelas;
use App\Models\Pengajar;
use App\Models\User;
use Livewire\Component;

class Profil extends Component
{
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $uname;
    public $password;
    public $kelas;

    public function mount()
    {
        if (auth()->user()->level == 'Guru') {
            $this->nama = auth()->user()->guru->nama;
            $this->jenis_kelamin = auth()->user()->guru->jenis_kelamin;
            $this->tanggal_lahir = auth()->user()->guru->tanggal_lahir;
            $this->alamat = auth()->user()->guru->alamat;
            $this->foto = auth()->user()->guru->foto;
            $this->uname = auth()->user()->username;
            $this->password = auth()->user()->password;
        } elseif (auth()->user()->level == "Siswa") {
            $this->nama = auth()->user()->siswa->nama;
            $this->jenis_kelamin = auth()->user()->siswa->jenis_kelamin;
            $this->tanggal_lahir = auth()->user()->siswa->tanggal_lahir;
            $this->alamat = auth()->user()->siswa->alamat;
            $this->kelas = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->get();
            $this->foto = auth()->user()->siswa->foto;
            $this->uname = auth()->user()->username;
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'Guru') {
            $mengajar = Pengajar::where('guru_id', auth()->user()->guru->id)->get();
            return view('livewire.profil', compact(['mengajar']))->extends('layouts.guru', ['title' => 'Profil'])->section('content');
        } else if (auth()->user()->level == 'Siswa') {
            return view('livewire.profil')->extends('layouts.siswa', ['title' => 'Profil'])->section('content');
        }
    }
}
