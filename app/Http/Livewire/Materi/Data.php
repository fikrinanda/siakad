<?php

namespace App\Http\Livewire\Materi;

use App\Models\Pengajar;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $kelas = Pengajar::join('mapel', 'mapel.id', '=', 'pengajar.mapel_id')->where('guru_id', auth()->user()->guru->id)->where('mapel.nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);
        return view('livewire.materi.data', compact(['kelas']))->extends('layouts.guru', ['title' => 'Data Materi'])->section('content');
    }
}
