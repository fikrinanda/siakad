<?php

namespace App\Http\Livewire\Materi;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Pengajar;
use App\Models\Soal;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $mpl;
    public $mapel_id;
    public $nama;
    public $soal_id;
    public $kelas_id;
    public $file;
    public $link;
    protected $listeners = ['berhasil'];

    public function mount($mapel_id, $kelas_id)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $mapel_id)->where('kelas_id', $kelas_id)->exists()) {
            $m = Mapel::find($mapel_id);
            $n = Kelas::find($kelas_id);
            $this->mpl = $m->nama . ' Kelas ' . $n->nama . ' ' . $n->tahun;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required',
            // 'file' => 'required'
        ]);
    }

    public function tambah()
    {
        // $extension = $this->file->getClientOriginalExtension();

        // dd($extension);
        $this->validate([
            'nama' => 'required',
            // 'file' => 'required'
        ]);

        $n = auth()->user()->guru->nama;
        if ($this->file != null) {
            $file = $this->file->store("materi/$this->mpl/$n", "public");
        } else {
            $file = null;
        }

        $x = Materi::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "MT" . sprintf("%04s", $y);

        if ($this->soal_id == "") {
            $this->soal_id = null;
        }

        Materi::create([
            'id' => $z,
            'guru_id' => auth()->user()->guru->id,
            'mapel_id' => $this->mapel_id,
            'kelas_id' => $this->kelas_id,
            'soal_id' => $this->soal_id ?? null,
            'nama' => $this->nama,
            'link' => $this->link,
            'file' => $file
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Materi berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/materi/lihat/$this->mapel_id/$this->kelas_id");
    }

    public function render()
    {
        $soal = Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->get();
        $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->get();
        return view('livewire.materi.tambah', compact(['soal', 'kelas']))->extends('layouts.guru', ['title' => 'Tambah Materi'])->section('content');
    }
}
