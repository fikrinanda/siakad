<?php

namespace App\Http\Livewire\Materi;

use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Pengajar;
use App\Models\Soal;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $mpl;
    public $materi_id;
    public $mapel_id;
    public $nama;
    public $soal_id;
    public $kelas_id;
    public $file;
    public $fi;
    public $link;
    protected $listeners = ['berhasil'];

    public function mount($materi_id)
    {
        $materi = Materi::find($materi_id);
        if ($materi) {
            $this->mpl = Mapel::find($materi->mapel_id)->nama;
            $this->mapel_id = $materi->mapel_id;
            $this->soal_id = $materi->soal_id;
            $this->kelas_id = $materi->kelas_id;
            $this->nama = $materi->nama;
            $this->fi = $materi->file;
            $this->link = $materi->link;
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'mapel_id' => 'required',
            'kelas_id' => 'required',
            'nama' => 'required',
            // 'file' => 'required'
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'mapel_id' => 'required',
            'kelas_id' => 'required',
            'nama' => 'required',
            // 'file' => 'required'
        ]);

        $n = auth()->user()->guru->nama;

        if ($this->file) {
            Storage::disk('public')->delete($this->fi);
            $file = $this->file->store("materi/$this->mpl/$n", "public");
        } else {
            $file = $this->fi ?? null;
        }


        Materi::where('id', $this->materi_id)->update([
            'guru_id' => auth()->user()->guru->id,
            'mapel_id' => $this->mapel_id,
            'kelas_id' => $this->kelas_id,
            'soal_id' => $this->soal_id ?? null,
            'nama' => $this->nama,
            'link' => $this->link,
            'file' => $file
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Materi berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/materi/lihat/$this->mapel_id");
    }

    public function render()
    {
        $soal = Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->get();
        $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->get();
        return view('livewire.materi.ubah', compact(['soal', 'kelas']))->extends('layouts.guru', ['title' => 'Ubah Materi'])->section('content');
    }
}
