<?php

namespace App\Http\Livewire\Materi;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Pengajar;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $i;
    public $kls;
    public $mpl;
    public $kelas_id;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function mount($mapel_id, $kelas_id)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $mapel_id)->where('kelas_id', $kelas_id)->exists()) {
            $this->i = $mapel_id;
            $m = Mapel::find($mapel_id);
            $n = Kelas::find($kelas_id);
            $this->mpl = $m->nama . ' Kelas ' . $n->nama . ' ' . $n->tahun;
        } else {
            abort('404');
        }
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $this->showConfirmation($id);
    }

    public function hancur($id)
    {
        $materi = Materi::find($id);
        Storage::disk('public')->delete($materi->file);
        $materi->delete();
        $this->showModal();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Materi berhasil dihapus",
        ]);
    }

    public function showConfirmation($id)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus materi?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $materi = Materi::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->i)->paginate($this->perPage);
        return view('livewire.materi.lihat', compact(['materi']))->extends('layouts.guru', ['title' => 'Lihat Materi'])->section('content');
    }
}
