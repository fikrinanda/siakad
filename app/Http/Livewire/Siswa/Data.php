<?php

namespace App\Http\Livewire\Siswa;

use App\Models\Kelas;
use App\Models\Pengajar;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $kls = '';
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function mount()
    {
        if (auth()->user()->level == 'Admin') {
            $this->kls = Kelas::first()->id;
        } else if (auth()->user()->level == 'Guru') {
            $this->kls = Pengajar::where('guru_id', auth()->user()->guru->id)->first()->kelas_id;
        }
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $user = User::find($id);
        $this->showConfirmation($user->id, $user->siswa->nama);
    }

    public function hancur($id)
    {
        $user = User::find($id);
        $nama = $user->siswa->nama;
        $siswa = Siswa::where('user_id', $id)->first();
        Storage::disk('public')->delete($siswa->foto);
        $user->delete();
        $this->showModal($nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Siswa $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Siswa $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {

        if (auth()->user()->level == 'Admin') {
            $kelas = Kelas::get();
            $siswa = Siswa::selectRaw('siswa.*')->join('users', 'users.id', '=', 'siswa.user_id')->join('detail_kelas', 'detail_kelas.siswa_id', '=', 'siswa.id')->where('detail_kelas.kelas_id',  $this->kls)->where(function ($q) {
                $q->where('nama', 'like', '%' . $this->search . '%');
                $q->orWhere('username', 'like', '%' . $this->search . '%');
            })->paginate($this->perPage);
            return view('livewire.siswa.data', compact(['siswa', 'kelas']))->extends('layouts.admin', ['title' => 'Data Siswa'])->section('content');
        } else if (auth()->user()->level == 'Guru') {
            $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->get();
            // dd($kelas);
            $siswa = Siswa::selectRaw('siswa.*')->join('users', 'users.id', '=', 'siswa.user_id')->join('detail_kelas', 'detail_kelas.siswa_id', '=', 'siswa.id')->where('detail_kelas.kelas_id',  $this->kls)->where(function ($p) {
                $saya = Pengajar::where('guru_id', auth()->user()->guru->id)->get();
                foreach ($saya as $key => $value) {
                    $p->orWhere('detail_kelas.kelas_id', $value->kelas_id);
                }
            })->where(function ($q) {
                $q->where('nama', 'like', '%' . $this->search . '%');
                $q->orWhere('username', 'like', '%' . $this->search . '%');
            })->paginate($this->perPage);
            return view('livewire.siswa.data', compact(['siswa', 'kelas']))->extends('layouts.guru', ['title' => 'Data Siswa'])->section('content');
        }
    }
}
