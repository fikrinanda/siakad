<?php

namespace App\Http\Livewire\Siswa;

use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Tambah extends Component
{
    use WithFileUploads;

    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $kelas;
    public $foto;
    public $nisn;
    public $tgl;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'kelas' => 'required',
            'nisn' => 'required|numeric|unique:users,username',
            'foto' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'kelas' => 'required',
            'nisn' => 'required|numeric|unique:users,username',
            'foto' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/siswa/foto', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->nisn,
            'password' => Hash::make($this->nisn),
            'level' => 'Siswa',
        ]);

        $user = User::where('username', $this->nisn)->first();

        $x2 = Siswa::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "SW" . sprintf("%04s", $y2);

        Siswa::create([
            'id' => $z2,
            'user_id' => $user->id,
            // 'kelas_id' => $this->kelas,
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'alamat' => $this->alamat,
            'foto' => $foto,
        ]);

        DetailKelas::create([
            'siswa_id' => $z2,
            'kelas_id' => $this->kelas,
            'hash' => Str::random(32),
            'keterangan' => 'Awal Masuk'
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Siswa $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/siswa/data');
    }

    public function render()
    {
        $kls = Kelas::get();
        return view('livewire.siswa.tambah', compact(['kls']))->extends('layouts.admin', ['title' => 'Tambah Siswa'])->section('content');
    }
}
