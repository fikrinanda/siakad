<?php

namespace App\Http\Livewire\Siswa;

use App\Models\DetailKelas;
use App\Models\Siswa;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $kelas;
    public $foto;
    public $uname;
    public $password;

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $siswa = Siswa::where('user_id', $user->id)->first();
            if ($siswa) {
                $this->nama = $siswa->nama;
                $this->jenis_kelamin = $siswa->jenis_kelamin;
                $this->tanggal_lahir = $siswa->tanggal_lahir;
                $this->alamat = $siswa->alamat;
                $this->kelas = DetailKelas::where('siswa_id', $siswa->id)->get();
                $this->foto = $siswa->foto;
                $this->uname = $user->username;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        if (auth()->user()->level == 'Admin') {
            return view('livewire.siswa.lihat')->extends('layouts.admin', ['title' => 'Lihat Siswa'])->section('content');
        } else if (auth()->user()->level == 'Guru')
            return view('livewire.siswa.lihat')->extends('layouts.guru', ['title' => 'Lihat Siswa'])->section('content');
    }
}
