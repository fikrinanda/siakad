<?php

namespace App\Http\Livewire\Siswa;

use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $kelas;
    public $foto;
    public $foto2;
    public $i;
    public $tgl;
    protected $listeners = ['berhasil'];

    protected $rules = [
        'kelas.*.kelas_id' => 'required',
    ];

    public function mount($username)
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
        $user = User::where('username', $username)->first();

        if ($user) {
            $siswa = Siswa::where('user_id', $user->id)->first();
            if ($siswa) {
                $this->i = $siswa->id;
                $this->kelas = DetailKelas::where('siswa_id', $siswa->id)->get();
                $this->nama = $siswa->nama;
                $this->jenis_kelamin = $siswa->jenis_kelamin;
                $this->tanggal_lahir = Carbon::parse($siswa->tanggal_lahir)->format('Y-m-d');
                $this->alamat = $siswa->alamat;
                $this->foto = $siswa->foto;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'kelas' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'kelas' => 'required',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/siswa/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        Siswa::where('id', $this->i)->update([
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'alamat' => $this->alamat,
            'foto' => $foto2,
        ]);

        foreach ($this->kelas as $data) {
            DetailKelas::where('hash', $data->hash)->update([
                'kelas_id' => $data['kelas_id']
            ]);
        }

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Siswa $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/siswa/data');
    }

    public function render()
    {
        $kls = Kelas::get();
        return view('livewire.siswa.ubah', compact(['kls']))->extends('layouts.admin', ['title' => 'Ubah Siswa'])->section('content');
    }
}
