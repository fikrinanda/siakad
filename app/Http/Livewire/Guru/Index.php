<?php

namespace App\Http\Livewire\Guru;

use App\Models\DetailKelas;
use App\Models\Guru;
use App\Models\Kelas;
use App\Models\Mapel;
use Livewire\Component;

class Index extends Component
{
    public $thn;
    public $guru;
    public $siswa;
    public $mapel;
    public $kelas;

    public function mount()
    {
        $this->thn = Kelas::latest('updated_at')->first()->tahun;
        $this->guru = Guru::count();
        $this->siswa = DetailKelas::where('kelas_id', 'like', '%' . substr($this->thn, 2, 2) . '%')->count();
        $this->mapel = Mapel::count();
        $this->kelas = Kelas::where('id', 'like', '%' . substr($this->thn, 2, 2) . '%')->count();
    }

    public function updatedThn()
    {
        $this->guru = Guru::count();
        $this->siswa = DetailKelas::where('kelas_id', 'like', '%' . substr($this->thn, 2, 2) . '%')->count();
        $this->mapel = Mapel::count();
        $this->kelas = Kelas::where('id', 'like', '%' . substr($this->thn, 2, 2) . '%')->count();
    }

    public function render()
    {
        $tahun = Kelas::select('tahun')->groupBy('tahun')->get();
        return view('livewire.guru.index', compact(['tahun']))->extends('layouts.guru', ['title' => 'Guru'])->section('content');
    }
}
