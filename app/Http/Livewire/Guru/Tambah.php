<?php

namespace App\Http\Livewire\Guru;

use App\Models\Guru;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $nip;
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $tgl;
    protected $listeners = ['berhasil'];

    public function mount()
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'nip' => 'required|numeric|unique:users,username',
            'foto' => 'required|image|max:5000',
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            'nip' => 'required|numeric|unique:users,username',
            'foto' => 'required|image|max:5000',
        ]);

        $foto = $this->foto->store('images/guru/foto', 'public');

        $x = User::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "US" . sprintf("%04s", $y);

        User::create([
            'id' => $z,
            'username' => $this->nip,
            'password' => Hash::make($this->nip),
            'level' => 'Guru',
        ]);

        $user = User::where('username', $this->nip)->first();

        $x2 = Guru::max('id');
        $y2 = (int) substr($x2, 2, 4);
        $y2++;
        $z2 = "GR" . sprintf("%04s", $y2);

        Guru::create([
            'id' => $z2,
            'user_id' => $user->id,
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'alamat' => $this->alamat,
            'foto' => $foto,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Guru $this->nama berhasil ditambahkan",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/guru/data');
    }

    public function render()
    {
        return view('livewire.guru.tambah')->extends('layouts.admin', ['title' => 'Tambah Guru'])->section('content');
    }
}
