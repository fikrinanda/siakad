<?php

namespace App\Http\Livewire\Guru;

use App\Models\Guru;
use App\Models\Pengajar;
use App\Models\User;
use Livewire\Component;

class Lihat extends Component
{
    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $uname;
    public $password;
    public $i;

    public function mount($username)
    {
        $user = User::where('username', $username)->first();

        if ($user) {
            $guru = Guru::where('user_id', $user->id)->first();
            if ($guru) {
                $this->i = $guru->id;
                $this->nama = $guru->nama;
                $this->jenis_kelamin = $guru->jenis_kelamin;
                $this->tanggal_lahir = $guru->tanggal_lahir;
                $this->alamat = $guru->alamat;
                $this->foto = $guru->foto;
                $this->uname = $user->username;
                $this->password = $user->password;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function render()
    {
        $mengajar = Pengajar::where('guru_id', $this->i)->get();
        return view('livewire.guru.lihat', compact(['mengajar']))->extends('layouts.admin', ['title' => 'Lihat Guru'])->section('content');
    }
}
