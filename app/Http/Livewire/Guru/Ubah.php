<?php

namespace App\Http\Livewire\Guru;

use App\Models\Guru;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;

    public $nama;
    public $jenis_kelamin;
    public $tanggal_lahir;
    public $alamat;
    public $foto;
    public $foto2;
    public $i;
    public $tgl;
    protected $listeners = ['berhasil'];

    public function mount($username)
    {
        $this->tgl = Carbon::now()->format('Y-m-d');
        $user = User::where('username', $username)->first();

        if ($user) {
            $guru = Guru::where('user_id', $user->id)->first();
            if ($guru) {
                $this->i = $guru->id;
                $this->nama = $guru->nama;
                $this->jenis_kelamin = $guru->jenis_kelamin;
                $this->tanggal_lahir = Carbon::parse($guru->tanggal_lahir)->format('Y-m-d');
                $this->alamat = $guru->alamat;
                $this->foto = $guru->foto;
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            // 'foto' => 'required|image|max:5000',
        ]);
    }

    public function ubah()
    {
        $this->validate([
            'nama' => 'required|regex:/^([^0-9]*)$/|min:3',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|before:today|after:01/01/1940',
            'alamat' => 'required|min:5',
            // 'foto' => 'required|image|max:5000',
        ]);

        if ($this->foto2) {
            Storage::disk('public')->delete($this->foto);
            $foto2 = $this->foto2->store('images/guru/foto', 'public');
        } else {
            $foto2 = $this->foto ?? null;
        }

        Guru::where('id', $this->i)->update([
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
            'tanggal_lahir' => $this->tanggal_lahir,
            'alamat' => $this->alamat,
            'foto' => $foto2,
        ]);

        $this->showModal();
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Guru $this->nama berhasil diubah",
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/guru/data');
    }

    public function render()
    {
        return view('livewire.guru.ubah')->extends('layouts.admin', ['title' => 'Ubah Guru'])->section('content');
    }
}
