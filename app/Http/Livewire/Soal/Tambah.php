<?php

namespace App\Http\Livewire\Soal;

use App\Models\Detail;
use Illuminate\Support\Str;
use App\Models\Mapel;
use App\Models\Pengajar;
use App\Models\Soal;
use Livewire\Component;
use Livewire\WithFileUploads;

class Tambah extends Component
{
    use WithFileUploads;

    public $mapel_id;
    public $mpl;
    public $soal = [];
    public $semester;
    public $jenis;
    public $nama;
    public $keterangan;
    public $pilgan = 0;
    public $uraian = 0;
    public $bobot_pilgan = 0;
    public $bobot_uraian = 0;
    public $dp = 'disabled';
    public $du = 'disabled';
    protected $listeners = ['berhasil'];

    public function mount($mapel_id)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $mapel_id)->first()) {
            $this->mpl = Mapel::find($mapel_id)->nama;

            $this->soal = [
                []
            ];
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'semester' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'keterangan' => 'required',
            'soal.*.pertanyaan' => 'required',
            'soal.*.jawaban' => 'required',
        ]);

        if ($this->pilgan > 0) {
            $this->validateOnly($field, [
                'bobot_pilgan' => "required|numeric|gt:0",
            ]);
        }

        if ($this->uraian > 0) {
            $this->validateOnly($field, [
                'bobot_uraian' => "required|numeric|gt:0",
            ]);
        }
    }

    public function updatedSoal()
    {
        $hp = 0;
        $hu = 0;
        foreach ($this->soal as $key => $value) {
            if (isset($value['jawaban'])) {
                if ($value['jawaban'] != 'Uraian') {
                    $hp++;
                } else if ($value['jawaban'] == 'Uraian') {
                    $hu++;
                }
            }
        }

        $this->pilgan = $hp;
        $this->uraian = $hu;

        if ($this->pilgan > 1) {
            if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
            } else {
                $this->dp = '';
                $this->bobot_pilgan = 0;
            }
        } else if ($this->pilgan == 1) {
            if ($this->uraian == 1) {
                $this->dp = '';
                $this->bobot_pilgan = 0;
            } else if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
                $this->bobot_uraian = 0;
            } else {
                $this->dp = '';
            }
        } else {
            $this->dp = 'disabled';
        }

        if ($this->uraian > 1) {
            if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
            } else {
                $this->du = '';
                $this->bobot_uraian = 0;
            }
        } else if ($this->uraian == 1) {
            if ($this->pilgan == 1) {
                $this->du = '';
                $this->bobot_uraian = 0;
            } else if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
                $this->bobot_pilgan = 0;
            } else {
                $this->du = '';
            }
        } else {
            $this->du = 'disabled';
        }
    }

    public function updatedBobotPilgan()
    {
        if ($this->uraian > 0) {
            $this->bobot_uraian = 100 - $this->bobot_pilgan;
        } else {
            $this->bobot_uraian = 0;
        }
    }

    public function updatedBobotUraian()
    {
        if ($this->pilgan > 0) {
            $this->bobot_pilgan = 100 - $this->bobot_uraian;
        } else {
            $this->bobot_pilgan = 0;
        }
    }

    public function add()
    {
        $this->soal[] = [];
    }

    public function remove($index)
    {
        if (count($this->soal) > 1) {
            unset($this->soal[$index]);
            $this->soal = array_values($this->soal);
        }
    }

    public function tambah()
    {
        $this->validate([
            'semester' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'keterangan' => 'required',
            'soal.*.pertanyaan' => 'required',
            'soal.*.jawaban' => 'required',
        ]);

        if ($this->pilgan > 0) {
            $this->validate([
                'bobot_pilgan' => "required|numeric|gt:0",
            ]);
        }

        if ($this->uraian > 0) {
            $this->validate([
                'bobot_uraian' => "required|numeric|gt:0",
            ]);
        }

        $x = Soal::max('id');
        $y = (int) substr($x, 2, 4);
        $y++;
        $z = "SL" . sprintf("%04s", $y);

        if (isset(Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis)) {
            if ((Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UTS') || (Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UAS')) {
                $this->showAlert();
            } else {
                Soal::create([
                    'id' => $z,
                    'guru_id' => auth()->user()->guru->id,
                    'mapel_id' => $this->mapel_id,
                    'semester' => $this->semester,
                    'nama' => $this->nama,
                    'keterangan' => $this->keterangan,
                    'jenis' => $this->jenis,
                    'bobot_pilgan' => $this->bobot_pilgan,
                    'bobot_uraian' => $this->bobot_uraian,
                ]);

                foreach ($this->soal as $data) {
                    if ((isset($data['a'])) || (isset($data['b'])) || (isset($data['c'])) || (isset($data['d'])) || (isset($data['e'])) || (isset($data['pilihan_a'])) || (isset($data['pilihan_b'])) || (isset($data['pilihan_c'])) || (isset($data['pilihan_d'])) || (isset($data['pilihan_e']))) {
                        if (isset($data['pertanyaan_gambar'])) {
                            $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            $karina = null;
                        }
                        if (isset($data['pilihan_a'])) {
                            $a = $data['pilihan_a']->store('images/soal', 'public');
                        } else {
                            $a = null;
                        }
                        if (isset($data['pilihan_b'])) {
                            $b = $data['pilihan_b']->store('images/soal', 'public');
                        } else {
                            $b = null;
                        }
                        if (isset($data['pilihan_c'])) {
                            $c = $data['pilihan_c']->store('images/soal', 'public');
                        } else {
                            $c = null;
                        }
                        if (isset($data['pilihan_d'])) {
                            $d = $data['pilihan_d']->store('images/soal', 'public');
                        } else {
                            $d = null;
                        }
                        if (isset($data['pilihan_e'])) {
                            $e = $data['pilihan_e']->store('images/soal', 'public');
                        } else {
                            $e = null;
                        }
                        Detail::create([
                            'soal_id' => $z,
                            'hash' => Str::random(32),
                            'pertanyaan' => $data['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'pilihan_a' => $data['a'] ?? null,
                            'pilihan_b' => $data['b'] ?? null,
                            'pilihan_c' => $data['c'] ?? null,
                            'pilihan_d' => $data['d'] ?? null,
                            'pilihan_e' => $data['e'] ?? null,
                            'pilihan_a_gambar' => $a,
                            'pilihan_b_gambar' => $b,
                            'pilihan_c_gambar' => $c,
                            'pilihan_d_gambar' => $d,
                            'pilihan_e_gambar' => $e,
                            'jawaban' => $data['jawaban'],
                        ]);
                    } else if ($data['jawaban'] == 'Uraian') {
                        if (isset($data['pertanyaan_gambar'])) {
                            $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            $karina = null;
                        }
                        Detail::create([
                            'soal_id' => $z,
                            'hash' => Str::random(32),
                            'pertanyaan' => $data['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'jawaban' => $data['jawaban'],
                        ]);
                    }
                }

                $this->showModal();
            }
        } else {
            Soal::create([
                'id' => $z,
                'guru_id' => auth()->user()->guru->id,
                'mapel_id' => $this->mapel_id,
                'semester' => $this->semester,
                'nama' => $this->nama,
                'keterangan' => $this->keterangan,
                'jenis' => $this->jenis,
                'bobot_pilgan' => $this->bobot_pilgan,
                'bobot_uraian' => $this->bobot_uraian,
            ]);

            foreach ($this->soal as $data) {
                if ((isset($data['a'])) || (isset($data['b'])) || (isset($data['c'])) || (isset($data['d'])) || (isset($data['e'])) || (isset($data['pilihan_a'])) || (isset($data['pilihan_b'])) || (isset($data['pilihan_c'])) || (isset($data['pilihan_d'])) || (isset($data['pilihan_e']))) {
                    if (isset($data['pertanyaan_gambar'])) {
                        $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                    } else {
                        $karina = null;
                    }
                    if (isset($data['pilihan_a'])) {
                        $a = $data['pilihan_a']->store('images/soal', 'public');
                    } else {
                        $a = null;
                    }
                    if (isset($data['pilihan_b'])) {
                        $b = $data['pilihan_b']->store('images/soal', 'public');
                    } else {
                        $b = null;
                    }
                    if (isset($data['pilihan_c'])) {
                        $c = $data['pilihan_c']->store('images/soal', 'public');
                    } else {
                        $c = null;
                    }
                    if (isset($data['pilihan_d'])) {
                        $d = $data['pilihan_d']->store('images/soal', 'public');
                    } else {
                        $d = null;
                    }
                    if (isset($data['pilihan_e'])) {
                        $e = $data['pilihan_e']->store('images/soal', 'public');
                    } else {
                        $e = null;
                    }
                    Detail::create([
                        'soal_id' => $z,
                        'hash' => Str::random(32),
                        'pertanyaan' => $data['pertanyaan'],
                        'pertanyaan_gambar' => $karina,
                        'pilihan_a' => $data['a'] ?? null,
                        'pilihan_b' => $data['b'] ?? null,
                        'pilihan_c' => $data['c'] ?? null,
                        'pilihan_d' => $data['d'] ?? null,
                        'pilihan_e' => $data['e'] ?? null,
                        'pilihan_a_gambar' => $a,
                        'pilihan_b_gambar' => $b,
                        'pilihan_c_gambar' => $c,
                        'pilihan_d_gambar' => $d,
                        'pilihan_e_gambar' => $e,
                        'jawaban' => $data['jawaban'],
                    ]);
                } else if ($data['jawaban'] == 'Uraian') {
                    if (isset($data['pertanyaan_gambar'])) {
                        $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                    } else {
                        $karina = null;
                    }
                    Detail::create([
                        'soal_id' => $z,
                        'hash' => Str::random(32),
                        'pertanyaan' => $data['pertanyaan'],
                        'pertanyaan_gambar' => $karina,
                        'jawaban' => $data['jawaban'],
                    ]);
                }
            }

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data soal berhasil ditambahkan",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Soal $this->jenis sudah ada",
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/soal/lihat/$this->mapel_id");
    }

    public function render()
    {
        return view('livewire.soal.tambah')->extends('layouts.guru', ['title' => 'Tambah Soal'])->section('content');
    }
}
