<?php

namespace App\Http\Livewire\Soal;

use App\Models\Mapel;
use App\Models\Pengajar;
use App\Models\Soal;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $i;
    public $kls;
    public $mpl;
    public $semester = '1';
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['yakin' => 'hancur', 'batal'];

    public function mount($mapel_id)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $mapel_id)->first()) {
            $this->i = $mapel_id;
            $m = Mapel::find($mapel_id);
            $this->mpl = $m->nama;
        } else {
            abort('404');
        }
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function hapus($id)
    {
        $soal = Soal::find($id);
        $this->showConfirmation($soal->id, $soal->nama);
    }

    public function hancur($id)
    {
        $soal = Soal::find($id);
        $soal->delete();
        $this->showModal($soal->nama);
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data Soal $nama berhasil dihapus",
        ]);
    }

    public function showConfirmation($id, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Soal $nama?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $id, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function render()
    {
        $soal = Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->i)->where('semester', $this->semester)->paginate($this->perPage);
        return view('livewire.soal.lihat', compact(['soal']))->extends('layouts.guru', ['title' => 'Lihat Soal'])->section('content');
    }
}
