<?php

namespace App\Http\Livewire\Soal;

use App\Models\Detail as ModelsDetail;
use App\Models\Soal;
use Livewire\Component;

class Detail extends Component
{
    public $soal;
    public $mpl;
    public $semester;
    public $nama;
    public $jenis;
    public $detail;

    public function mount($soal_id)
    {
        if (Soal::find($soal_id)) {
            $z = Soal::find($soal_id);
            $this->soal = Soal::find($soal_id);
            $this->mpl = $z->mapel->nama;
            $this->semester = $z->semester;
            $this->nama = $z->nama;
            $this->jenis = $z->jenis;
            $this->detail = ModelsDetail::where('soal_id', $soal_id)->get();
        } else {
            abort('404');
        }
    }

    public function render()
    {
        return view('livewire.soal.detail')->extends('layouts.guru', ['title' => 'Detail Soal'])->section('content');
    }
}
