<?php

namespace App\Http\Livewire\Soal;

use App\Models\Detail;
use App\Models\Soal;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Ubah extends Component
{
    use WithFileUploads;
    public $soal_id;
    public $mapel_id;
    public $mpl;
    public $semester;
    public $jenis;
    public $keterangan;
    public $nama;
    public $pilgan = 0;
    public $uraian = 0;
    public $bobot_pilgan = 0;
    public $bobot_uraian = 0;
    public $dp = 'disabled';
    public $du = 'disabled';
    public $ds;
    public $dhp = 0;
    public $dhu = 0;
    public $shp = 0;
    public $shu = 0;
    public $detail;
    public $soal = [];
    public $shin;
    public $no;
    public $s;
    public $z;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil', 'sip'];

    protected $rules = [
        'detail.*.pertanyaan' => 'required|string',
        'detail.*.pertanyaan_gambar' => 'image',
        'detail.*.pilihan_a' => 'string',
        'detail.*.pilihan_b' => 'string',
        'detail.*.pilihan_c' => 'string',
        'detail.*.pilihan_d' => 'string',
        'detail.*.pilihan_e' => 'string',
        'detail.*.pilihan_a_gambar' => 'image',
        'detail.*.pilihan_b_gambar' => 'image',
        'detail.*.pilihan_c_gambar' => 'image',
        'detail.*.pilihan_d_gambar' => 'image',
        'detail.*.pilihan_e_gambar' => 'image',
        'detail.*.jawaban' => 'required',
    ];

    public function mount($soal_id)
    {
        if (Soal::find($soal_id)) {
            $z = Soal::find($soal_id);
            $this->soal = Soal::find($soal_id);
            $this->mpl = $z->mapel->nama;
            $this->semester = $z->semester;
            $this->nama = $z->nama;
            $this->jenis = $z->jenis;
            $this->keterangan = $z->keterangan;
            $this->detail = Detail::where('soal_id', $soal_id)->get();
            $this->shin = count($this->detail);
            $this->mapel_id = $z->mapel_id;
            $this->pilgan = Detail::where('soal_id', $soal_id)->where('jawaban', '!=', 'Uraian')->count();
            $this->bobot_pilgan = $z->bobot_pilgan;
            $this->uraian = Detail::where('soal_id', $soal_id)->where('jawaban', 'Uraian')->count();
            $this->bobot_uraian = $z->bobot_uraian;

            $this->soal = [
                []
            ];


            $hp = 0;
            $hu = 0;
            foreach ($this->detail as $key => $value) {
                $this->s[] = [];
                $this->z[$key]['pertanyaan'] = $value->pertanyaan;
                $this->z[$key]['a'] = $value->pilihan_a;
                $this->z[$key]['b'] = $value->pilihan_b;
                $this->z[$key]['c'] = $value->pilihan_c;
                $this->z[$key]['d'] = $value->pilihan_d;
                $this->z[$key]['e'] = $value->pilihan_e;
                $this->z[$key]['jawaban'] = $value->jawaban;
                $this->z[$key]['pertanyaan_gambar'] = $value->pertanyaan_gambar;
                $this->ds[$key]['pertanyaan_gambar'] = '';
                $this->z[$key]['pilihan_a_gambar'] = $value->pilihan_a_gambar;
                $this->z[$key]['pilihan_b_gambar'] = $value->pilihan_b_gambar;
                $this->z[$key]['pilihan_c_gambar'] = $value->pilihan_c_gambar;
                $this->z[$key]['pilihan_d_gambar'] = $value->pilihan_d_gambar;
                $this->z[$key]['pilihan_e_gambar'] = $value->pilihan_e_gambar;
                $this->ds[$key]['pilihan_a_gambar'] = '';
                $this->ds[$key]['pilihan_b_gambar'] = '';
                $this->ds[$key]['pilihan_c_gambar'] = '';
                $this->ds[$key]['pilihan_d_gambar'] = '';
                $this->ds[$key]['pilihan_e_gambar'] = '';

                if (isset($value['jawaban'])) {
                    if ($value['jawaban'] != 'Uraian') {
                        $hp++;
                    } else if ($value['jawaban'] == 'Uraian') {
                        $hu++;
                    }
                }
            }

            $this->dhp = $hp;
            $this->dhu = $hu;

            if ($this->bobot_pilgan > 0) {
                $this->dp = '';
            }
            if ($this->bobot_uraian > 0) {
                $this->du = '';
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'semester' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'keterangan' => 'required',
            'z.*.pertanyaan' => 'required',
            'z.*.jawaban' => 'required',
        ]);

        if (count($this->soal) > 1) {
            $this->validateOnly($field, [
                'soal.*.pertanyaan' => 'required',
                'soal.*.jawaban' => 'required',
            ]);
        }

        if ($this->pilgan > 0) {
            $this->validateOnly($field, [
                'bobot_pilgan' => "required|numeric|gt:0",
            ]);
        }

        if ($this->uraian > 0) {
            $this->validateOnly($field, [
                'bobot_uraian' => "required|numeric|gt:0",
            ]);
        }
    }

    public function updatedDetail()
    {
        $hp = 0;
        $hu = 0;
        foreach ($this->detail as $key => $value) {
            if (isset($value['jawaban'])) {
                if ($value['jawaban'] != 'Uraian') {
                    $hp++;
                } else if ($value['jawaban'] == 'Uraian') {
                    $hu++;
                }
            }
        }

        $this->dhp = $hp;
        $this->dhu = $hu;

        $this->pilgan = $this->shp + $this->dhp;
        $this->uraian = $this->shu + $this->dhu;

        if ($this->pilgan > 1) {
            if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
            } else {
                $this->dp = '';
                $this->bobot_pilgan = 0;
            }
        } else if ($this->pilgan == 1) {
            if ($this->uraian == 1) {
                $this->dp = '';
            } else if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
                $this->bobot_uraian = 0;
            } else {
                $this->dp = '';
            }
        } else {
            $this->dp = 'disabled';
        }

        if ($this->uraian > 1) {
            if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
            } else {
                $this->du = '';
                $this->bobot_uraian = 0;
            }
        } else if ($this->uraian == 1) {
            if ($this->pilgan == 1) {
                $this->du = '';
            } else if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
                $this->bobot_pilgan = 0;
            } else {
                $this->du = '';
            }
        } else {
            $this->du = 'disabled';
        }
    }

    public function updatedSoal()
    {
        $hp = 0;
        $hu = 0;
        foreach ($this->soal as $key => $value) {
            if (isset($value['jawaban'])) {
                if ($value['jawaban'] != 'Uraian') {
                    $hp++;
                } else if ($value['jawaban'] == 'Uraian') {
                    $hu++;
                }
            }
        }

        $this->shp = $hp;
        $this->shu = $hu;

        $this->pilgan = $this->shp + $this->dhp;
        $this->uraian = $this->shu + $this->dhu;

        if ($this->pilgan > 1) {
            if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
            } else {
                $this->dp = '';
                $this->bobot_pilgan = 0;
            }
        } else if ($this->pilgan == 1) {
            if ($this->uraian == 1) {
                $this->dp = '';
            } else if ($this->uraian == 0) {
                $this->dp = 'disabled';
                $this->bobot_pilgan = 100;
                $this->bobot_uraian = 0;
            } else {
                $this->dp = '';
            }
        } else {
            $this->dp = 'disabled';
        }

        if ($this->uraian > 1) {
            if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
            } else {
                $this->du = '';
                $this->bobot_uraian = 0;
            }
        } else if ($this->uraian == 1) {
            if ($this->pilgan == 1) {
                $this->du = '';
            } else if ($this->pilgan == 0) {
                $this->du = 'disabled';
                $this->bobot_uraian = 100;
                $this->bobot_pilgan = 0;
            } else {
                $this->du = '';
            }
        } else {
            $this->du = 'disabled';
        }
    }

    public function updatedBobotPilgan()
    {
        if ($this->uraian > 0) {
            $this->bobot_uraian = 100 - $this->bobot_pilgan;
        } else {
            $this->bobot_uraian = 0;
        }
    }

    public function updatedBobotUraian()
    {
        if ($this->pilgan > 0) {
            $this->bobot_pilgan = 100 - $this->bobot_uraian;
        } else {
            $this->bobot_pilgan = 0;
        }
    }

    public function add()
    {
        $this->soal[] = [];
    }

    public function remove($index)
    {
        if (count($this->soal) > 1) {
            unset($this->soal[$index]);
            $this->soal = array_values($this->soal);
        }
    }

    public function del($index)
    {
        $this->ds[$index]['pertanyaan_gambar'] = 'd-none';
        $this->z[$index]['pertanyaan_gambar'] = null;
    }

    public function bat($index)
    {
        $this->ds[$index]['pertanyaan_gambar'] = '';
        $this->z[$index]['pertanyaan_gambar'] = $this->detail[$index]['pertanyaan_gambar'];
    }

    public function del_a($index)
    {
        $this->ds[$index]['pilihan_a_gambar'] = 'd-none';
        $this->z[$index]['pilihan_a_gambar'] = null;
    }

    public function bat_a($index)
    {
        $this->ds[$index]['pilihan_a_gambar'] = '';
        $this->z[$index]['pilihan_a_gambar'] = $this->detail[$index]['pilihan_a_gambar'];
    }

    public function del_b($index)
    {
        $this->ds[$index]['pilihan_b_gambar'] = 'd-none';
        $this->z[$index]['pilihan_b_gambar'] = null;
    }

    public function bat_b($index)
    {
        $this->ds[$index]['pilihan_b_gambar'] = '';
        $this->z[$index]['pilihan_b_gambar'] = $this->detail[$index]['pilihan_b_gambar'];
    }

    public function del_c($index)
    {
        $this->ds[$index]['pilihan_c_gambar'] = 'd-none';
        $this->z[$index]['pilihan_c_gambar'] = null;
    }

    public function bat_c($index)
    {
        $this->ds[$index]['pilihan_c_gambar'] = '';
        $this->z[$index]['pilihan_c_gambar'] = $this->detail[$index]['pilihan_c_gambar'];
    }

    public function del_d($index)
    {
        $this->ds[$index]['pilihan_d_gambar'] = 'd-none';
        $this->z[$index]['pilihan_d_gambar'] = null;
    }

    public function bat_d($index)
    {
        $this->ds[$index]['pilihan_d_gambar'] = '';
        $this->z[$index]['pilihan_d_gambar'] = $this->detail[$index]['pilihan_d_gambar'];
    }

    public function del_e($index)
    {
        $this->ds[$index]['pilihan_e_gambar'] = 'd-none';
        $this->z[$index]['pilihan_e_gambar'] = null;
    }

    public function bat_e($index)
    {
        $this->ds[$index]['pilihan_e_gambar'] = '';
        $this->z[$index]['pilihan_e_gambar'] = $this->detail[$index]['pilihan_e_gambar'];
    }

    public function ubah()
    {
        // dd($this->z);
        $this->validate([
            'semester' => 'required',
            'nama' => 'required',
            'jenis' => 'required',
            'keterangan' => 'required',
            'z.*.pertanyaan' => 'required',
            'z.*.jawaban' => 'required',
        ]);

        if (count($this->soal) > 1) {
            $this->validate([
                'soal.*.pertanyaan' => 'required',
                'soal.*.jawaban' => 'required',
            ]);
        }

        if ($this->pilgan > 0) {
            $this->validate([
                'bobot_pilgan' => "required|numeric|gt:0",
            ]);
        }

        if ($this->uraian > 0) {
            $this->validate([
                'bobot_uraian' => "required|numeric|gt:0",
            ]);
        }

        if (isset(Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis)) {
            if ((Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UTS') || (Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UAS')) {
                if (Soal::where('guru_id', auth()->user()->guru->id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->id == $this->soal_id) {
                    Soal::where('id', $this->soal_id)->update([
                        'semester' => $this->semester,
                        'nama' => $this->nama,
                        'jenis' => $this->jenis,
                        'keterangan' => $this->keterangan,
                        'bobot_pilgan' => $this->bobot_pilgan,
                        'bobot_uraian' => $this->bobot_uraian,
                    ]);

                    foreach ($this->detail as $k => $data) {
                        if ((isset($this->z[$k]['a'])) || (isset($this->z[$k]['b'])) || (isset($this->z[$k]['c'])) || (isset($this->z[$k]['d'])) || (isset($this->z[$k]['e']))) {
                            if (isset($this->s[$k]['pertanyaan_gambar'])) {
                                if (isset($this->z[$k]['pertanyaan_gambar'])) {
                                    Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                                }
                                $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pertanyaan_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                                    $karina = null;
                                } else {
                                    $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                                }
                            }
                            if (isset($this->s[$k]['pilihan_a_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                                $a = $this->s[$k]['pilihan_a_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pilihan_a_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                                    $a = null;
                                } else {
                                    $a = $this->z[$k]['pilihan_a_gambar'] ?? null;
                                }
                            }
                            if (isset($this->s[$k]['pilihan_b_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                                $b = $this->s[$k]['pilihan_b_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pilihan_b_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                                    $b = null;
                                } else {
                                    $b = $this->z[$k]['pilihan_b_gambar'] ?? null;
                                }
                            }
                            if (isset($this->s[$k]['pilihan_c_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                                $c = $this->s[$k]['pilihan_c_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pilihan_c_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                                    $c = null;
                                } else {
                                    $c = $this->z[$k]['pilihan_c_gambar'] ?? null;
                                }
                            }
                            if (isset($this->s[$k]['pilihan_d_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                                $d = $this->s[$k]['pilihan_d_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pilihan_d_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                                    $d = null;
                                } else {
                                    $d = $this->z[$k]['pilihan_d_gambar'] ?? null;
                                }
                            }
                            if (isset($this->s[$k]['pilihan_e_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                                $e = $this->s[$k]['pilihan_e_gambar']->store('images/soal', 'public');
                            } else {
                                if ($this->z[$k]['pilihan_e_gambar'] == null) {
                                    Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                                    $e = null;
                                } else {
                                    $e = $this->z[$k]['pilihan_e_gambar'] ?? null;
                                }
                            }
                            Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                                'pertanyaan' => $this->z[$k]['pertanyaan'],
                                'pertanyaan_gambar' => $karina,
                                'pilihan_a' => $this->z[$k]['a'] ?? null,
                                'pilihan_b' => $this->z[$k]['b'] ?? null,
                                'pilihan_c' => $this->z[$k]['c'] ?? null,
                                'pilihan_d' => $this->z[$k]['d'] ?? null,
                                'pilihan_e' => $this->z[$k]['e'] ?? null,
                                'pilihan_a_gambar' => $a,
                                'pilihan_b_gambar' => $b,
                                'pilihan_c_gambar' => $c,
                                'pilihan_d_gambar' => $d,
                                'pilihan_e_gambar' => $e,
                                'jawaban' => $this->z[$k]['jawaban'],
                            ]);
                        } else if ($this->z[$k]['jawaban'] == 'Uraian') {
                            if (isset($this->s[$k]['pertanyaan_gambar'])) {
                                if (isset($this->z[$k]['pertanyaan_gambar'])) {
                                    Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                                }
                                $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                            } else {
                                $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                            }
                            Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                                'pertanyaan' => $this->z[$k]['pertanyaan'],
                                'pertanyaan_gambar' => $karina,
                                'jawaban' => $this->z[$k]['jawaban'],
                            ]);
                        }
                    }

                    if ((count($this->soal) > 1) || (isset($this->soal[0]['pertanyaan'])) || (isset($this->soal[0]['jawaban']))) {
                        foreach ($this->soal as $data) {
                            if ((isset($data['a'])) || (isset($data['b'])) || (isset($data['c'])) || (isset($data['d'])) || (isset($data['e']))) {
                                if (isset($data['pertanyaan_gambar'])) {
                                    $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                                } else {
                                    $karina = null;
                                }
                                if (isset($data['pilihan_a'])) {
                                    $a = $data['pilihan_a']->store('images/soal', 'public');
                                } else {
                                    $a = null;
                                }
                                if (isset($data['pilihan_b'])) {
                                    $b = $data['pilihan_b']->store('images/soal', 'public');
                                } else {
                                    $b = null;
                                }
                                if (isset($data['pilihan_c'])) {
                                    $c = $data['pilihan_c']->store('images/soal', 'public');
                                } else {
                                    $c = null;
                                }
                                if (isset($data['pilihan_d'])) {
                                    $d = $data['pilihan_d']->store('images/soal', 'public');
                                } else {
                                    $d = null;
                                }
                                if (isset($data['pilihan_e'])) {
                                    $e = $data['pilihan_e']->store('images/soal', 'public');
                                } else {
                                    $e = null;
                                }
                                Detail::create([
                                    'soal_id' => $this->soal_id,
                                    'hash' => Str::random(32),
                                    'pertanyaan' => $data['pertanyaan'],
                                    'pertanyaan_gambar' => $karina,
                                    'pilihan_a' => $data['a'] ?? null,
                                    'pilihan_b' => $data['b'] ?? null,
                                    'pilihan_c' => $data['c'] ?? null,
                                    'pilihan_d' => $data['d'] ?? null,
                                    'pilihan_e' => $data['e'] ?? null,
                                    'pilihan_a_gambar' => $a,
                                    'pilihan_b_gambar' => $b,
                                    'pilihan_c_gambar' => $c,
                                    'pilihan_d_gambar' => $d,
                                    'pilihan_e_gambar' => $e,
                                    'jawaban' => $data['jawaban'],
                                ]);
                            } else if ($data['jawaban'] == 'Uraian') {
                                if (isset($data['pertanyaan_gambar'])) {
                                    $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                                } else {
                                    $karina = null;
                                }
                                Detail::create([
                                    'soal_id' => $this->soal_id,
                                    'hash' => Str::random(32),
                                    'pertanyaan' => $data['pertanyaan'],
                                    'pertanyaan_gambar' => $karina,
                                    'jawaban' => $data['jawaban'],
                                ]);
                            }
                        }
                    }

                    $this->showModal();
                } else {
                    $this->showAlert();
                }
            } else {
                Soal::where('id', $this->soal_id)->update([
                    'semester' => $this->semester,
                    'nama' => $this->nama,
                    'jenis' => $this->jenis,
                    'keterangan' => $this->keterangan,
                    'bobot_pilgan' => $this->bobot_pilgan,
                    'bobot_uraian' => $this->bobot_uraian,
                ]);

                foreach ($this->detail as $k => $data) {
                    if ((isset($this->z[$k]['a'])) || (isset($this->z[$k]['b'])) || (isset($this->z[$k]['c'])) || (isset($this->z[$k]['d'])) || (isset($this->z[$k]['e']))) {
                        if (isset($this->s[$k]['pertanyaan_gambar'])) {
                            if (isset($this->z[$k]['pertanyaan_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                            }
                            $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pertanyaan_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                                $karina = null;
                            } else {
                                $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                            }
                        }
                        if (isset($this->s[$k]['pilihan_a_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                            $a = $this->s[$k]['pilihan_a_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pilihan_a_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                                $a = null;
                            } else {
                                $a = $this->z[$k]['pilihan_a_gambar'] ?? null;
                            }
                        }
                        if (isset($this->s[$k]['pilihan_b_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                            $b = $this->s[$k]['pilihan_b_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pilihan_b_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                                $b = null;
                            } else {
                                $b = $this->z[$k]['pilihan_b_gambar'] ?? null;
                            }
                        }
                        if (isset($this->s[$k]['pilihan_c_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                            $c = $this->s[$k]['pilihan_c_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pilihan_c_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                                $c = null;
                            } else {
                                $c = $this->z[$k]['pilihan_c_gambar'] ?? null;
                            }
                        }
                        if (isset($this->s[$k]['pilihan_d_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                            $d = $this->s[$k]['pilihan_d_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pilihan_d_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                                $d = null;
                            } else {
                                $d = $this->z[$k]['pilihan_d_gambar'] ?? null;
                            }
                        }
                        if (isset($this->s[$k]['pilihan_e_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                            $e = $this->s[$k]['pilihan_e_gambar']->store('images/soal', 'public');
                        } else {
                            if ($this->z[$k]['pilihan_e_gambar'] == null) {
                                Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                                $e = null;
                            } else {
                                $e = $this->z[$k]['pilihan_e_gambar'] ?? null;
                            }
                        }
                        Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                            'pertanyaan' => $this->z[$k]['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'pilihan_a' => $this->z[$k]['a'] ?? null,
                            'pilihan_b' => $this->z[$k]['b'] ?? null,
                            'pilihan_c' => $this->z[$k]['c'] ?? null,
                            'pilihan_d' => $this->z[$k]['d'] ?? null,
                            'pilihan_e' => $this->z[$k]['e'] ?? null,
                            'pilihan_a_gambar' => $a,
                            'pilihan_b_gambar' => $b,
                            'pilihan_c_gambar' => $c,
                            'pilihan_d_gambar' => $d,
                            'pilihan_e_gambar' => $e,
                            'jawaban' => $this->z[$k]['jawaban'],
                        ]);
                    } else if ($this->z[$k]['jawaban'] == 'Uraian') {
                        if (isset($this->s[$k]['pertanyaan_gambar'])) {
                            if (isset($this->z[$k]['pertanyaan_gambar'])) {
                                Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                            }
                            $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                        }
                        Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                            'pertanyaan' => $this->z[$k]['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'jawaban' => $this->z[$k]['jawaban'],
                        ]);
                    }
                }

                if ((count($this->soal) > 1) || (isset($this->soal[0]['pertanyaan'])) || (isset($this->soal[0]['jawaban']))) {
                    foreach ($this->soal as $data) {
                        if ((isset($data['a'])) || (isset($data['b'])) || (isset($data['c'])) || (isset($data['d'])) || (isset($data['e']))) {
                            if (isset($data['pertanyaan_gambar'])) {
                                $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                            } else {
                                $karina = null;
                            }
                            if (isset($data['pilihan_a'])) {
                                $a = $data['pilihan_a']->store('images/soal', 'public');
                            } else {
                                $a = null;
                            }
                            if (isset($data['pilihan_b'])) {
                                $b = $data['pilihan_b']->store('images/soal', 'public');
                            } else {
                                $b = null;
                            }
                            if (isset($data['pilihan_c'])) {
                                $c = $data['pilihan_c']->store('images/soal', 'public');
                            } else {
                                $c = null;
                            }
                            if (isset($data['pilihan_d'])) {
                                $d = $data['pilihan_d']->store('images/soal', 'public');
                            } else {
                                $d = null;
                            }
                            if (isset($data['pilihan_e'])) {
                                $e = $data['pilihan_e']->store('images/soal', 'public');
                            } else {
                                $e = null;
                            }
                            Detail::create([
                                'soal_id' => $this->soal_id,
                                'hash' => Str::random(32),
                                'pertanyaan' => $data['pertanyaan'],
                                'pertanyaan_gambar' => $karina,
                                'pilihan_a' => $data['a'] ?? null,
                                'pilihan_b' => $data['b'] ?? null,
                                'pilihan_c' => $data['c'] ?? null,
                                'pilihan_d' => $data['d'] ?? null,
                                'pilihan_e' => $data['e'] ?? null,
                                'pilihan_a_gambar' => $a,
                                'pilihan_b_gambar' => $b,
                                'pilihan_c_gambar' => $c,
                                'pilihan_d_gambar' => $d,
                                'pilihan_e_gambar' => $e,
                                'jawaban' => $data['jawaban'],
                            ]);
                        } else if ($data['jawaban'] == 'Uraian') {
                            if (isset($data['pertanyaan_gambar'])) {
                                $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                            } else {
                                $karina = null;
                            }
                            Detail::create([
                                'soal_id' => $this->soal_id,
                                'hash' => Str::random(32),
                                'pertanyaan' => $data['pertanyaan'],
                                'pertanyaan_gambar' => $karina,
                                'jawaban' => $data['jawaban'],
                            ]);
                        }
                    }
                }

                $this->showModal();
            }
        } else {
            Soal::where('id', $this->soal_id)->update([
                'semester' => $this->semester,
                'nama' => $this->nama,
                'jenis' => $this->jenis,
                'keterangan' => $this->keterangan,
                'bobot_pilgan' => $this->bobot_pilgan,
                'bobot_uraian' => $this->bobot_uraian,
            ]);

            foreach ($this->detail as $k => $data) {
                if ((isset($this->z[$k]['a'])) || (isset($this->z[$k]['b'])) || (isset($this->z[$k]['c'])) || (isset($this->z[$k]['d'])) || (isset($this->z[$k]['e']))) {
                    if (isset($this->s[$k]['pertanyaan_gambar'])) {
                        if (isset($this->z[$k]['pertanyaan_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                        }
                        $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pertanyaan_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                            $karina = null;
                        } else {
                            $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                        }
                    }
                    if (isset($this->s[$k]['pilihan_a_gambar'])) {
                        Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                        $a = $this->s[$k]['pilihan_a_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pilihan_a_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_a_gambar']);
                            $a = null;
                        } else {
                            $a = $this->z[$k]['pilihan_a_gambar'] ?? null;
                        }
                    }
                    if (isset($this->s[$k]['pilihan_b_gambar'])) {
                        Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                        $b = $this->s[$k]['pilihan_b_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pilihan_b_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_b_gambar']);
                            $b = null;
                        } else {
                            $b = $this->z[$k]['pilihan_b_gambar'] ?? null;
                        }
                    }
                    if (isset($this->s[$k]['pilihan_c_gambar'])) {
                        Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                        $c = $this->s[$k]['pilihan_c_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pilihan_c_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_c_gambar']);
                            $c = null;
                        } else {
                            $c = $this->z[$k]['pilihan_c_gambar'] ?? null;
                        }
                    }
                    if (isset($this->s[$k]['pilihan_d_gambar'])) {
                        Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                        $d = $this->s[$k]['pilihan_d_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pilihan_d_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_d_gambar']);
                            $d = null;
                        } else {
                            $d = $this->z[$k]['pilihan_d_gambar'] ?? null;
                        }
                    }
                    if (isset($this->s[$k]['pilihan_e_gambar'])) {
                        Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                        $e = $this->s[$k]['pilihan_e_gambar']->store('images/soal', 'public');
                    } else {
                        if ($this->z[$k]['pilihan_e_gambar'] == null) {
                            Storage::disk('public')->delete($this->z[$k]['pilihan_e_gambar']);
                            $e = null;
                        } else {
                            $e = $this->z[$k]['pilihan_e_gambar'] ?? null;
                        }
                    }
                    Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                        'soal_id' => $this->soal_id,
                        'pertanyaan' => $this->z[$k]['pertanyaan'],
                        'pertanyaan_gambar' => $karina,
                        'pilihan_a' => $this->z[$k]['a'] ?? null,
                        'pilihan_b' => $this->z[$k]['b'] ?? null,
                        'pilihan_c' => $this->z[$k]['c'] ?? null,
                        'pilihan_d' => $this->z[$k]['d'] ?? null,
                        'pilihan_e' => $this->z[$k]['e'] ?? null,
                        'pilihan_a_gambar' => $a,
                        'pilihan_b_gambar' => $b,
                        'pilihan_c_gambar' => $c,
                        'pilihan_d_gambar' => $d,
                        'pilihan_e_gambar' => $e,
                        'jawaban' => $this->z[$k]['jawaban'],
                    ]);
                } else if ($this->z[$k]['jawaban'] == 'Uraian') {
                    if (isset($this->s[$k]['pertanyaan_gambar'])) {
                        if (isset($this->z[$k]['pertanyaan_gambar'])) {
                            Storage::disk('public')->delete($this->z[$k]['pertanyaan_gambar']);
                        }
                        $karina = $this->s[$k]['pertanyaan_gambar']->store('images/soal', 'public');
                    } else {
                        $karina = $this->z[$k]['pertanyaan_gambar'] ?? null;
                    }
                    Detail::where('soal_id', $this->soal_id)->where('hash', $data->hash)->update([
                        'pertanyaan' => $this->z[$k]['pertanyaan'],
                        'pertanyaan_gambar' => $karina,
                        'jawaban' => $this->z[$k]['jawaban'],
                    ]);
                }
            }

            if ((count($this->soal) > 1) || (isset($this->soal[0]['pertanyaan'])) || (isset($this->soal[0]['jawaban']))) {

                foreach ($this->soal as $data) {
                    if ((isset($data['a'])) || (isset($data['b'])) || (isset($data['c'])) || (isset($data['d'])) || (isset($data['e']))) {
                        if (isset($data['pertanyaan_gambar'])) {
                            $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            $karina = null;
                        }
                        if (isset($data['pilihan_a'])) {
                            $a = $data['pilihan_a']->store('images/soal', 'public');
                        } else {
                            $a = null;
                        }
                        if (isset($data['pilihan_b'])) {
                            $b = $data['pilihan_b']->store('images/soal', 'public');
                        } else {
                            $b = null;
                        }
                        if (isset($data['pilihan_c'])) {
                            $c = $data['pilihan_c']->store('images/soal', 'public');
                        } else {
                            $c = null;
                        }
                        if (isset($data['pilihan_d'])) {
                            $d = $data['pilihan_d']->store('images/soal', 'public');
                        } else {
                            $d = null;
                        }
                        if (isset($data['pilihan_e'])) {
                            $e = $data['pilihan_e']->store('images/soal', 'public');
                        } else {
                            $e = null;
                        }
                        Detail::create([
                            'soal_id' => $this->soal_id,
                            'hash' => Str::random(32),
                            'pertanyaan' => $data['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'pilihan_a' => $data['a'] ?? null,
                            'pilihan_b' => $data['b'] ?? null,
                            'pilihan_c' => $data['c'] ?? null,
                            'pilihan_d' => $data['d'] ?? null,
                            'pilihan_e' => $data['e'] ?? null,
                            'pilihan_a_gambar' => $a,
                            'pilihan_b_gambar' => $b,
                            'pilihan_c_gambar' => $c,
                            'pilihan_d_gambar' => $d,
                            'pilihan_e_gambar' => $e,
                            'jawaban' => $data['jawaban'],
                        ]);
                    } else if ($data['jawaban'] == 'Uraian') {
                        if (isset($data['pertanyaan_gambar'])) {
                            $karina = $data['pertanyaan_gambar']->store('images/soal', 'public');
                        } else {
                            $karina = null;
                        }
                        Detail::create([
                            'soal_id' => $this->soal_id,
                            'hash' => Str::random(32),
                            'pertanyaan' => $data['pertanyaan'],
                            'pertanyaan_gambar' => $karina,
                            'jawaban' => $data['jawaban'],
                        ]);
                    }
                }
            }

            $this->showModal();
        }
    }

    public function hapus($no)
    {
        $this->showConfirmation($no);
    }

    public function hancur($no)
    {
        $this->no = $no;
        $this->showModal2();
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data soal berhasil diubah",
        ]);
    }
    public function showModal2()
    {
        $this->emit('swal:modal2', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data soal berhasil dihapus",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Soal $this->jenis sudah ada",
            'timeout' => 5000
        ]);
    }


    public function showConfirmation($no)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin menghapus Soal?",
            'text'        => "Setalah dihapus, anda tidak dapat mengembalikan data ini!",
            'confirmText' => 'Ya, hapus!',
            'method'      => 'appointments:delete',
            'params'      => $no, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/soal/lihat/$this->mapel_id");
    }

    public function sip()
    {
        Detail::where('hash', $this->no)->delete();
        return redirect()->to("/soal/ubah/$this->soal_id");
    }

    public function render()
    {
        return view('livewire.soal.ubah')->extends('layouts.guru', ['title' => 'Ubah Soal'])->section('content');
    }
}
