<?php

namespace App\Http\Livewire\Soal;

use App\Models\Pengajar;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $kelas = Pengajar::where('guru_id', auth()->user()->guru->id)->paginate($this->perPage);
        return view('livewire.soal.data', compact(['kelas']))->extends('layouts.guru', ['title' => 'Data Soal'])->section('content');
    }
}
