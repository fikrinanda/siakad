<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Login extends Component
{
    public $username;
    public $password;

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->first();
        if ($user && Hash::check($this->password, $user->password)) {
            if ($user->level == 'Admin') {
                Auth::login($user);
                return redirect()->intended('/admin');
            } else if ($user->level == 'Siswa') {
                Auth::login($user);
                return redirect()->intended('/siswa');
            } else {
                return back()->with('error', 'Username / Password Salah');
            }
        } else {
            return back()->with('error', 'Username / Password Salah');
        }
    }

    public function render()
    {
        return view('livewire.auth.login')->extends('layouts.auth.login', ['title' => 'Login'])->section('content');
    }
}
