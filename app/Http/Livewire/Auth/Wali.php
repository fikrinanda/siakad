<?php

namespace App\Http\Livewire\Auth;

use App\Models\Kelas;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Wali extends Component
{
    public $username;
    public $password;

    public function login()
    {
        $this->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::whereUsername($this->username)->first();
        if ($user && Hash::check($this->password, $user->password)) {
            if ($user->level == 'Guru') {
                $test = Kelas::where('guru_id', $user->guru->id)->exists();
                if ($test) {
                    Auth::login($user);
                    return redirect()->intended('/guru');
                } else {
                    return back()->with('error', 'Username / Password Salah');
                }
            } else {
                return back()->with('error', 'Username / Password Salah');
            }
        } else {
            return back()->with('error', 'Username / Password Salah');
        }
    }

    public function render()
    {
        return view('livewire.auth.wali')->extends('layouts.auth.login', ['title' => 'Login wali kelas'])->section('content');
    }
}
