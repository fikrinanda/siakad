<?php

namespace App\Http\Livewire\Nilai;

use App\Models\Nilai;
use Livewire\Component;
use Livewire\WithPagination;

class Detail extends Component
{
    use WithPagination;

    public $perPage = 5;
    public $kelas_id;
    public $mapel_id;
    public $siswa_id;
    public $semester;
    protected $paginationTheme = 'bootstrap';

    public function mount($kelas_id, $mapel_id, $siswa_id, $semester)
    {
        $test = Nilai::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $semester)->exists();
        if ($test) {
        } else {
            abort('404');
        }
    }

    public function render()
    {
        $nilai = Nilai::where('siswa_id', $this->siswa_id)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->paginate($this->perPage);
        return view('livewire.nilai.detail', compact(['nilai']))->extends('layouts.guru', ['title' => 'Detail Nilai'])->section('content');
    }
}
