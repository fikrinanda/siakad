<?php

namespace App\Http\Livewire\Nilai;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Siswa;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $i;
    public $i2;
    public $kls;
    public $mpl;
    public $semester = '1';
    protected $paginationTheme = 'bootstrap';

    public function mount($kelas_id, $mapel_id)
    {
        $this->i = $kelas_id;
        $this->i2 = $mapel_id;
        $k = Kelas::find($kelas_id);
        $this->kls = $k->nama . ' Tahun ' . $k->tahun;
        $m = Mapel::find($mapel_id);
        $this->mpl = $m->nama;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $nilai = Siswa::selectRaw('siswa.*')->join('detail_kelas', 'detail_kelas.siswa_id', '=', 'siswa.id')->where('detail_kelas.kelas_id',  $this->i)->paginate($this->perPage);

        // dd($nilai);
        return view('livewire.nilai.lihat', compact(['nilai']))->extends('layouts.guru', ['title' => 'Lihat Nilai'])->section('content');
    }
}
