<?php

namespace App\Http\Livewire\Nilai;

use App\Imports\NilaiImport;
use Livewire\Component;
use Livewire\WithFileUploads;

class Import extends Component
{
    use WithFileUploads;
    public $file;

    public function import()
    {
        $import = new NilaiImport;
        $import->import($this->file);
        if ($import->failures()->isNotEmpty()) {
            return back()->withFailures($import->failures());
        }
        return back()->withStatus('Data nilai berhasil diimport');
    }

    public function render()
    {
        return view('livewire.nilai.import')->extends('layouts.guru', ['title' => 'Import Nilai'])->section('content');
    }
}
