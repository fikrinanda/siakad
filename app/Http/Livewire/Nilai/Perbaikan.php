<?php

namespace App\Http\Livewire\Nilai;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Pengajar;
use App\Models\Perbaikan as ModelsPerbaikan;
use App\Models\Siswa;
use Livewire\Component;

class Perbaikan extends Component
{
    public $kls;
    public $mpl;
    public $ssw;
    public $semester;
    public $siswa;
    public $siswa2;
    protected $listeners = ['berhasil'];
    public $nilai;
    public $kelas_id;
    public $mapel_id;

    public function mount($siswa, $kelas_id, $mapel_id, $semester)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->first()) {
            $j = 0;
            $t = 0;
            $tugas2 = Nilai::where('siswa_id', $siswa)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $semester)->where(function ($q) {
                $q->where('jenis', 'Tugas Rumah');
                $q->orWhere('jenis', 'Kuis');
                $q->orWhere('jenis', 'Tugas');
            })->get();
            foreach ($tugas2 as $key) {
                $j += $key->nilai;
                $t++;
            }
            if ($j > 0) {
                $tugas = $j / $t;
            } else {
                $tugas = 0;
            }

            $uts = Nilai::where('siswa_id', $siswa)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $semester)->where('jenis', 'UTS')->first();
            if (isset($uts->nilai)) {
                $salon = $uts->nilai;
            } else {
                $salon = 0;
            }

            $uas = Nilai::where('siswa_id', $siswa)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $semester)->where('jenis', 'UAS')->first();
            if (isset($uts->nilai)) {
                $bobrok = $uas->nilai;
            } else {
                $bobrok = 0;
            }

            $pekenyes = round(((2 * $tugas) + $salon + $bobrok) / 4);
            $remidi = ModelsPerbaikan::where('siswa_id', $siswa)->where('mapel_id', $mapel_id)->where('kelas_id', $kelas_id)->where('semester', $semester)->exists();
            if ($pekenyes < 70) {
                if ($remidi) {
                    abort('404');
                } else {
                    $this->kls = Kelas::find($kelas_id)->nama . ' Tahun ' . Kelas::find($kelas_id)->tahun;
                    $this->mpl = Mapel::find($mapel_id)->nama;
                    // $this->ssw = Siswa::where('kelas_id', $kelas_id)->get();
                    $this->siswa2 = Siswa::find($siswa)->nama;
                    $this->semester = $semester;

                    $this->nilai = [
                        []
                    ];
                }
            } else {
                abort('404');
            }
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'nilai' => 'required|regex:/^[0-9\s]+$/i|numeric|between:0,100'
        ]);
    }

    public function tambah()
    {
        $this->validate([
            'nilai' => 'required|regex:/^[0-9\s]+$/i|numeric|between:0,100'
        ]);

        $test = ModelsPerbaikan::where('siswa_id', $this->siswa)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->exists();
        if ($test) {
            $this->showAlert();
        } else {
            ModelsPerbaikan::create([
                'siswa_id' => $this->siswa,
                'kelas_id' => $this->kelas_id,
                'mapel_id' => $this->mapel_id,
                'soal_id' => null,
                'semester' => $this->semester,
                'nilai' => $this->nilai,
            ]);

            $this->showModal();
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data nilai perbaikan berhasil ditambahkan",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Nilai perbaikan sudah ada",
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/nilai/lihat/$this->kelas_id/$this->mapel_id");
    }

    public function render()
    {
        return view('livewire.nilai.perbaikan')->extends('layouts.guru', ['title' => 'Tambah Nilai'])->section('content');
    }
}
