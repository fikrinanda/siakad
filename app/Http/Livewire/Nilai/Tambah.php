<?php

namespace App\Http\Livewire\Nilai;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Pengajar;
use App\Models\Siswa;
use Livewire\Component;

class Tambah extends Component
{
    public $kls;
    public $mpl;
    public $ssw;
    public $semester;
    public $jenis;
    public $siswa;
    public $siswa2;
    protected $listeners = ['berhasil'];
    public $nilai = [];
    public $kelas_id;
    public $mapel_id;

    public function mount($siswa, $kelas_id, $mapel_id, $semester)
    {
        if (Pengajar::where('guru_id', auth()->user()->guru->id)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->first()) {
            $this->kls = Kelas::find($kelas_id)->nama . ' Tahun ' . Kelas::find($kelas_id)->tahun;
            $this->mpl = Mapel::find($mapel_id)->nama;
            // $this->ssw = Siswa::where('kelas_id', $kelas_id)->get();
            $this->siswa2 = Siswa::find($siswa)->nama;
            $this->semester = $semester;

            $this->nilai = [
                []
            ];
        } else {
            abort('404');
        }
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'jenis' => 'required',
            'nilai.*' => 'required|regex:/^[0-9\s]+$/i|numeric|between:0,100'
        ]);
    }

    public function updatedJenis()
    {
        if (($this->jenis == 'UAS') || ($this->jenis == 'UTS') || ($this->jenis == 'Perbaikan')) {
            $this->nilai = [
                []
            ];
        }
    }

    public function tambah()
    {
        $this->validate([
            'jenis' => 'required',
            'nilai.*' => 'required|regex:/^[0-9\s]+$/i|numeric|between:0,100'
        ]);

        if ($this->jenis == 'Perbaikan') {
            dd($this->nilai);
        } else {
            if (isset(Nilai::where('siswa_id', $this->siswa)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis)) {
                if ((Nilai::where('siswa_id', $this->siswa)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UTS') || (Nilai::where('siswa_id', $this->siswa)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->semester)->where('jenis', $this->jenis)->first()->jenis == 'UAS')) {
                    $this->showAlert();
                } else {
                    foreach ($this->nilai as $data) {
                        Nilai::create([
                            'siswa_id' => $this->siswa,
                            'kelas_id' => $this->kelas_id,
                            'mapel_id' => $this->mapel_id,
                            'semester' => $this->semester,
                            'jenis' => $this->jenis,
                            'nilai' => $data
                        ]);
                    }

                    $this->showModal();
                }
            } else {
                foreach ($this->nilai as $data) {
                    Nilai::create([
                        'siswa_id' => $this->siswa,
                        'kelas_id' => $this->kelas_id,
                        'mapel_id' => $this->mapel_id,
                        'semester' => $this->semester,
                        'jenis' => $this->jenis,
                        'nilai' => $data
                    ]);
                }

                $this->showModal();
            }
        }
    }

    public function add()
    {
        $this->nilai[] = [];
    }

    public function remove($index)
    {
        if ((count($this->nilai) > 1)) {
            unset($this->nilai[$index]);
            $this->nilai = array_values($this->nilai);
        }
    }

    public function showModal()
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Data nilai berhasil ditambahkan",
        ]);
    }

    public function showAlert()
    {
        $this->emit('swal:alert', [
            'icon'    => 'error',
            'title'   => "Nilai $this->jenis sudah ada",
            'timeout' => 5000
        ]);
    }

    public function berhasil()
    {
        return redirect()->to("/nilai/lihat/$this->kelas_id/$this->mapel_id");
    }

    public function render()
    {
        return view('livewire.nilai.tambah')->extends('layouts.guru', ['title' => 'Tambah Nilai'])->section('content');
    }
}
