<?php

namespace App\Http\Livewire\Raport;

use App\Models\Absensi;
use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Ketidakhadiran;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Siswa;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Str;

class Detail extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $kelas_id;
    public $iu;
    public $siswa_id;
    public $map = [];
    public $total = 0;
    public $naik;
    public $tinggal;
    public $d = 'disabled';
    public $nd;
    public $td;
    public $nama;
    public $kls;
    public $tahun;
    public $sakit;
    public $izin;
    public $tanpa_keterangan;
    public $z;
    public $x;
    public $itzeh;
    protected $listeners = ['yakin' => 'hancur', 'batal', 'berhasil'];

    public function mount($kelas_id, $iu, $siswa_id)
    {
        $n = Nilai::where('kelas_id', $kelas_id)->where('siswa_id', $siswa_id)->where('semester', $iu)->exists();
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else if (!$n) {
            abort('404');
        } else {
            $p = Kelas::find($kelas_id)->tahun;
            $pp = explode("/", $p);
            if ($iu == '1') {
                $from = date($pp[0] . '-07-01');
                $to = date($pp[0] . '-12-31');
                $this->sakit = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $this->izin = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $this->tanpa_keterangan = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            } else if ($iu == '2') {
                $from = date($pp[1] . '-01-01');
                $to = date($pp[1] . '-06-30');
                $this->sakit = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $this->izin = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $this->tanpa_keterangan = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            }

            $tahun = Carbon::now()->format('Y');
            $this->x = DetailKelas::where('siswa_id', $siswa_id)->where('kelas_id', 'like', '%' . substr($tahun, -2) . '%')->where('keterangan', '!=', 'Awal Masuk')->exists();
            $this->itzeh = DetailKelas::where('siswa_id', $siswa_id)->where('keterangan', 'Lulus')->exists();
            // dd($this->x);

            if ($this->x) {
                $yuna = DetailKelas::where('siswa_id', $siswa_id)->where('kelas_id', 'like', '%' . substr($tahun, -2) . '%')->where('keterangan', '!=', 'Awal Masuk')->first();

                if ($yuna->kelas_id[0] == $kelas_id[0]) {
                    $this->z = "Tinggal kelas";
                } else {
                    $this->z = "Naik kelas";
                }
            }

            $this->nama = Siswa::find($siswa_id)->nama;
            $kl = Kelas::find($kelas_id);
            $this->tahun = $kl->tahun;
            if ($kl->nama[0] == '7') {
                $this->kls = 'VII - ' . $kl->nama[1];
            } else   if ($kl->nama[0] == '8') {
                $this->kls = 'VIII - ' . $kl->nama[1];
            } else   if ($kl->nama[0] == '9') {
                $this->kls = 'IX - ' . $kl->nama[1];
            }
            $mapel = Mapel::get();
            foreach ($mapel as $key => $value) {
                $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
                $jisun = explode("-", $value->nama);
                if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $jisun[0];
                } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $jisun[0];
                } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $jisun[0];
                }
            }


            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
            foreach ($this->map as $key => $value) {
                $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $value['id'])->where('siswa_id', $siswa_id)->where('semester', $iu)->get();
                foreach ($nilai as $key2 => $value2) {
                    if ($value2->jenis == 'Tugas Rumah' || $value2->jenis == 'Kuis' || $value2->jenis == 'Tugas') {
                        $n++;
                        $tugas += $value2->nilai;
                    }
                    if ($value2->jenis == 'UTS') {
                        $uts += $value2->nilai;
                    }
                    if ($value2->jenis == 'UAS') {
                        $uas += $value2->nilai;
                    }
                }
                if ($tugas == 0 || $n == 0) {
                    $this->map[$key]['tugas'] = 0;
                    $this->map[$key]['total'] = ((2 * 0) + $uts + $uas) / 4;
                } else {
                    $tugas = $tugas / $n;
                    $this->map[$key]['tugas'] = $tugas;
                    $this->map[$key]['uts'] = $uts;
                    $this->map[$key]['uas'] = $uas;
                    $this->map[$key]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
                }

                $tugas = 0;
                $uts = 0;
                $uas = 0;
                $n = 0;
            }
        }
    }

    public function keputusan()
    {
        if ($this->naik != null) {
            $this->showConfirmation("naik kelas", $this->nama);
        } else if ($this->tinggal != null) {
            $this->showConfirmation("tinggal kelas", $this->nama);
        }
    }

    public function updated($field)
    {
        if ($this->tinggal != null || $this->naik != null) {
            $this->d = '';
        }
    }

    public function updatedNaik()
    {
        $this->td = 'disabled';
    }

    public function updatedTinggal()
    {
        $this->nd = 'disabled';
    }

    public function hancur($id)
    {
        if ($this->naik != null) {
            $ket = 'Naik Kelas';
        } else if ($this->tinggal != null) {
            $ket = 'Tinggal Kelas';
        }

        if ($this->naik == 'Lulus') {
            DetailKelas::create([
                'siswa_id' => $this->siswa_id,
                'kelas_id' => null,
                'hash' => Str::random(32),
                'keterangan' => 'Lulus'
            ]);
        } else {
            DetailKelas::create([
                'siswa_id' => $this->siswa_id,
                'kelas_id' => $this->naik ?? $this->tinggal,
                'hash' => Str::random(32),
                'keterangan' => $ket
            ]);
        }

        if ($this->naik != null) {
            $this->showModal("naik kelas", $this->nama);
        } else if ($this->tinggal != null) {
            $this->showModal("tinggal kelas", $this->nama);
        }
    }

    public function batal()
    {
        // dd('batal');
    }

    public function showModal($k, $nama)
    {
        $this->emit('swal:modal', [
            'icon'  => 'success',
            'title' => 'Berhasil!!!',
            'text'  => "Siswa $nama $k",
        ]);
    }

    public function showConfirmation($k, $nama)
    {
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'title'       => "Yakin dengan keputusan?",
            'text'        => "Siswa $nama $k",
            'confirmText' => 'Ya!',
            'method'      => 'appointments:delete',
            'params'      => null, // optional, send params to success confirmation
            'callback'    => '', // optional, fire event if no confirmed
        ]);
    }

    public function berhasil()
    {
        return redirect()->to('/raport/data');
    }

    public function render()
    {
        $bulan = Carbon::now()->format('m');
        if ($bulan <= 6) {
            $aw = Carbon::now()->format('Y');
            $ah = Carbon::now()->format('Y') + 1;
        } else if ($bulan >= 7) {
            $aw = Carbon::now()->format('Y') + 1;
            $ah = Carbon::now()->format('Y') + 2;
        }

        $cur = DetailKelas::where('siswa_id', $this->siswa_id)->where('kelas_id', $this->kelas_id)->first()->kelas_id[0] + 1;
        $cur2 = DetailKelas::where('siswa_id', $this->siswa_id)->where('kelas_id', $this->kelas_id)->first()->kelas_id[0];
        $kelas = Kelas::where('tahun', $aw . '/' . $ah)->where('nama', 'like', '%' . $cur . '%')->get();
        $kelas2 = Kelas::where('tahun', $aw . '/' . $ah)->where('nama', 'like', '%' . $cur2 . '%')->get();
        // if (auth()->user()->level == 'Admin') {
        return view('livewire.raport.detail', compact(['kelas', 'kelas2']))->extends('layouts.admin', ['title' => 'Detail Raport'])->section('content');
        // } else if (auth()->user()->level == 'Guru') {
        //     return view('livewire.raport.detail', compact(['kelas', 'kelas2']))->extends('layouts.guru', ['title' => 'Detail Raport'])->section('content');
        // }
    }
}
