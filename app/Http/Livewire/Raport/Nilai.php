<?php

namespace App\Http\Livewire\Raport;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai as ModelsNilai;
use Livewire\Component;
use Livewire\WithPagination;

class Nilai extends Component
{
    use WithPagination;

    public $kelas_id;
    public $mapel_id;
    public $iu;
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $ket;

    public function mount($kelas_id, $mapel_id, $iu)
    {
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else {
            $test = ModelsNilai::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $iu);
            if ($test->exists()) {
                $m = Mapel::find($mapel_id)->nama;
                $k = Kelas::find($kelas_id);
                $this->ket = "Mata Pelajaran $m Kelas $k->nama Tahun $k->tahun Semester $iu";
            } else {
                abort('404');
            }
        }
    }

    public function render()
    {
        $nilai = ModelsNilai::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $this->kelas_id)->where('mapel_id', $this->mapel_id)->where('semester', $this->iu)->paginate($this->perPage);
        return view('livewire.raport.nilai', compact(['nilai']))->extends('layouts.siswa', ['title' => 'Detail Nilai'])->section('content');
    }
}
