<?php

namespace App\Http\Livewire\Raport;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Nilai;
use App\Models\Perbaikan;
use App\Models\Siswa;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Lihat extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $kelas_id;
    public $iu;
    public $ket;

    public function mount($kelas_id, $iu)
    {
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else {
            $k = Kelas::find($kelas_id);
            $this->ket = "Kelas " . $k->nama . " Tahun " . $k->tahun . " Semester " . $iu;
        }
    }

    public function render()
    {
        $siswa10 = Siswa::selectRaw('siswa.*')->join('detail_kelas', 'detail_kelas.siswa_id', '=', 'siswa.id')->where('detail_kelas.kelas_id',  $this->kelas_id)->where('nama', 'like', '%' . $this->search . '%')->paginate($this->perPage);

        $siswa = Siswa::selectRaw('siswa.*')->join('detail_kelas', 'detail_kelas.siswa_id', '=', 'siswa.id')->where('detail_kelas.kelas_id',  $this->kelas_id)->get();
        foreach ($siswa as $key => $value) {
            $mapel = Mapel::get();
            foreach ($mapel as $key2 => $value2) {
                $winter = Nilai::where('siswa_id', $value->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu)->where('mapel_id', $value2->id)->where(function ($q) {
                    $q->where('jenis', 'Tugas Rumah');
                    $q->orWhere('jenis', 'Kuis');
                    $q->orWhere('jenis', 'Tugas');
                });
                $jum = $winter->count();

                $giselle = Nilai::where('siswa_id', $value->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu)->where('mapel_id', $value2->id)->where('jenis', 'UTS');

                $ningning = Nilai::where('siswa_id', $value->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu)->where('mapel_id', $value2->id)->where('jenis', 'UAS');

                if ($winter->exists()) {
                    if ($giselle->exists()) {
                        $uts = $giselle->first()->nilai;
                        if ($ningning->exists()) {
                            $uas = $ningning->first()->nilai;
                            $nilai = DB::table('nilai')
                                ->select(DB::raw("(((sum(nilai) / $jum) * 2) + $uts + $uas) / 4 as rata"), 'siswa_id')
                                ->where('semester', $this->iu)
                                ->where('kelas_id', $this->kelas_id)
                                ->where(function ($q) {
                                    $q->where('jenis', 'Tugas Rumah');
                                    $q->orWhere('jenis', 'Kuis');
                                    $q->orWhere('jenis', 'Tugas');
                                })
                                ->where('siswa_id', $value->id)
                                ->where('mapel_id', $value2->id)
                                ->groupBy('siswa_id')
                                ->orderBy(DB::raw("(((sum(nilai) / $jum) * 2) + $uts + $uas) / 4"), 'desc')
                                ->get();

                            $karina[$value->id][$value2->id]['siswa'] = $value->nama;
                            $karina[$value->id][$value2->id]['mapel'] = $value2->nama;
                            if ($nilai[0]->rata < 70) {
                                $remidi = Perbaikan::where('siswa_id', $value->id)->where('mapel_id', $value2->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu);
                                if ($remidi->exists()) {
                                    if ($remidi->first()->nilai >= 70) {
                                        $karina[$value->id][$value2->id]['rata'] = 70;
                                    } else if ($remidi->first()->nilai > $nilai[0]->rata) {
                                        $karina[$value->id][$value2->id]['rata'] = $remidi->first()->nilai;
                                    } else {
                                        $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                                    }
                                } else {
                                    $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                                }
                            } else {
                                $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                            }
                        } else {
                            $nilai = DB::table('nilai')
                                ->select(DB::raw("(((sum(nilai) / $jum) * 2) + $uts) / 4 as rata"), 'siswa_id')
                                ->where('semester', $this->iu)
                                ->where('kelas_id', $this->kelas_id)
                                ->where(function ($q) {
                                    $q->where('jenis', 'Tugas Rumah');
                                    $q->orWhere('jenis', 'Kuis');
                                    $q->orWhere('jenis', 'Tugas');
                                })
                                ->where('siswa_id', $value->id)
                                ->where('mapel_id', $value2->id)
                                ->groupBy('siswa_id')
                                ->orderBy(DB::raw("(((sum(nilai) / $jum) * 2) + $uts) / 4"), 'desc')
                                ->get();

                            $karina[$value->id][$value2->id]['siswa'] = $value->nama;
                            $karina[$value->id][$value2->id]['mapel'] = $value2->nama;
                            if ($nilai[0]->rata < 70) {
                                $remidi = Perbaikan::where('siswa_id', $value->id)->where('mapel_id', $value2->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu);
                                if ($remidi->exists()) {
                                    if ($remidi->first()->nilai >= 70) {
                                        $karina[$value->id][$value2->id]['rata'] = 70;
                                    } else if ($remidi->first()->nilai > $nilai[0]->rata) {
                                        $karina[$value->id][$value2->id]['rata'] = $remidi->first()->nilai;
                                    } else {
                                        $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                                    }
                                } else {
                                    $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                                }
                            } else {
                                $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                            }
                        }
                    } else {
                        $nilai = DB::table('nilai')
                            ->select(DB::raw("((sum(nilai) / $jum) * 2) / 4 as rata"), 'siswa_id')
                            ->where('semester', $this->iu)
                            ->where('kelas_id', $this->kelas_id)
                            ->where(function ($q) {
                                $q->where('jenis', 'Tugas Rumah');
                                $q->orWhere('jenis', 'Kuis');
                                $q->orWhere('jenis', 'Tugas');
                            })
                            ->where('siswa_id', $value->id)
                            ->where('mapel_id', $value2->id)
                            ->groupBy('siswa_id')
                            ->orderBy(DB::raw("((sum(nilai) / $jum) * 2) / 4"), 'desc')
                            ->get();

                        $karina[$value->id][$value2->id]['siswa'] = $value->nama;
                        $karina[$value->id][$value2->id]['mapel'] = $value2->nama;
                        if ($nilai[0]->rata < 70) {
                            $remidi = Perbaikan::where('siswa_id', $value->id)->where('mapel_id', $value2->id)->where('kelas_id', $this->kelas_id)->where('semester', $this->iu);
                            if ($remidi->exists()) {
                                if ($remidi->first()->nilai >= 70) {
                                    $karina[$value->id][$value2->id]['rata'] = 70;
                                } else if ($remidi->first()->nilai > $nilai[0]->rata) {
                                    $karina[$value->id][$value2->id]['rata'] = $remidi->first()->nilai;
                                } else {
                                    $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                                }
                            } else {
                                $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                            }
                        } else {
                            $karina[$value->id][$value2->id]['rata'] = $nilai[0]->rata;
                        }
                    }
                }
            }
        }

        $contentID = array();
        foreach ($karina as $key => $row) {
            foreach ($row as $key2 => $value) {
                $contentID[$key] = $value['rata'];
            }
        }
        array_multisort($contentID, SORT_DESC, $karina);

        foreach ($mapel as $key => $value) {
            $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
            $jisun = explode("-", $value->nama);
            if (($this->kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                $map[$key]['id'] = $value->id;
                $map[$key]['nama'] = $jisun[0];
            } else if (($this->kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                $map[$key]['id'] = $value->id;
                $map[$key]['nama'] = $jisun[0];
            } else if (($this->kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                $map[$key]['id'] = $value->id;
                $map[$key]['nama'] = $jisun[0];
            }
        }

        $ae = array();
        foreach ($karina as $key => $value) {
            $ae[$key]['rata'] = 0;
            foreach ($value as $key2 => $value2) {
                $ae[$key]['rata'] += $value2['rata'];
                $ae[$key]['nama'] = $value2['siswa'];
            }
        }

        foreach ($ae as $key => $value) {
            $gee[$key]['rata'] = round($value['rata'] / 12);
            $gee[$key]['nama'] = $value['nama'];
            $gee[$key]['id'] = $key;
        }

        // if (auth()->user()->level == 'Admin') {
        return view('livewire.raport.lihat', compact(['siswa10', 'gee']))->extends('layouts.admin', ['title' => 'Lihat Raport'])->section('content');
        // } else if (auth()->user()->level == 'Guru') {
        //     return view('livewire.raport.lihat', compact(['siswa10', 'gee']))->extends('layouts.guru', ['title' => 'Lihat Raport'])->section('content');
        // }
    }
}
