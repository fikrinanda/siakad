<?php

namespace App\Http\Livewire\Raport;

use App\Models\Absensi;
use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Ketidakhadiran;
use App\Models\Mapel;
use App\Models\Nilai;
use Carbon\Carbon;
use Livewire\Component;

class Show extends Component
{
    public $search = '';
    public $perPage = 5;
    protected $paginationTheme = 'bootstrap';
    public $kelas_id;
    public $iu;
    public $map = [];
    public $total = 0;
    public $z;
    public $x;
    public $kls;
    public $tahun;
    public $sakit;
    public $izin;
    public $tanpa_keterangan;

    public function mount($kelas_id, $iu)
    {
        $n = Nilai::where('kelas_id', $kelas_id)->where('siswa_id', auth()->user()->siswa->id)->where('semester', $iu)->exists();
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else if (!$n) {
            abort('404');
        } else {
            $p = Kelas::find($kelas_id)->tahun;
            $pp = explode("/", $p);
            if ($iu == '1') {
                $from = date($pp[0] . '-07-01');
                $to = date($pp[0] . '-12-31');
                $this->sakit = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $this->izin = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $this->tanpa_keterangan = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            } else if ($iu == '2') {
                $from = date($pp[1] . '-01-01');
                $to = date($pp[1] . '-06-30');
                $this->sakit = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $this->izin = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $this->tanpa_keterangan = Absensi::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            }
            $tahun = Carbon::now()->format('Y');
            $this->x = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', 'like', '%' . substr($tahun, -2) . '%')->exists();
            if ($this->x) {
                $yuna = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', 'like', '%' . substr($tahun, -2) . '%')->first();
                if ($yuna->kelas_id[0] == $kelas_id[0]) {
                    $this->z = "Tinggal kelas";
                } else {
                    $this->z = "Naik kelas";
                }
            }

            $kl = Kelas::find($kelas_id);
            $this->tahun = $kl->tahun;
            if ($kl->nama[0] == '7') {
                $this->kls = 'VII - ' . $kl->nama[1];
            } else if ($kl->nama[0] == '8') {
                $this->kls = 'VIII - ' . $kl->nama[1];
            } else if ($kl->nama[0] == '9') {
                $this->kls = 'IX - ' . $kl->nama[1];
            }

            $mapel = Mapel::get();
            foreach ($mapel as $key => $value) {
                $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
                if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                    $this->map[$key]['id'] = $value->id;
                    $this->map[$key]['nama'] = $value->nama;
                }
            }

            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
            foreach ($this->map as $key => $value) {
                $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $value['id'])->where('siswa_id', auth()->user()->siswa->id)->where('semester', $iu)->get();
                foreach ($nilai as $key2 => $value2) {
                    if ($value2->jenis == 'Tugas Rumah' || $value2->jenis == 'Kuis' || $value2->jenis == 'Tugas') {
                        $n++;
                        $tugas += $value2->nilai;
                    }
                    if ($value2->jenis == 'UTS') {
                        $uts += $value2->nilai;
                    }
                    if ($value2->jenis == 'UAS') {
                        $uas += $value2->nilai;
                    }
                }
                if ($tugas == 0 || $n == 0) {
                    $this->map[$key]['tugas'] = 0;
                    $this->map[$key]['total'] = ((2 * 0) + $uts + $uas) / 4;
                } else {
                    $tugas = $tugas / $n;
                    $this->map[$key]['tugas'] = $tugas;
                    $this->map[$key]['uts'] = $uts;
                    $this->map[$key]['uas'] = $uas;
                    $this->map[$key]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
                }

                $tugas = 0;
                $uts = 0;
                $uas = 0;
                $n = 0;
            }
        }
    }

    public function render()
    {
        return view('livewire.raport.show')->extends('layouts.siswa', ['title' => 'Detail Raport'])->section('content');
    }
}
