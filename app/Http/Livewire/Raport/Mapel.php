<?php

namespace App\Http\Livewire\Raport;

use App\Models\DetailKelas;
use App\Models\Mapel as ModelsMapel;
use Livewire\Component;

class Mapel extends Component
{
    public $kelas_id;
    public $iu;
    public $mapel;

    public function mount($kelas_id, $iu)
    {
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else {
            $test = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->where('kelas_id', $kelas_id)->exists();
            if ($test) {
                if ($kelas_id[0] == '7') {
                    $anu = "VII";
                } else if ($kelas_id[0] == '8') {
                    $anu = "VIII";
                } else if ($kelas_id[0] == '9') {
                    $anu = "IX";
                }
                $this->mapel = ModelsMapel::where('tingkat', $anu)->get();
            } else {
                abort('404');
            }
        }
    }

    public function render()
    {
        return view('livewire.raport.mapel')->extends('layouts.siswa', ['title' => 'Nilai Kelas ' . $this->kelas_id])->section('content');
    }
}
