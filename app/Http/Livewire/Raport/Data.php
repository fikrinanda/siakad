<?php

namespace App\Http\Livewire\Raport;

use App\Models\Kelas;
use App\Models\Siswa;
use Livewire\Component;
use Livewire\WithPagination;

class Data extends Component
{
    use WithPagination;

    public $search = '';
    public $perPage = 5;
    public $t = '';
    protected $paginationTheme = 'bootstrap';

    public function mount()
    {
        // if (auth()->user()->level == 'Admin') {
        //     # code...
        // } else if (auth()->user()->level == 'Guru') {
        //     $test = Kelas::where('guru_id', auth()->user()->guru->id)->exists();
        //     if ($test) {
        //         # code...
        //     } else {
        //         abort('404');
        //     }
        // }
    }

    public function render()
    {
        // if (auth()->user()->level == 'Admin') {
        $kelas = Kelas::where('tahun', 'like', '%' . $this->t . '%')->paginate($this->perPage);
        $thn = Kelas::select('tahun')->groupBy('tahun')->get();
        return view('livewire.raport.data', compact(['kelas', 'thn']))->extends('layouts.admin', ['title' => 'Data Raport'])->section('content');
        // } else if (auth()->user()->level == 'Guru') {
        //     $kelas = Kelas::where('guru_id', auth()->user()->guru->id)->paginate($this->perPage);
        //     return view('livewire.raport.data', compact(['kelas']))->extends('layouts.guru', ['title' => 'Data Raport'])->section('content');
        // }
    }
}
