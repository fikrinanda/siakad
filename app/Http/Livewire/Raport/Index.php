<?php

namespace App\Http\Livewire\Raport;

use App\Models\DetailKelas;
use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        $raport = DetailKelas::where('siswa_id', auth()->user()->siswa->id)->get();
        return view('livewire.raport.index', compact(['raport']))->extends('layouts.siswa', ['title' => 'Raport'])->section('content');
    }
}
