<?php

namespace App\Http\Controllers;

use App\Exports\NilaiExport;
use App\Imports\GuruImport;
use App\Models\Absensi;
use App\Models\DetailKelas;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Materi;
use App\Models\Nilai;
use App\Models\Siswa;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __invoke(Request $request)
    {
        auth()->logout();
        return redirect('/');
    }

    public function cetak($kelas_id, $iu, $siswa_id)
    {
        $n = Nilai::where('kelas_id', $kelas_id)->where('siswa_id', $siswa_id)->where('semester', $iu)->exists();
        if (($iu != '1') && ($iu != '2')) {
            abort('404');
        } else if (!$n) {
            abort('404');
        } else {
            $p = Kelas::find($kelas_id)->tahun;
            $pp = explode("/", $p);
            if ($iu == '1') {
                $from = date($pp[0] . '-07-01');
                $to = date($pp[0] . '-12-31');
                $sakit = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $izin = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $tanpa_keterangan = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            } else if ($iu == '2') {
                $from = date($pp[1] . '-01-01');
                $to = date($pp[1] . '-06-30');
                $sakit = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Sakit')->count();
                $izin = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Izin')->count();
                $tanpa_keterangan = Absensi::where('siswa_id', $siswa_id)->where('kelas_id', $kelas_id)->whereBetween('tanggal', [$from, $to])->where('keterangan', 'Bolos')->count();
            }

            $nama = Siswa::find($siswa_id)->nama;
            $kl = Kelas::find($kelas_id);
            $tahun = $kl->tahun;
            if ($kl->nama[0] == '7') {
                $kls = 'VII - ' . $kl->nama[1];
            } else   if ($kl->nama[0] == '8') {
                $kls = 'VIII - ' . $kl->nama[1];
            } else   if ($kl->nama[0] == '9') {
                $kls = 'IX - ' . $kl->nama[1];
            }
            $mapel = Mapel::get();
            foreach ($mapel as $key => $value) {
                $whatIWant = substr($value->nama, strpos($value->nama, "-") + 1);
                $jisun = explode("-", $value->nama);
                if (($kelas_id[0] == '7') && ($whatIWant == 'VII')) {
                    $map[$key]['id'] = $value->id;
                    $map[$key]['nama'] = $jisun[0];
                } else if (($kelas_id[0] == '8') && ($whatIWant == 'VIII')) {
                    $map[$key]['id'] = $value->id;
                    $map[$key]['nama'] = $jisun[0];
                } else if (($kelas_id[0] == '9') && ($whatIWant == 'IX')) {
                    $map[$key]['id'] = $value->id;
                    $map[$key]['nama'] = $jisun[0];
                }
            }

            $tugas = 0;
            $uts = 0;
            $uas = 0;
            $n = 0;
            foreach ($map as $key => $value) {
                $nilai = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $value['id'])->where('siswa_id', $siswa_id)->where('semester', $iu)->get();
                foreach ($nilai as $key2 => $value2) {
                    if ($value2->jenis == 'Tugas') {
                        $n++;
                        $tugas += $value2->nilai;
                    }
                    if ($value2->jenis == 'UTS') {
                        $uts += $value2->nilai;
                    }
                    if ($value2->jenis == 'UAS') {
                        $uas += $value2->nilai;
                    }
                }
                if ($tugas == 0 || $n == 0) {
                    $map[$key]['tugas'] = 0;
                    $map[$key]['total'] = ((2 * 0) + $uts + $uas) / 4;
                } else {
                    $tugas = $tugas / $n;
                    $map[$key]['tugas'] = $tugas;
                    $map[$key]['uts'] = $uts;
                    $map[$key]['uas'] = $uas;
                    $map[$key]['total'] = ((2 * $tugas) + $uts + $uas) / 4;
                }

                $tugas = 0;
                $uts = 0;
                $uas = 0;
                $n = 0;
            }
            $r = $kelas_id[0] + 1;
            $x = DetailKelas::where('siswa_id', $siswa_id)->where('kelas_id', 'like', '%' . $r . '%')->exists();
            $tanggal = Carbon::now();
            $pdf = PDF::loadView('anu', ['x' => $x, 'iu' => $iu, 'sakit' => $sakit, 'izin' => $izin, 'tanpa_keterangan' => $tanpa_keterangan, 'nama' => $nama, 'tahun' => $tahun, 'kls' => $kls, 'map' => $map, 'tanggal' => $tanggal->format('d F Y'), 'kelas_id' => $kelas_id])->setPaper('a4', 'potrait');
            return $pdf->stream('Raport ' . $tanggal->format('dmYHis') . '.pdf');
        }
    }

    public function show()
    {
        return view('materi');
    }

    public function export($kelas_id, $mapel_id, $iu)
    {
        $test = Nilai::where('kelas_id', $kelas_id)->where('mapel_id', $mapel_id)->where('semester', $iu)->exists();
        if ($test) {
            $kelas = Kelas::find($kelas_id);
            $mapel = Mapel::find($mapel_id);
            $ring = explode("/", $kelas->tahun);
            $tahun = $ring[0] . "-" . $ring[1];
            $nama = "Nilai Kelas " . $kelas->nama . " Tahun " . $tahun . " Semester " . $iu . " Mata Pelajaran " . $mapel->nama;
            $nama = preg_replace('/[[:^print:]]/', '', $nama);
            return Excel::download(new NilaiExport($kelas_id, $mapel_id, $iu), "$nama.xlsx");
        } else {
            abort('404');
        }
    }
}
