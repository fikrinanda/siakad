<?php

namespace App\Imports;

use App\Models\Detail;
use App\Models\DetailKelas;
use App\Models\Guru;
use App\Models\Siswa;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class GuruImport implements ToCollection, WithHeadingRow, SkipsOnError, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        // Validator::make($rows->toArray(), [
        //     '*.email' => ['email', 'unique:users,email'],
        // ])->validate();
        foreach ($rows as $row) {
            DetailKelas::create([
                'siswa_id' => $row['id'],
                'kelas_id' => $row['kelas'],
                'hash' => Str::random(32),
                'keterangan' => 'Naik Kelas'
            ]);
        }
    }
}
