<?php

namespace App\Imports;

use App\Models\Nilai;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class NilaiImport implements ToCollection, WithHeadingRow, SkipsOnError, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.kelas_id' => ['required'],
            '*.mapel_id' => ['required'],
            '*.siswa_id' => ['required'],
            '*.semester' => ['required'],
            '*.jenis' => ['required'],
            '*.nilai' => ['required', 'regex:/^[0-9\s]+$/i', 'numeric', 'between:0,100']
        ])->validate();

        foreach ($rows as $row) {
            Nilai::create([
                'siswa_id' => $row['siswa_id'],
                'kelas_id' => $row['kelas_id'],
                'mapel_id' => $row['mapel_id'],
                'semester' => $row['semester'],
                'jenis' => $row['jenis'],
                'nilai' => $row['nilai'],
            ]);
        }
    }
}
