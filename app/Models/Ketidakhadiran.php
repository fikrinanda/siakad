<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ketidakhadiran extends Model
{
    use HasFactory;

    protected $table = 'ketidakhadiran';
    protected $fillable = ['siswa_id', 'kelas_id', 'hash', 'semester', 'sakit', 'izin', 'tanpa_keterangan'];
    public $incrementing = false;
    public $primaryKey = 'hash';

    public function siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }
}
