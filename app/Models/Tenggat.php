<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenggat extends Model
{
    use HasFactory;

    protected $table = 'tenggat';
    protected $fillable = ['soal_id', 'kelas_id', 'tenggat', 'bagikan'];
    protected $dates = ['tenggat'];
    public $incrementing = false;

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }
}
