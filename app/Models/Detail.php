<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    use HasFactory;

    protected $table = 'detail_soal';
    protected $fillable = ['soal_id', 'hash', 'pertanyaan', 'pertanyaan_gambar', 'pilihan_a', 'pilihan_b', 'pilihan_c', 'pilihan_d', 'pilihan_e', 'pilihan_a_gambar', 'pilihan_b_gambar', 'pilihan_c_gambar', 'pilihan_d_gambar', 'pilihan_e_gambar', 'jawaban'];
    public $primaryKey = 'hash';
    public $incrementing = false;

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }
}
