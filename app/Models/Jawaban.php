<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;

    protected $table = 'jawaban_soal';
    protected $fillable = ['siswa_id', 'soal_id', 'kelas_id', 'hash', 'jawaban'];
    public $primaryKey = 'hash';
    public $incrementing = false;

    public function siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }
}
