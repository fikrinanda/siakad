<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    use HasFactory;

    protected $table = 'soal';
    protected $fillable = ['id', 'guru_id',  'mapel_id', 'nama', 'keterangan', 'jenis',  'semester', 'bobot_pilgan', 'bobot_uraian'];
    public $incrementing = false;

    public function guru()
    {
        return $this->hasOne(Guru::class, 'id', 'guru_id');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }

    public function mapel()
    {
        return $this->hasOne(Mapel::class, 'id', 'mapel_id');
    }
}
