<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;

    protected $table = 'kelas';
    protected $fillable = ['id', 'guru_id', 'nama', 'tahun'];
    public $incrementing = false;

    public function guru()
    {
        return $this->hasOne(Guru::class, 'id', 'guru_id');
    }
}
