<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasil extends Model
{
    use HasFactory;

    protected $table = 'hasil_soal';
    protected $fillable = ['siswa_id', 'soal_id', 'kelas_id', 'pilihan_ganda', 'uraian', 'total'];
    public $incrementing = false;

    public function siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }
}
