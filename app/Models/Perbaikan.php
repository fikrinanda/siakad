<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perbaikan extends Model
{
    use HasFactory;

    protected $table = 'perbaikan';
    protected $fillable = ['siswa_id', 'kelas_id', 'mapel_id', 'soal_id', 'nilai', 'semester'];
    public $incrementing = false;

    public function siswa()
    {
        return $this->hasOne(Siswa::class, 'id', 'siswa_id');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }

    public function mapel()
    {
        return $this->hasOne(Mapel::class, 'id', 'mapel_id');
    }

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }
}
