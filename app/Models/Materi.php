<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    use HasFactory;

    protected $table = 'materi';
    protected $fillable = ['id', 'guru_id', 'mapel_id', 'kelas_id', 'soal_id', 'nama', 'file', 'link'];
    public $incrementing = false;

    public function guru()
    {
        return $this->hasOne(Guru::class, 'id', 'guru_id');
    }

    public function mapel()
    {
        return $this->hasOne(Mapel::class, 'id', 'mapel_id');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }

    public function soal()
    {
        return $this->hasOne(Soal::class, 'id', 'soal_id');
    }
}
