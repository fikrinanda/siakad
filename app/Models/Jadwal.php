<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    use HasFactory;

    protected $table = 'jadwal';
    protected $fillable = ['id', 'kelas_id', 'mapel_id', 'guru_id', 'jam', 'hari'];
    public $incrementing = false;

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id', 'kelas_id');
    }

    public function mapel()
    {
        return $this->hasOne(Mapel::class, 'id', 'mapel_id');
    }

    public function guru()
    {
        return $this->hasOne(Guru::class, 'id', 'guru_id');
    }
}
