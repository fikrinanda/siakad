<?php

namespace App\Console\Commands;

use App\Models\Absensi as ModelsAbsensi;
use App\Models\DetailKelas;
use App\Models\Kelas;
use Carbon\Carbon;
use Illuminate\Console\Command;

class absensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:absensi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tanggal = Carbon::now()->format('Y-m-d');
        $bulan = Carbon::now()->format('m');
        $sem1 = Carbon::now()->format('Y');
        $sem2 = Carbon::now()->format('Y') - 1;

        if ($bulan <= 6) {
            $kelas = Kelas::where('id', 'like', '%' . substr($sem2, -2) . '%')->get();
            foreach ($kelas as $key => $value) {
                $siswa = DetailKelas::where('kelas_id', $value->id)->get();
                foreach ($siswa as $key => $value2) {
                    ModelsAbsensi::create([
                        'siswa_id' => $value2->siswa_id,
                        'tanggal' => $tanggal,
                        'kelas_id' => $value2->kelas_id,
                        'keterangan' => 'Bolos'
                    ]);
                }
            }
        } else if ($bulan >= 7) {
            $kelas = Kelas::where('id', 'like', '%' . substr($sem1, -2) . '%')->get();
            foreach ($kelas as $key => $value) {
                $siswa = DetailKelas::where('kelas_id', $value->id)->get();
                foreach ($siswa as $key => $value2) {
                    ModelsAbsensi::create([
                        'siswa_id' => $value2->siswa_id,
                        'tanggal' => $tanggal,
                        'kelas_id' => $value2->kelas_id,
                        'keterangan' => 'Bolos'
                    ]);
                }
            }
        }
    }
}
