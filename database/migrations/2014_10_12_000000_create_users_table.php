<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('username', 20);
            $table->string('password', 100);
            $table->string('level', 5);
            $table->timestamps();
        });

        Schema::create('mapel', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('nama', 20);
            $table->timestamps();
        });

        Schema::create('guru', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('nama', 50);
            $table->string('jenis_kelamin', 15);
            $table->date('tanggal_lahir');
            $table->string('alamat', 250);
            $table->string('foto', 100);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('kelas', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('guru_id', 50);
            $table->string('nama', 20);
            $table->string('tahun', 10);
            $table->timestamps();

            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');
        });

        Schema::create('siswa', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('user_id', 10);
            $table->string('kelas_id', 10);
            $table->string('nama', 50);
            $table->string('jenis_kelamin', 15);
            $table->date('tanggal_lahir');
            $table->string('alamat', 250);
            $table->string('foto', 100);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
        });

        Schema::create('pengajar', function (Blueprint $table) {
            $table->string('kelas_id', 10);
            $table->string('mapel_id', 10);
            $table->string('guru_id', 50);
            $table->timestamps();

            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
            $table->foreign('mapel_id')->references('id')->on('mapel')->onDelete('cascade');
            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guru', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
        });
        Schema::table('kelas', function (Blueprint $table) {
            $table->dropForeign(['guru_id']);
            $table->dropColumn('guru_id');
        });
        Schema::table('siswa', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropForeign(['kelas_id']);
            $table->dropColumn('kelas_id');
        });
        Schema::table('pengajar', function (Blueprint $table) {
            $table->dropForeign(['kelas_id']);
            $table->dropColumn('kelas_id');
            $table->dropForeign(['mapel_id']);
            $table->dropColumn('mapel_id');
            $table->dropForeign(['guru_id']);
            $table->dropColumn('guru_id');
        });
        Schema::dropIfExists('users');
        Schema::dropIfExists('admin');
        Schema::dropIfExists('mapel');
        Schema::dropIfExists('guru');
        Schema::dropIfExists('kelas');
        Schema::dropIfExists('siswa');
        Schema::dropIfExists('pengajar');
    }
}
