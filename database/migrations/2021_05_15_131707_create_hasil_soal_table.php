<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHasilSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_soal', function (Blueprint $table) {
            $table->string('siswa_id', 10);
            $table->string('soal_id', 10);
            $table->integer('pilihan_ganda')->nullable();
            $table->integer('uraian')->nullable();
            $table->integer('total');
            $table->timestamps();

            $table->foreign('siswa_id')->references('id')->on('siswa')->onDelete('cascade');
            $table->foreign('soal_id')->references('id')->on('soal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hasil_soal', function (Blueprint $table) {
            $table->dropForeign(['siswa_id']);
            $table->dropColumn('siswa_id');
            $table->dropForeign(['soal_id']);
            $table->dropColumn('soal_id');
        });
        Schema::dropIfExists('hasil_soal');
    }
}
