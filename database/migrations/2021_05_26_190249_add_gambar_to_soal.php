<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGambarToSoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_soal', function (Blueprint $table) {
            $table->string('pertanyaan_gambar', 100)->after('pertanyaan')->nullable();
            $table->string('pilihan_a_gambar', 100)->after('pilihan_a')->nullable();
            $table->string('pilihan_b_gambar', 100)->after('pilihan_b')->nullable();
            $table->string('pilihan_c_gambar', 100)->after('pilihan_c')->nullable();
            $table->string('pilihan_d_gambar', 100)->after('pilihan_d')->nullable();
            $table->string('pilihan_e_gambar', 100)->after('pilihan_e')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_soal', function (Blueprint $table) {
            $table->dropColumn('pertanyaan_gambar');
            $table->dropColumn('pilihan_a_gambar');
            $table->dropColumn('pilihan_b_gambar');
            $table->dropColumn('pilihan_c_gambar');
            $table->dropColumn('pilihan_d_gambar');
            $table->dropColumn('pilihan_e_gambar');
        });
    }
}
