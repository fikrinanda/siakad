<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('hari', 10);
            $table->string('kelas_id', 10);
            $table->string('mapel_id', 10);
            $table->string('guru_id', 10);
            $table->string('jam', 50);

            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
            $table->foreign('mapel_id')->references('id')->on('mapel')->onDelete('cascade');
            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jadwal', function (Blueprint $table) {
            $table->dropForeign(['kelas_id']);
            $table->dropColumn('kelas_id');
            $table->dropForeign(['mapel_id']);
            $table->dropColumn('mapel_id');
            $table->dropForeign(['guru_id']);
            $table->dropColumn('guru_id');
        });
        Schema::dropIfExists('jadwal');
    }
}
