<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soal', function (Blueprint $table) {
            $table->string('id', 10)->primary();
            $table->string('guru_id', 10);
            $table->string('mapel_id', 10);
            $table->string('semester', 3);
            $table->string('nama', 250);
            $table->string('jenis', 25);
            $table->timestamps();

            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade');
            $table->foreign('mapel_id')->references('id')->on('mapel')->onDelete('cascade');
        });

        Schema::create('detail_soal', function (Blueprint $table) {
            $table->string('soal_id', 10);
            $table->string('pertanyaan', 250);
            $table->string('pilihan_a', 250)->nullable();
            $table->string('pilihan_b', 250)->nullable();
            $table->string('pilihan_c', 250)->nullable();
            $table->string('pilihan_d', 250)->nullable();
            $table->string('pilihan_e', 250)->nullable();
            $table->string('jawaban', 10);
            $table->timestamps();

            $table->foreign('soal_id')->references('id')->on('soal')->onDelete('cascade');
        });

        Schema::create('tenggat', function (Blueprint $table) {
            $table->string('soal_id', 10);
            $table->string('kelas_id', 10);
            $table->dateTime('tenggat');
            $table->timestamps();

            $table->foreign('soal_id')->references('id')->on('soal')->onDelete('cascade');
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soal', function (Blueprint $table) {
            $table->dropForeign(['guru_id']);
            $table->dropColumn('guru_id');
            $table->dropForeign(['mapel_id']);
            $table->dropColumn('mapel_id');
        });

        Schema::table('detail_soal', function (Blueprint $table) {
            $table->dropForeign(['soal_id']);
            $table->dropColumn('soal_id');
        });

        Schema::table('tenggat', function (Blueprint $table) {
            $table->dropForeign(['soal_id']);
            $table->dropColumn('soal_id');
            $table->dropForeign(['kelas_id']);
            $table->dropColumn('kelas_id');
        });

        Schema::dropIfExists('soal');
        Schema::dropIfExists('detail_soal');
        Schema::dropIfExists('tenggat');
    }
}
