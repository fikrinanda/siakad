<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJawabanSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawaban_soal', function (Blueprint $table) {
            $table->string('siswa_id', 10);
            $table->string('soal_id', 10);
            $table->string('kelas_id', 10);
            $table->string('hash', 32);
            $table->longText('jawaban');
            $table->timestamps();

            $table->foreign('siswa_id')->references('id')->on('siswa')->onDelete('cascade');
            $table->foreign('soal_id')->references('id')->on('soal')->onDelete('cascade');
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
        });

        Schema::table('soal', function (Blueprint $table) {
            $table->longText('keterangan')->after('nama');
        });
        Schema::table('detail_soal', function (Blueprint $table) {
            $table->longText('pertanyaan')->change();
            $table->longText('pilihan_a')->change();
            $table->longText('pilihan_b')->change();
            $table->longText('pilihan_c')->change();
            $table->longText('pilihan_d')->change();
            $table->longText('pilihan_e')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jawaban_soal', function (Blueprint $table) {
            $table->dropForeign(['siswa_id']);
            $table->dropColumn('siswa_id');
            $table->dropForeign(['soal_id']);
            $table->dropColumn('soal_id');
            $table->dropForeign(['kelas_id']);
            $table->dropColumn('kelas_id');
        });
        Schema::table('soal', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
        Schema::table('detail_soal', function (Blueprint $table) {
            $table->string('pertanyaan')->change();
            $table->string('pilihan_a')->change();
            $table->string('pilihan_b')->change();
            $table->string('pilihan_c')->change();
            $table->string('pilihan_d')->change();
            $table->string('pilihan_e')->change();
        });
        Schema::dropIfExists('jawaban_soal');
    }
}
