<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_kelas', function (Blueprint $table) {
            $table->string('keterangan', 15)->after('hash')->nullable();
            $table->string('kelas_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_kelas', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
    }
}
